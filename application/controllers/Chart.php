<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require_once APPPATH."/third_party/PHPExcel.php";

class Chart extends CI_Controller {

	var $url   			 = 'chart';
	var $model           = 'Model_chart_fe';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Model_chart_fe');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_desa');
	}

	public function index()
	{
		$category = $this->uri->segment(3);
		$title_page = "";
		$filter = array();
		if ( $category == "total-anggaran" ) {
			$title_page = "GRAFIK TOTAL ANGGARAN";
		} else if ( $category == "jumlah-dana" ) {
			$title_page = "GRAFIK JUMLAH DANA TEREALISASI";
		} else if ( $category == "persentase-pembangunan" ) {
			$title_page = "GRAFIK PERSENTASE PEMBANGUNAN";
		}
		
		$assets = array(
            "title_page" => $title_page
		);

		$result_search = "Hasil Pencarian Berdasaran <br>";
		$post = $this->input->post();
		if ( $post ) {
			$filter = array(
				"kec_id"      => explode("-", @$post['kec_id'])[0],
				"desa_id"     => explode("-", @$post['desa_id'])[0]
			);

			if ( ! empty($post['kec_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Kecamatan: " . explode("-", @$post['kec_id'])[1] . "
									</span> ";
			}
	
			if ( ! empty($post['desa_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Desa: " . explode("-", @$post['desa_id'])[1] . "
									</span> ";
			}
		}

		if ( $this->session->userdata('role') == 2 ) {
			$filter['kec_id'] = $this->session->userdata('kec_id');
		} elseif ( $this->session->userdata('role') == 3 ) {
			$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
			$filter['desa_id'] = $this->session->userdata('desa_id');
		}

		if ( $category == "total-anggaran" ) {
			$get_chart  = json_encode($this->{$this->model}->get_chart_total_anggaran($filter));
			$get_table  = $this->{$this->model}->get_table_total_anggaran($filter);
		} else if ( $category == "jumlah-dana" ) {
			$get_chart  = json_encode($this->{$this->model}->get_chart_jumlah_dana($filter));
			$get_table  = $this->{$this->model}->get_table_jumlah_dana($filter);
		} else if ( $category == "persentase-pembangunan" ) {
			$get_chart  = json_encode($this->{$this->model}->get_chart_persentase_pembangunan($filter));
			$get_table  = $this->{$this->model}->get_table_persentase_pembangunan($filter);
		}

		$data   = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
			"get_chart"      => $get_chart,
			"get_table"      => $get_table,
			"category"       => $category,
			"filter"         => urlencode(json_encode($filter)),
			"result_search"  => $result_search,
		);

		$this->load->view('template/header', $assets);		
		$this->load->view('template/menu');		
		$this->load->view('' . $this->url . '/index', $data);	
		$this->load->view('template/footer', $assets);
	}

	public function get_all_desa_by_kecamatan()
	{
		$id   = explode("-", $this->uri->segment(4))[0];
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/rcmadmin/Home.php */