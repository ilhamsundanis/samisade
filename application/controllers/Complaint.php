<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Complaint extends CI_Controller {

	var $url   			 = 'complaint';
	var $model           = 'Model_complaint_fe';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Model_complaint_fe');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_desa');
	}

	public function index()
	{
		$assets = array();

		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			$save = $this->{$this->model}->add($this->url, $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil mengirim aduan');
				redirect('complaint');
				
			} else {
				$this->session->set_flashdata('warning', 'Gagal');
			}
		}

		$data = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
		);


		$this->load->view('template/header', $assets);		
		$this->load->view('template/menu');		
		$this->load->view('' . $this->url . '/index', $data);	
		$this->load->view('template/footer', $assets);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/rcmadmin/Home.php */