<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbursement_date extends CI_Controller {

	var $url   			 = 'disbursement_date';
	var $model 			 = 'Model_disbursement_date_fe';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_disbursement_date_fe');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR SEMUA TANGGAL PENCAIRAN DANA"
		);

        $data = array(
            "list_disbursement_date" => $this->{$this->model}->get_all_disbursement_date(),
		);
		
		$this->load->view('template/header', $assets);		
		$this->load->view('template/menu');		
		$this->load->view('' . $this->url . '/index', $data);	
		$this->load->view('template/footer', $assets);
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */