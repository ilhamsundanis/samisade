<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	var $url   = 'home';
	var $model = 'Model_welcome';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_welcome');
	}
	
	public function index()
	{
        $assets = array();

        $data = array(
            'data' => $this->{$this->model}->get_all(),
        );
        
        $this->load->view('template/header', $assets);		
		$this->load->view('template/menu');		
		$this->load->view('' . $this->url . '/index', $data);	
		$this->load->view('template/footer', $assets);
	}
}
