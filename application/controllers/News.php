<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	var $url   = 'news';
    var $model = 'Model_news_fe';
    var $type  = 'news';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_news_fe');
	}
	
	public function index()
	{
        $assets = array();
        
		$config['base_url']   = base_url().'news/index';
		$config['total_rows'] = $this->{$this->model}->count_news();
        $config['per_page']   = 6;

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = 'PERTAMA';
        $config['last_link']        = 'TERAKHIR';
        $config['next_link']        = 'SELANJUTNYA';
        $config['prev_link']        = 'SEBELUMNYA';
        $config['full_tag_open']    = '<div class="col-md-12 pt-50"><nav aria-label="Page navigation"><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item">';
        $config['num_tag_close']    = '</li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-active">';
        $config['cur_tag_close']    = '</span><span class="sr-only">(current)</span></li>';
        $config['next_tag_open']    = '<li class="page-item">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></li>';
        $config['prev_tag_open']    = '<li class="page-item">';
        $config['prev_tagl_close']  = 'SELANJUTNYA</li>';
        $config['first_tag_open']   = '<li class="page-item">';
        $config['first_tagl_close'] = '</li>';
        $config['last_tag_open']    = '<li class="page-item">';
        $config['last_tagl_close']  = '</li>';
        
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);	
        
        $data = array(
            'data'                  => $this->{$this->model}->get_all($config['per_page'], $from),
        );

        $this->load->view('template/header', $assets);		
		$this->load->view('template/menu');		
		$this->load->view('' . $this->url . '/index', $data);	
		$this->load->view('template/footer', $assets);
    }
    
    public function detail() {
        $assets = array();

        $id = $this->uri->segment(3);

        $post = $this->input->post();
		if ( isset($post['save']) ) {
            unset($post['save']);
            
			$post['type'] = $this->type;
			$post['link_id'] = $id;
			$post['ip_address'] = $this->input->ip_address();

			$save = $this->{$this->model}->add('comment', $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Komentar Berhasil Dikirim.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal.');
			}
        }
        
        $nextPage = 0;
        $previousPage = 0;
        if ( $id > 1 ) {
            $nextPage = $id + 1;
            $previousPage = $id - 1;
        } else {
            $nextPage = $id + 1;
            $previousPage = 1;
        }

        $data = array(
            'data'                  => $this->{$this->model}->get_detail($id),
            'next_page'             => $nextPage,
            'previous_page'         => $previousPage,
        );

        $this->load->view('template/header', $assets);		
        $this->load->view('template/menu');	
        if (!$data['data']) {
            $this->load->view('template/notfound');
        } else {
            $this->load->view('' . $this->url . '/detail', $data);	
        }	
		$this->load->view('template/footer', $assets);
    }
}
