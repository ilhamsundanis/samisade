<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Realization extends CI_Controller {

	var $url   			 = 'realization';
	var $model           = 'Model_realization_fe';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';
	var $model_proposals_submission = 'Model_proposals_submission';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Model_realization_fe');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_desa');
		$this->load->model('Model_proposals_submission');
	}

	public function index()
	{
		// check_login();
		$assets = array();

		$list_all_data = array();
		$filter = array();

		if ( $this->session->userdata('role') == 2 ) {
			$filter['kec_id'] = $this->session->userdata('kec_id');
		} elseif ( $this->session->userdata('role') == 3 ) {
			$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
			$filter['desa_id'] = $this->session->userdata('desa_id');
		}

		$result_search = "Hasil Pencarian Berdasaran <br>";
		$post = $this->input->post();
		if ( $post ) {
			$filter = array(
				"kec_id"      => explode("-", @$post['kec_id'])[0],
				"desa_id"     => explode("-", @$post['desa_id'])[0],
				"activity_id" => explode("-", @$post['activity_id'])[0],
				"anggaran"    => $post['anggaran'],
			);

			if ( $this->session->userdata('role') == 2 ) {
				$filter['kec_id'] = $this->session->userdata('kec_id');
			} elseif ( $this->session->userdata('role') == 3 ) {
				$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
				$filter['desa_id'] = $this->session->userdata('desa_id');
			}

			$list_all_data = $this->{$this->model}->get_all_data($filter);

			if ( ! empty($post['kec_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Kecamatan: " . explode("-", @$post['kec_id'])[1] . "
									</span> ";
			}
	
			if ( ! empty($post['desa_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Desa: " . explode("-", @$post['desa_id'])[1] . "
									</span> ";
			}
	
			if ( ! empty($post['activity_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Jenis Pembangunan: " . explode("-", @$post['activity_id'])[1] . "
									</span> ";
			}

			if ( ! empty($post['anggaran']) ) {
				$temp = ($post['anggaran'] == "asc") ? "Terendah" : "Tertinggi";
				$result_search .= "<span class=\"label label-info\">
										Anggaran: " . $temp . "
									</span> ";
			}
		}

		$data = array(
			"list_kecamatan" 		   => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 		   => $this->{$this->model_desa}->get_all(),
			"list_activity"  		   => $this->{$this->model}->get_all_activity(),
			"get_chart_activities"     => json_encode($this->{$this->model}->get_chart_activities($filter)),
			"get_chart_top_total_cost" => json_encode($this->{$this->model}->get_chart_top_total_cost($filter)),
			"list_all_data"  		   => $list_all_data,
			"result_search"  		   => $result_search,
		);


		// pre($data['list_all_data']);

		$this->load->view('template/header', $assets);		
		$this->load->view('template/menu');		
		$this->load->view('' . $this->url . '/index', $data);	
		$this->load->view('template/footer', $assets);
	}

	// public function popup()
	// {
    //     $assets = array();

	// 	$type   = urldecode($this->uri->segment(4));
	// 	$point1 = urldecode($this->uri->segment(5));
	// 	$point2 = urldecode($this->uri->segment(6));
		
    //     $data = array(
    //         "type"   => $type,
    //         "point1" => $point1,
    //         "point2" => $point2,
	// 	);

	// 	// pre($data);
		
	// 	$this->load->view('adminsamisade/template/blank/header', $assets);	
	// 	$this->load->view('adminsamisade/' . $this->url . '/popup/popup_maps', $data);	
	// 	$this->load->view('adminsamisade/template/blank/footer', $assets);
	// }

	public function get_all_desa_by_kecamatan()
	{
		$id   = explode("-", $this->uri->segment(4))[0];
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}

	public function get_chart_activities()
	{
		$data = $this->{$this->model}->get_chart_activities();

		echo json_encode($data);
	}

	public function get_chart_top_total_cost()
	{
		$data = $this->{$this->model}->get_chart_top_total_cost();

		echo json_encode($data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/rcmadmin/Home.php */