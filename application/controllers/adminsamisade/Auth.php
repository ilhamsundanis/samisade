<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	var $url   = 'auth';
	var $model = 'Model_auth';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_auth');
	}


	public function index()
	{

		$asset = array(
					'title' => "Login"
				 );

		$post = $this->input->post();
		if ( $post ) {
			$data = $this->{$this->model}->login($post['email'], $post['password']);
			if ( $data ) {
				$this->{$this->model}->log_access($data);
				
				$this->session->set_userdata('id', $data['id']);
				$this->session->set_userdata('name', $data['name']);
				$this->session->set_userdata('email', $data['email']);
				$this->session->set_userdata('role', $data['role']);
				$this->session->set_userdata('kec_id', $data['kec_id']);
				$this->session->set_userdata('desa_id', $data['desa_id']);
				$this->session->set_userdata('name_kecamatan', $data['name_kecamatan']);
				$this->session->set_userdata('name_desa', $data['name_desa']);

				redirect('adminsamisade/home');
				
			} else {
				$this->session->set_flashdata('warning', 'Email/Password Salah');
			}
		}


		$this->load->view('adminsamisade/template/login/header', $asset);		
		$this->load->view('adminsamisade/' . $this->url . '/index');
		$this->load->view('adminsamisade/template/login/footer', $asset);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/adminsamisade/auth');
	}

	public function change_password()
	{
        $assets = array(
            "title_page" => "UBAH KATA SANDI"
		);
		
		$post = $this->input->post();
		if ( isset ( $post['save'] ) ) {
			unset($post['save']);

			$check = $this->{$this->model}->get_user($post['old_password']);
			if ( $check ) {
				if ( $post['new_password'] == $post['re_new_password'] ) {

					$params = array(
						"password" => md5($post['new_password']),
					);
					$save = $this->{$this->model}->update("user", $params, array('id' => $check['id']));
					$this->session->set_flashdata('success', 'Berhasil');
					
					redirect('/adminsamisade/auth/logout');
					
					if ( $save ) {
						$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
					} else {
						$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
					}

				} else {
					$this->session->set_flashdata('warning', 'Kata sandi baru tidak sama.');
				}
			} else {
				$this->session->set_flashdata('warning', 'Kata sandi lama salah.');
			}
		}

		$data = array();

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/change_password', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/rcmadmin/Auth.php */