<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	var $url   = 'banner';
	var $model = 'Model_banner';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_banner');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "PENGATURAN DASAR > Daftar " . $this->url
		);
		
        $data = array(
            "list_banner" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "PENGATURAN DASAR > Tambah " . $this->url
		);
		
		$post = $this->input->post();
		if ( isset ( $post['save'] ) ) {
			unset($post['save']);

			$image = upload_file( "banner" . date('Ymdhis'), "banner/", "image" );
			if ( $image != "default.jpg" ) {
				$post['image'] = $image;
			}

			$post['created_by'] = $this->session->userdata('id');

			$save = $this->{$this->model}->add($this->url, $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array();

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "PENGATURAN DASAR > Lihat " . $this->url
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( isset ( $post['save'] ) ) {
			unset($post['save']);

			$image = upload_file( "banner" . date('Ymdhis'), "banner/", "image" );
		
			if ( $image != "default.jpg" ) {
				$post['image'] = $image;
			} else {
				unset($post['image']);
			}

			$post['created_by'] = $this->session->userdata('id');
			$post['updated_at'] = date("Y-m-d h:i:s");

			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"banner" => $this->{$this->model}->get_detail($id),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */