<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	var $url   			 = 'category';
	var $model 			 = 'Model_category';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_category');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR KATEGORI"
		);
		
        $data = array(
            "list_category" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "TAMBAH KATEGORI"
		);
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			$post['type']       = "description";
			$post['created_by'] = $this->session->userdata('id');

			$save = $this->{$this->model}->add($this->url, $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"post"			 => $post,
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "LIHAT ATEGORI"
		);

		$id = $this->uri->segment(4); 
		$queryCategory = $this->{$this->model}->get_detail($id);
		
		$post = $this->input->post();
		if ( $post ) {
			$category = $post;
		} else {
			$category = $queryCategory;
		}
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			$post['type']       = "description";
			$post['created_by'] = $this->session->userdata('id');
			$post['updated_at'] = date("Y-m-d h:i:s");

			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"category" 		 => $category,
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

	public function get_all_desa_by_kecamatan()
	{
		$id   = $this->uri->segment(4);
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */