<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Chart extends CI_Controller {

	var $url   			 = 'chart';
	var $model           = 'Model_chart';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Model_chart');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_desa');
	}

	public function index()
	{
		$category = $this->uri->segment(4);
		$title_page = "";
		$filter = array();
		if ( $category == "total-anggaran" ) {
			$title_page = "GRAFIK TOTAL ANGGARAN";
		} else if ( $category == "jumlah-dana" ) {
			$title_page = "GRAFIK JUMLAH DANA TEREALISASI";
		} else if ( $category == "persentase-pembangunan" ) {
			$title_page = "GRAFIK PERSENTASE PEMBANGUNAN";
		}
		
		$assets = array(
            "title_page" => $title_page
		);

		$result_search = "Hasil Pencarian Berdasaran <br>";
		$post = $this->input->post();
		if ( $post ) {
			$filter = array(
				"kec_id"      => explode("-", @$post['kec_id'])[0],
				"desa_id"     => explode("-", @$post['desa_id'])[0]
			);

			if ( ! empty($post['kec_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Kecamatan: " . explode("-", @$post['kec_id'])[1] . "
									</span> ";
			}
	
			if ( ! empty($post['desa_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Desa: " . explode("-", @$post['desa_id'])[1] . "
									</span> ";
			}
		}

		if ( $this->session->userdata('role') == 2 ) {
			$filter['kec_id'] = $this->session->userdata('kec_id');
		} elseif ( $this->session->userdata('role') == 3 ) {
			$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
			$filter['desa_id'] = $this->session->userdata('desa_id');
		}

		if ( $category == "total-anggaran" ) {
			$get_chart  = json_encode($this->{$this->model}->get_chart_total_anggaran($filter));
			$get_table  = $this->{$this->model}->get_table_total_anggaran($filter);
		} else if ( $category == "jumlah-dana" ) {
			$get_chart  = json_encode($this->{$this->model}->get_chart_jumlah_dana($filter));
			$get_table  = $this->{$this->model}->get_table_jumlah_dana($filter);
		} else if ( $category == "persentase-pembangunan" ) {
			$get_chart  = json_encode($this->{$this->model}->get_chart_persentase_pembangunan($filter));
			$get_table  = $this->{$this->model}->get_table_persentase_pembangunan($filter);
		}

		$data   = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
			"get_chart"      => $get_chart,
			"get_table"      => $get_table,
			"category"       => $category,
			"filter"         => urlencode(json_encode($filter)),
			"result_search"  => $result_search,
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/index', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);	
	}

	public function access()
	{
		$assets = array();
		$data = array();
		$filter = array(
			"month" => date("m"),
			"year" 	=> date("Y"),
		);

		$post = $this->input->post();
		if ( $post ) {
			if ( empty($post['month']) ) {
				$post['month'] = date("m");
			}
			$filter = array(
				"month" => $post['month'],
				"year" 	=> $post['year'],
			);
		}
		$data['data_access'] = $this->{$this->model}->get_chart_access($filter);
		$data['post']		 = $post;

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/access', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);	
	}

	public function table()
	{
		$assets = array();
		$data = array();
		$filter = array(
			"month" => date("m"),
			"year" 	=> date("Y"),
		);

		$post = $this->input->post();
		if ( $post ) {
			$filter = array(
				"month" => $post['month'],
				"year" 	=> $post['year'],
			);
		}
		$data['list_access'] = $this->{$this->model}->get_all_access($filter);
		$data['post']		 = $post;

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/table', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);	
	}

	public function get_all_desa_by_kecamatan()
	{
		$id   = explode("-", $this->uri->segment(4))[0];
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}

	public function export() {
		$category = $this->uri->segment(4);
		$filter   = json_decode(urldecode($this->uri->segment(5)), true);

		if ( $this->session->userdata('role') == 2 ) {
			$filter['kec_id'] = $this->session->userdata('kec_id');
		} elseif ( $this->session->userdata('role') == 3 ) {
			$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
			$filter['desa_id'] = $this->session->userdata('desa_id');
		}

		if ( $category == "total-anggaran" ) {
			$get_table  = $this->{$this->model}->get_table_total_anggaran($filter);
		} else if ( $category == "jumlah-dana" ) {
			$get_table  = $this->{$this->model}->get_table_jumlah_dana($filter);
		} else if ( $category == "persentase-pembangunan" ) {
			$get_table  = $this->{$this->model}->get_table_persentase_pembangunan($filter);
		}
		
		$alphas = range("A", "Z");

    	$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);

		$tableBorder = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000')
				)
			)
		);

		$center = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);	

		$right = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			)
		);	

		if ( $category == "total-anggaran" ) {
			$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
			$objPHPExcel->getActiveSheet()->setCellValue("A1", "Laporan Total Anggaran");
			$objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($center);

			$objPHPExcel->getActiveSheet()->setCellValue("A3", "No");
			$objPHPExcel->getActiveSheet()->setCellValue("B3", "Kecamatan");
			$objPHPExcel->getActiveSheet()->setCellValue("C3", "Desa");
			$objPHPExcel->getActiveSheet()->setCellValue("D3", "Total Anggaran");
			
			$no = 3;
			$number = 0;
			$total = 0;
			foreach ($get_table as $value) {
				$no++;
				$number++;
				$total += $value['total_cost'];
				$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, $number);
				$objPHPExcel->getActiveSheet()->setCellValue("B" . $no, $value['kec_name']);
				$objPHPExcel->getActiveSheet()->setCellValue("C" . $no, $value['desa_name']);
				$objPHPExcel->getActiveSheet()->setCellValue("D" . $no, "Rp. " . number_format($value['total_cost'],0,',','.'));
			}

			$objPHPExcel->getActiveSheet()->mergeCells('A'. ($no + 1) .':C' . ($no + 1));
			$objPHPExcel->getActiveSheet()->setCellValue("A". ($no + 1), "Total Keseluruhan Anggaran");
			$objPHPExcel->getActiveSheet()->getStyle("A". ($no + 1))->applyFromArray($right);
			$objPHPExcel->getActiveSheet()->setCellValue("D" . ($no + 1), "Rp. " . number_format($total,0,',','.'));

			$objPHPExcel->getActiveSheet()->getStyle("A3:D" . ($no + 1))->applyFromArray($tableBorder);

			$namaFile = "Laporan Total Anggaran " . date('Ymd');

		} else if ( $category == "jumlah-dana" ) {
			
			$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
			$objPHPExcel->getActiveSheet()->setCellValue("A1", "Laporan Jumlah Dana Terealisasi");
			$objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($center);

			$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
			$objPHPExcel->getActiveSheet()->setCellValue("A3", "No");

			$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
			$objPHPExcel->getActiveSheet()->setCellValue("B3", "Kecamatan");

			$objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
			$objPHPExcel->getActiveSheet()->setCellValue("C3", "Desa");

			$objPHPExcel->getActiveSheet()->mergeCells('D3:E3');
			$objPHPExcel->getActiveSheet()->setCellValue("D3", "Jumlah Dana Terealisasi");
			$objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($center);

			$objPHPExcel->getActiveSheet()->setCellValue("D4", "Tahap 1");
			$objPHPExcel->getActiveSheet()->getStyle("D4")->applyFromArray($center);

			$objPHPExcel->getActiveSheet()->setCellValue("E4", "Tahap 2");
			$objPHPExcel->getActiveSheet()->getStyle("E4")->applyFromArray($center);
			
			$no = 4;
			$number = 0;
			$total_step1 = 0;
			$total_step2 = 0;
			foreach ($get_table as $value) {
				$no++;
				$number++;
				$total_step1 += $value['step1'];
				$total_step2 += $value['step2'];
				$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, $number);
				$objPHPExcel->getActiveSheet()->setCellValue("B" . $no, $value['kec_name']);
				$objPHPExcel->getActiveSheet()->setCellValue("C" . $no, $value['desa_name']);
				$objPHPExcel->getActiveSheet()->setCellValue("D" . $no, "Rp. " . number_format($value['step1'],0,',','.'));
				$objPHPExcel->getActiveSheet()->setCellValue("E" . $no, "Rp. " . number_format($value['step2'],0,',','.'));
			}

			$objPHPExcel->getActiveSheet()->mergeCells('A'. ($no + 1) .':C' . ($no + 1));
			$objPHPExcel->getActiveSheet()->setCellValue("A". ($no + 1), "Total Keseluruhan Anggaran");
			$objPHPExcel->getActiveSheet()->getStyle("A". ($no + 1))->applyFromArray($right);
			$objPHPExcel->getActiveSheet()->setCellValue("D" . ($no + 1), "Rp. " . number_format($total_step1,0,',','.'));
			$objPHPExcel->getActiveSheet()->setCellValue("E" . ($no + 1), "Rp. " . number_format($total_step2,0,',','.'));

			$objPHPExcel->getActiveSheet()->getStyle("A4:E" . ($no + 1))->applyFromArray($tableBorder);

			$namaFile = "Laporan Jumlah Dana Terealisasi " . date('Ymd');

		} else if ( $category == "persentase-pembangunan" ) {
			
			$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
			$objPHPExcel->getActiveSheet()->setCellValue("A1", "Laporan Presentasi Pembangunan");
			$objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($center);

			$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
			$objPHPExcel->getActiveSheet()->setCellValue("A3", "No");

			$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
			$objPHPExcel->getActiveSheet()->setCellValue("B3", "Kecamatan");

			$objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
			$objPHPExcel->getActiveSheet()->setCellValue("C3", "Desa");

			$objPHPExcel->getActiveSheet()->mergeCells('D3:E3');
			$objPHPExcel->getActiveSheet()->setCellValue("D3", "Presentase Pembangunan");
			$objPHPExcel->getActiveSheet()->getStyle("D3")->applyFromArray($center);

			$objPHPExcel->getActiveSheet()->setCellValue("D4", "Tahap 1");
			$objPHPExcel->getActiveSheet()->getStyle("D4")->applyFromArray($center);

			$objPHPExcel->getActiveSheet()->setCellValue("E4", "Tahap 2");
			$objPHPExcel->getActiveSheet()->getStyle("E4")->applyFromArray($center);
			
			$no = 4;
			$number = 0;
			foreach ($get_table as $value) {
				$no++;
				$number++;
				$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, $number);
				$objPHPExcel->getActiveSheet()->setCellValue("B" . $no, $value['kec_name']);
				$objPHPExcel->getActiveSheet()->setCellValue("C" . $no, $value['desa_name']);
				$objPHPExcel->getActiveSheet()->setCellValue("D" . $no, $value['step1']. "%");
				$objPHPExcel->getActiveSheet()->setCellValue("E" . $no, $value['step2']. "%");
			}

			$objPHPExcel->getActiveSheet()->getStyle("A4:E" . ($no + 1))->applyFromArray($tableBorder);

			$namaFile = "Laporan Persentase Pembangunan " . date('Ymd');

		}

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'. $namaFile .'.xls"'); 
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output'); 
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/rcmadmin/Home.php */