<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

	var $url   = 'company';
	var $model = 'Model_company';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_company');
	}


	public function index()
	{

		$assets = array(
					'title' => "PENGATURAN DASAR > Intansi"
				 );

		$post = $this->input->post();
		if ( $post ) {

			$logo = upload_file( "Logo", "company/", "logo" );
			$image = upload_all_file( "Profile", "company/", "file" );
			
			if ( $logo != "default.jpg" ) {
				$post['logo'] = $logo;
			}

			if ( @$image['url'] != "default.jpg" ) {
				$post['file'] 	   = @$image['url'];
				$post['file_type'] = explode("/", @$image['file_type'])[0];
			}

			$post['created_by'] = $this->session->userdata('id');

			$check = $this->{$this->model}->find_by_id(1);
			if ( $check ) {
				$post['updated_at'] = date("Y-m-d h:i:s");
				
				$save = $this->{$this->model}->update($this->url, $post, array('id' => 1));
			} else {
				$save = $this->{$this->model}->add($this->url, $post);
			}

		}
        
        $data = array(
			"company" => $this->{$this->model}->find_by_id(1),
		);


		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/index', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);	
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/rcmadmin/Auth.php */