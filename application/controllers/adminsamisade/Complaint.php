<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends CI_Controller {

	var $url   = 'complaint';
	var $model = 'Model_complaint';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_complaint');
	}


	public function index()
	{

        $assets = array(
            "title_page" => "DAFTAR ADUAN MASYARAKAT"
        );
        $data = array(
            "list_complaint" => $this->{$this->model}->get_all(),
        );

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{
		
        $assets = array(
            "title_page" => "LIHAT ADUAN MASYARAKAT"
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {
			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"complaint" => $this->{$this->model}->get_detail($id),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}