<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends CI_Controller {

	var $url   			 = 'desa';
	var $model 			 = 'Model_desa';
	var $model_kecamatan = 'Model_kecamatan';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
	}


	public function index()
	{
		// cek role
		// check_role();

        $assets = array(
            "title_page" => "MASTER DATA > DAFTAR " . $this->url
		);
		
        $data = array(
            "list_desa" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
		// cek role
		// check_role();

        $assets = array(
            "title_page" => "MASTER DATA > TAMBAH " . $this->url
		);

		$data = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
		);
		
		$post = $this->input->post();
		if ( $post ) {
			$save = $this->{$this->model}->add($this->url, $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{
		// cek role
		// check_role();
		
        $assets = array(
            "title_page" => "MASTER DATA > LIHAT " . $this->url
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {
			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"desa" => $this->{$this->model}->get_detail($id),
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}