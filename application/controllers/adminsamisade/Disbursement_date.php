<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbursement_date extends CI_Controller {

	var $url   			 = 'disbursement_date';
	var $model 			 = 'Model_disbursement_date';
	var $model_desa 	 = 'Model_desa';
	var $model_kecamatan = 'Model_kecamatan';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_disbursement_date');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR TANGGAL PENCAIRAN DANA"
		);

        $data = array(
            "list_disbursement_date" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function all()
	{
        $assets = array(
            "title_page" => "DAFTAR SEMUA TANGGAL PENCAIRAN DANA"
		);

        $data = array(
            "list_disbursement_date" => $this->{$this->model}->get_all_disbursement_date(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_all_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function add()
	{
        $assets = array(
            "title_page" => "TAMBAH TANGGAL PENCAIRAN DANA"
		);

		$data = array(
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
		);
		
		$post = $this->input->post();
		if ( $post ) {
			$post['created_by'] = $this->session->userdata('id');

			$filter = array(
				"kec_id"  => $post['kec_id'],
				"desa_id" => $post['desa_id'],
				"step"    => $post['step'],
				"wave"    => $post['wave'],
			);
			$check = $this->{$this->model}->get_detail_by_data($filter);
			if ( $check ) {
				$this->session->set_flashdata('info', 'Data tersebut sudah pernah di input.');
			} else {
				$save = $this->{$this->model}->add('rab_' . $this->url, $post);
				if ( $save ) {
					$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
				} else {
					$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
				}
			}
		}

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "LIHAT TANGGAL PENCAIRAN DANA"
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {
			$post['created_by'] = $this->session->userdata('id');
			$post['updated_at'] = date("Y-m-d h:i:s");

			$save = $this->{$this->model}->update('rab_' . $this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"disbursement_date" => $this->{$this->model}->get_detail($id),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */