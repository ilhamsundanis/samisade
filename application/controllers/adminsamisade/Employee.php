<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	var $url   			= 'employee';
	var $model 			= 'Model_employee';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_employee');
	}


	public function index()
	{

        $assets = array();
		
		$rab_id = $this->uri->segment(4);
		
        $data = array(
            "list_employee" => $this->{$this->model}->get_all($rab_id),
		);

		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}
	
	public function add()
	{
        $assets = array();

		$rab_id = $this->uri->segment(4);
		$data = array(
			"list_rab_item" => $this->{$this->model}->get_rab_item_by_is_worker($rab_id),
		);

		$post = $this->input->post();
		if ( $post ) {
			$post['created_by'] = $this->session->userdata('id');
			$save = $this->{$this->model}->add("rab_item_employee", $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}
	
	public function view()
	{
		
        $assets = array();

		$rab_id = $this->uri->segment(4); 
		$id 	= $this->uri->segment(5); 
		
		$post = $this->input->post();
		if ( $post ) {
			$save = $this->{$this->model}->update("rab_item_employee", $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"detail" 	 	=> $this->{$this->model}->get_detail($id),
			"list_rab_item" => $this->{$this->model}->get_rab_item_by_is_worker($rab_id),
		);

		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);
		$this->load->view('adminsamisade/template/blank/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

	public function checknik()
    {
		$nik = $this->uri->segment(4);
		
        /* API URL */
        $url = 'http://103.51.103.61/sibos-api/public/index.php/penerima_bantuan/'. $nik .'?key=siboslagiapanih';
   
        /* Init cURL resource */
        $curl = curl_init($url);
   
		curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				// CURLOPT_TIMEOUT => 30000,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				// CURLOPT_HTTPHEADER => array(
				// // Set Here Your Requesred Headers
				// 	'Content-Type: application/json',
				// ),
			));
            
        /* execute request */
        $result = curl_exec($curl);
             
        /* close cURL resource */
        curl_close($curl);

		echo $result;
    }

}