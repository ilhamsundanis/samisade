<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Home extends CI_Controller {

	var $url   			 = 'home';
	var $model           = 'Model_home';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';
	var $model_proposals_submission = 'Model_proposals_submission';
	var $model_complaint = 'Model_complaint';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Model_home');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_desa');
		$this->load->model('Model_proposals_submission');
		$this->load->model('Model_complaint');
	}

	public function index()
	{
		// check_login();
		$assets = array();

		$list_all_data = array();
		$filter = array();

		if ( $this->session->userdata('role') == 2 ) {
			$filter['kec_id'] = $this->session->userdata('kec_id');
		} elseif ( $this->session->userdata('role') == 3 ) {
			$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
			$filter['desa_id'] = $this->session->userdata('desa_id');
		}

		$result_search = "Hasil Pencarian Berdasaran <br>";
		$post = $this->input->post();
		if ( $post ) {
			$filter = array(
				"kec_id"      => explode("-", @$post['kec_id'])[0],
				"desa_id"     => explode("-", @$post['desa_id'])[0],
				"activity_id" => explode("-", @$post['activity_id'])[0],
				"anggaran"    => $post['anggaran'],
			);

			if ( $this->session->userdata('role') == 2 ) {
				$filter['kec_id'] = $this->session->userdata('kec_id');
			} elseif ( $this->session->userdata('role') == 3 ) {
				$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
				$filter['desa_id'] = $this->session->userdata('desa_id');
			}

			$list_all_data = $this->{$this->model}->get_all_data($filter);

			if ( ! empty($post['kec_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Kecamatan: " . explode("-", @$post['kec_id'])[1] . "
									</span> ";
			}
	
			if ( ! empty($post['desa_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Desa: " . explode("-", @$post['desa_id'])[1] . "
									</span> ";
			}
	
			if ( ! empty($post['activity_id']) ) {
				$result_search .= "<span class=\"label label-info\">
										Jenis Pembangunan: " . explode("-", @$post['activity_id'])[1] . "
									</span> ";
			}

			if ( ! empty($post['anggaran']) ) {
				$temp = ($post['anggaran'] == "asc") ? "Terendah" : "Tertinggi";
				$result_search .= "<span class=\"label label-info\">
										Anggaran: " . $temp . "
									</span> ";
			}
		}

		$data = array(
			"list_kecamatan" 		   => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 		   => $this->{$this->model_desa}->get_all(),
			"list_activity"  		   => $this->{$this->model}->get_all_activity(),
			"get_chart_activities"     => json_encode($this->{$this->model}->get_chart_activities($filter)),
			"get_chart_top_total_cost" => json_encode($this->{$this->model}->get_chart_top_total_cost($filter)),
			"list_all_data"  		   => $list_all_data,
			"result_search"  		   => $result_search,
		);


		// pre($data['list_all_data']);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/index', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);	
	}

	public function popup()
	{
        $assets = array();

		$type   = urldecode($this->uri->segment(4));
		$point1 = urldecode($this->uri->segment(5));
		$point2 = urldecode($this->uri->segment(6));
		
        $data = array(
            "type"   => $type,
            "point1" => $point1,
            "point2" => $point2,
		);

		// pre($data);
		
		$this->load->view('adminsamisade/template/blank/header', $assets);	
		$this->load->view('adminsamisade/' . $this->url . '/popup/popup_maps', $data);	
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}

	public function get_all_desa_by_kecamatan()
	{
		$id   = explode("-", $this->uri->segment(4))[0];
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}

	public function get_chart_activities()
	{
		$data = $this->{$this->model}->get_chart_activities();

		echo json_encode($data);
	}

	public function get_chart_top_total_cost()
	{
		$data = $this->{$this->model}->get_chart_top_total_cost();

		echo json_encode($data);
	}

	public function export_data() {
		$proposal_id = $this->uri->segment(4);
		$rab_id = $this->uri->segment(5);
		$activity_id = $this->uri->segment(6);
		
		$alphas = range("A", "Z");

    	$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);

		$tableBorder = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000')
				)
			)
		);

		$center = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);	

		$resp 			= $this->{$this->model_proposals_submission}->get_detail_all($proposal_id, $rab_id, $activity_id);
		$resp_complaint = $this->{$this->model_complaint}->get_complaint_by_desa($resp['desa_id']);

		$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
		$objPHPExcel->getActiveSheet()->setCellValue("A1", "RENCANA ANGGARAN BIAYA");
		$objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($center);

		$objPHPExcel->getActiveSheet()->setCellValue("A3", "Provinsi");
		$objPHPExcel->getActiveSheet()->setCellValue("B3", ": Jawa Barat");
		$objPHPExcel->getActiveSheet()->setCellValue("G3", "Program");
		$objPHPExcel->getActiveSheet()->setCellValue("H3", "SAMISADE");

		$objPHPExcel->getActiveSheet()->setCellValue("A4", "Kabupaten");
		$objPHPExcel->getActiveSheet()->setCellValue("B4", ": Bogor");
		$objPHPExcel->getActiveSheet()->setCellValue("G4", "Jenis Kegiatan");
		$objPHPExcel->getActiveSheet()->setCellValue("H4", ": " . $resp['rab']['activity_name']);

		$objPHPExcel->getActiveSheet()->setCellValue("A5", "Kecamatan");
		$objPHPExcel->getActiveSheet()->setCellValue("B5", ": " . $resp['kec_name']);
		$objPHPExcel->getActiveSheet()->setCellValue("G5", "Ukuran/Dimensi");
		$objPHPExcel->getActiveSheet()->setCellValue("H5", ": " . $resp['rab']['dimension']);

		$objPHPExcel->getActiveSheet()->setCellValue("A6", "Desa");
		$objPHPExcel->getActiveSheet()->setCellValue("B6", ": " . $resp['desa_name']);

		$objPHPExcel->getActiveSheet()->setCellValue("A8", "No");		
		$objPHPExcel->getActiveSheet()->setCellValue("B8", "Uraian");		
		$objPHPExcel->getActiveSheet()->setCellValue("C8", "Volume");		
		$objPHPExcel->getActiveSheet()->setCellValue("D8", "Satuan");		
		$objPHPExcel->getActiveSheet()->setCellValue("E8", "Harga Satuan");		
		$objPHPExcel->getActiveSheet()->setCellValue("F8", "Total Harga");		
		$objPHPExcel->getActiveSheet()->setCellValue("G8", "Realisasi Tahap 1");		
		$objPHPExcel->getActiveSheet()->setCellValue("H8", "Realisasi Tahap 2");
		
		$no = 9;
		$grand_total = 0;
		$grand_total_step1 = 0;
		$grand_total_step2 = 0;
		foreach ( $resp['rab_item'] as $value ) {

			$objPHPExcel->getActiveSheet()->mergeCells('A' . $no . ':H' . $no);
			$objPHPExcel->getActiveSheet()->getStyle('A' . $no)->getFont()->setBold( true );
			$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, $value['category_name']);

			$number = 0;
			foreach ($value['item'] as $data) {
				$no = $no + 1;
				$number++;
				$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, $number);		
				$objPHPExcel->getActiveSheet()->setCellValue("B" . $no, $data['name']);		
				$objPHPExcel->getActiveSheet()->setCellValue("C" . $no, $data['volume']);		
				$objPHPExcel->getActiveSheet()->setCellValue("D" . $no, $data['unit']);		
				$objPHPExcel->getActiveSheet()->setCellValue("E" . $no, "Rp. " . number_format($data['unit_price'],0,',','.'));		
				$objPHPExcel->getActiveSheet()->setCellValue("F" . $no, "Rp. " . number_format($data['total_price'],0,',','.'));

				$total_realization_step1 = 0;
				$total_realization_step2 = 0;
				foreach ( $data['realization']['step1']['receipt'] as $step1 ) {
					if ($step1['status'] == "verified" || $step1['status'] == "done" ) {
						$total_realization_step1 += $step1['total_price'];
					}
				}	
				foreach ( $data['realization']['step2']['receipt'] as $step2 ) {
					if ($step2['status'] == "verified" || $step2['status'] == "done" ) {
						$total_realization_step2 += $step2['total_price'];
					}
				}		
				$objPHPExcel->getActiveSheet()->setCellValue("G" . $no, "Rp. " . number_format($total_realization_step1,0,',','.'));
				$objPHPExcel->getActiveSheet()->setCellValue("H" . $no, "Rp. " . number_format($total_realization_step2,0,',','.'));

				$grand_total += $data['total_price'];
				$grand_total_step1 += $total_realization_step1;
				$grand_total_step2 += $total_realization_step2;
			}
			$no++;
		}

		$objPHPExcel->getActiveSheet()->mergeCells('A' . $no . ':E' . $no);
		$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, "Grand Total: ");		
		$objPHPExcel->getActiveSheet()->setCellValue("F" . $no, "Rp. " . number_format($grand_total,0,',','.'));		
		$objPHPExcel->getActiveSheet()->setCellValue("G" . $no, "Rp. " . number_format($grand_total_step1,0,',','.'));		
		$objPHPExcel->getActiveSheet()->setCellValue("H" . $no, "Rp. " . number_format($grand_total_step2,0,',','.'));

		$objPHPExcel->getActiveSheet()->getStyle("A8:H" . $no)->applyFromArray($tableBorder);

		// complaint
		$cell_complaint = ($no + 2);
		$objPHPExcel->getActiveSheet()->mergeCells('A' . ($no + 2) . ':H' . ($no + 2));
		$objPHPExcel->getActiveSheet()->setCellValue("A" . ($no + 2), "DAFTAR ADUAN MASYARAKAT");
		$objPHPExcel->getActiveSheet()->getStyle("A" . ($no + 2))->applyFromArray($center);

		$objPHPExcel->getActiveSheet()->setCellValue("A" . ($no + 3), "No");		
		$objPHPExcel->getActiveSheet()->setCellValue("B" . ($no + 3), "Tanggal Aduan");
		$objPHPExcel->getActiveSheet()->setCellValue("C" . ($no + 3), "Nama Pelapor");		
		$objPHPExcel->getActiveSheet()->setCellValue("D" . ($no + 3), "Email Pelapor");		
		$objPHPExcel->getActiveSheet()->setCellValue("E" . ($no + 3), "No Telepon");
		
		$objPHPExcel->getActiveSheet()->mergeCells('F' . ($no + 3) . ':H' . ($no + 3));
		$objPHPExcel->getActiveSheet()->setCellValue("F" . ($no + 3), "Aduan");

		$no = $no + 4;
		$number_complaint = 0;
		foreach ( $resp_complaint as $data ) {
			$number_complaint++;
			$objPHPExcel->getActiveSheet()->setCellValue("A" . $no, $number_complaint);	
			$objPHPExcel->getActiveSheet()->setCellValue("B" . $no, $data['created_at']);	
			$objPHPExcel->getActiveSheet()->setCellValue("C" . $no, $data['name']);		
			$objPHPExcel->getActiveSheet()->setCellValue("D" . $no, $data['email']);		
			$objPHPExcel->getActiveSheet()->setCellValue("E" . $no, $data['phone']);	
			
			$objPHPExcel->getActiveSheet()->mergeCells('F' . $no . ':H' . $no);
			$objPHPExcel->getActiveSheet()->setCellValue("F" . $no, $data['description']);
			$no++;
		}

		$objPHPExcel->getActiveSheet()->getStyle("A" . $cell_complaint . ":H" . ($no - 1))->applyFromArray($tableBorder);

		$namaFile = "Laporan Realisasi " . date('Ymd');

		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'. $namaFile .'.xls"'); 
		header('Cache-Control: max-age=0'); //no cache
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output'); 
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/rcmadmin/Home.php */