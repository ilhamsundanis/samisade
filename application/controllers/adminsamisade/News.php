<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	var $url   = 'news';
	var $model = 'Model_news';
	var $type  = 'news';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_news');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR BERITA"
		);
		
        $data = array(
            "list_news" => $this->{$this->model}->get_all($this->type),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "TAMBAH BERITA"
		);
		
		$post = $this->input->post();
		if ( $post ) {

			$image = upload_file( "news" . date('Ymdhis'), "news/", "image" );
			
			if ( $image != "default.jpg" ) {
				$post['image'] = $image;
			}

			$post['created_by'] = $this->session->userdata('id');

			$save = $this->{$this->model}->add('news', $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "LIHAT BERITA"
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {

			$image = upload_file( "news" . date('Ymdhis'), "news/", "image" );
		
			if ( $image != "default.jpg" ) {
				$post['image'] = $image;
			} else {
				unset($post['image']);
			}

			$post['created_by'] = $this->session->userdata('id');
			$post['updated_at'] = date("Y-m-d h:i:s");

			$save = $this->{$this->model}->update('news', $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"news" => $this->{$this->model}->get_detail($id),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */