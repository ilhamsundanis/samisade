<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proposals_submission extends CI_Controller {

	var $url   			 = 'proposals_submission';
	var $model 			 = 'Model_proposals_submission';
	var $model_desa 	 = 'Model_desa';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_activity  = 'Model_activity';

	public function __construct()
	{
        parent::__construct();

		$this->load->model('Model_proposals_submission');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_activity');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR PENGAJUAN PROPOSAL"
		);

		$list_proposals_submission = array();
		$filter = array();

		if ( $this->session->userdata('role') == 2 ) {
			$filter['kec_id'] = $this->session->userdata('kec_id');

			$this->session->set_userdata('search_kec_id', $this->session->userdata('kec_id'));
		} elseif ( $this->session->userdata('role') == 3 ) {
			$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
			$filter['desa_id'] = $this->session->userdata('desa_id');

			$this->session->set_userdata('search_kec_id', get_kecamatan($this->session->userdata('desa_id'))['kec_id']);
			$this->session->set_userdata('search_desa_id', $this->session->userdata('desa_id'));
		}

		if ( !empty( $this->session->userdata('search_kec_id') ) ) {
			$filter['kec_id']  = $this->session->userdata('search_kec_id');
		}

		if ( !empty( $this->session->userdata('search_desa_id') ) ) {
			$filter['desa_id']  = $this->session->userdata('search_desa_id');
		}

		if ( !empty( $this->session->userdata('search_proposal') ) ) {
			$filter['proposal']  = $this->session->userdata('search_proposal');
		}

		$config['base_url']   = base_url().'adminsamisade/proposals_submission/index';
		$config['total_rows'] = $this->{$this->model}->count($filter);
        $config['per_page']   = 10;

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = 'PERTAMA';
        $config['last_link']        = 'TERAKHIR';
        $config['next_link']        = 'SELANJUTNYA';
        $config['prev_link']        = 'SEBELUMNYA';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>SELANJUTNYA</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        
        $from = $this->uri->segment(4);
		$list_proposals_submission = $this->{$this->model}->get_all($filter, $config['per_page'], $from);

		$result_search = "Hasil Pencarian Berdasaran <br>";
		$post = $this->input->post();
		if ( $post ) {

			if ( isset ( $post['filter'] ) ) {
				$filter = array(
					"kec_id"      => explode("-", @$post['kec_id'])[0],
					"desa_id"     => explode("-", @$post['desa_id'])[0],
					"proposal"    => $post['proposal'],
				);
	
				$this->session->set_userdata('search_kec_id', explode("-", @$post['kec_id'])[0]);
				$this->session->set_userdata('search_desa_id', explode("-", @$post['desa_id'])[0]);
				$this->session->set_userdata('search_proposal', $post['proposal']);
	
				if ( $this->session->userdata('role') == 2 ) {
					$filter['kec_id'] = $this->session->userdata('kec_id');
	
					$this->session->set_userdata('search_kec_id', $this->session->userdata('kec_id'));
					
				} elseif ( $this->session->userdata('role') == 3 ) {
					$filter['kec_id']  = get_kecamatan($this->session->userdata('desa_id'))['kec_id'];
					$filter['desa_id'] = $this->session->userdata('desa_id');
	
					$this->session->set_userdata('search_kec_id', get_kecamatan($this->session->userdata('desa_id'))['kec_id']);
					$this->session->set_userdata('search_desa_id', $this->session->userdata('desa_id'));
				}
	
				$config['total_rows'] = $this->{$this->model}->count($filter);
				$list_proposals_submission = $this->{$this->model}->get_all($filter, $config['per_page'], $from);
	
				if ( ! empty($post['kec_id']) ) {
					$result_search .= "<span class=\"label label-info\">
											Kecamatan: " . explode("-", @$post['kec_id'])[1] . "
										</span> ";
				}
		
				if ( ! empty($post['desa_id']) ) {
					$result_search .= "<span class=\"label label-info\">
											Desa: " . explode("-", @$post['desa_id'])[1] . "
										</span> ";
				}
		
				if ( ! empty($post['proposal']) ) {
					$result_search .= "<span class=\"label label-info\">
											Proposal: " . explode("-", @$post['proposal'])[1] . "
										</span> ";
				}
				$this->session->set_userdata('result_search', $result_search);
			}

			if ( isset ( $post['reset'] ) ) {
				$this->session->unset_userdata('search_kec_id');
				$this->session->unset_userdata('search_desa_id');
				$this->session->unset_userdata('proposal');
				$this->session->unset_userdata('result_search');
			}

			redirect('adminsamisade/proposals_submission');
			
		}
		
        $data = array(
			"list_kecamatan" 		    => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 		    => $this->{$this->model_desa}->get_all(),
            "list_proposals_submission" => $list_proposals_submission,
		);
		$this->pagination->initialize($config);	
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "TAMBAH PENGAJUAN PROPOSAL"
		);

		$post['desa_id'] = $this->session->userdata('desa_id');
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			$file = upload_file_pdf( "file" . date('Ymdhis'), "file/", "file" );
			if ( $file['error'] == false ) {
				$post['file'] 		= $file['url'];
				$post['status'] 	= "verification";
				$post['created_by'] = $this->session->userdata('id');

				$check = $this->{$this->model}->check_proposal($post['desa_id']);
				if ( $check > 0 ) {
					$this->session->set_flashdata('warning', 'Anda tidak boleh membuat proposal lebih dari 1.');
				} else {
					$save = $this->{$this->model}->add_last_id("proposal", $post);
					if ( $save ) {
						$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
						
						redirect('adminsamisade/proposals_submission/view/' . $save);
					} else {
						$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
					}
				}
			} else {
				$this->session->set_flashdata('warning', error_message_upload($file['message']));
				redirect('adminsamisade/proposals_submission/add');
			}
		}

		$data = array(
			"post"			 => $post,
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "LIHAT PENGAJUAN PROPOSAL"
		);

		$id = $this->uri->segment(4); 
		$queryUser = $this->{$this->model}->get_detail($id);

		$post['desa_id'] = $this->session->userdata('desa_id');
		
		$post = $this->input->post();
		if ( $post ) {
			$proposals_submission 			= $post;
			$proposals_submission['status'] = $queryUser['status'];
			$proposals_submission['note']   = $queryUser['note'];
			$proposals_submission['file']   = $queryUser['file'];
		} else {
			$proposals_submission = $queryUser;
		}

		
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			
			$file = upload_file_pdf( "file" . date('Ymdhis'), "file/", "file" );
			if ( $file['error'] == false ) {
				$post['file'] 		= $file['url'];

				if ( $proposals_submission['status'] == "pending" || $proposals_submission['status'] == "reject" ) {
					$post['status'] 	= "verification";
				}
				$post['created_by'] = $this->session->userdata('id');
				$post['updated_at'] = date("Y-m-d h:i:s");

				$save = $this->{$this->model}->update('proposal', $post, array('id' => $id));
				if ( $save ) {
					$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
					redirect('adminsamisade/proposals_submission/view/' . $id);
				} else {
					$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
				}
			} else {
				$this->session->set_flashdata('warning', error_message_upload($file['message']));
				redirect('adminsamisade/proposals_submission/view/' . $id);
			}
			
		}

		$data = array(
			"proposals_submission" 	=> $proposals_submission,
			"list_desa" 			=> $this->{$this->model_desa}->get_all(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function rab()
	{
        $assets = array(
            "title_page" => "INPUT RAB"
		);

		$id 	   				  = $this->uri->segment(4);
		$queryProposalsSubmission = $this->{$this->model}->get_detail($id);
		$desa_id			      = $queryProposalsSubmission['desa_id'];
		
		$tempPost = array();
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {

			$paramRAB = array(
				"proposal_id" => (int)$id,					
				"activity_id" => (int)explode("-", $post["activity_id"])[0],		
				"dimension"   => $post["dimension"],
				"total_cost"  => (float)$post["total_cost"],
				"created_by"  => (int)$this->session->userdata("id"),
			);

			$image1 = upload_file( "rab1" . date('Ymdhis'), "rab/", "image1" );
			if ( $image1 != "default.jpg" ) {
				$paramRAB['image1'] = $image1;
			}

			$image2 = upload_file( "rab2" . date('Ymdhis'), "rab/", "image2" );
			if ( $image2 != "default.jpg" ) {
				$paramRAB['image2'] = $image2;
			}

			$image3 = upload_file( "rab3" . date('Ymdhis'), "rab/", "image3" );
			if ( $image3 != "default.jpg" ) {
				$paramRAB['image3'] = $image3;
			}

			$image4 = upload_file( "rab4" . date('Ymdhis'), "rab/", "image4" );
			if ( $image4 != "default.jpg" ) {
				$paramRAB['image4'] = $image4;
			}

			$image5 = upload_file( "rab5" . date('Ymdhis'), "rab/", "image5" );
			if ( $image5 != "default.jpg" ) {
				$paramRAB['image5'] = $image5;
			}

			$check = $this->{$this->model}->check_rab($id);
			if ( ($check['all_total_cost'] + $paramRAB['total_cost']) > 1000000000 ) {
				$this->session->set_flashdata('warning', '
					Pengajuan anggaran RAB tidak boleh lebih dari 1 Milyar<br>
					Sisa anggaran RAB yang dapat di ajukan: Rp. ' . number_format($check['rest_total_cost'],0,',','.')
				);
			} else {
				$save = $this->{$this->model}->add_last_id("rab", $paramRAB);
				if ( $save ) {
					$paramRABLocation = array(
						"rab_id" 	 => (int)$save,
						"created_by" => (int)$this->session->userdata("id"),
					);
					if ( explode("-", $post["activity_id"])[1] == "maps_direction" ) {
						unset($paramRABLocation["point1"]);
						$paramRABLocation["point1"] = $post["point1_lat"] . "," . $post["point1_lng"];
						$paramRABLocation["point2"] = $post["point2_lat"] . "," . $post["point2_lng"];
					} else if ( explode("-", $post["activity_id"])[1] == "maps" ) {
						$paramRABLocation['point1'] = $post["point1"];
					}
					$this->{$this->model}->add("rab_location", $paramRABLocation);

					if ( isset( $post['name'] ) ) {
						foreach ( $post['name'] as $index => $value ) {
							foreach ( $value as $indexItem => $data) {
								$paramRABItem = array(
									"rab_id" 	  => (int)$save,				
									"category_id" => (int)$index,				
									"name" 	  	  => $data,		
									"unit" 		  => $post["unit"][$index][$indexItem],		
									"volume"      => (float)$post["volume"][$index][$indexItem],
									"unit_price"  => (float)$post["unit_price"][$index][$indexItem],
									"total_price" => (float)$post["total_price"][$index][$indexItem],
									"created_by"  => (int)$this->session->userdata("id"),
									"is_worker"   => $post["is_worker"][$index][$indexItem],
								);
		
								$saveRABItem = $this->{$this->model}->add("rab_item", $paramRABItem);
							}
						}

						if ( $saveRABItem ) {
							$this->session->set_flashdata('success', 'Berhasil meyimpan data.');
							
							redirect('adminsamisade/proposals_submission/rab_activity/' . $id . '/' . $save .'/'.explode("-", $post['activity_id'])[0]);
							
						} else {
							$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
						}
					}
				} else {
					$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
				}
			}
		}

		if ( $post ) {
			$post = $post;
		} else {
		}

		$data = array(
			"post"					=> $post,
			"detail"				=> $queryProposalsSubmission,
			"list_kecamatan" 		=> $this->{$this->model_kecamatan}->get_all(),
			"list_activity" 		=> $this->{$this->model_activity}->get_all(),
			"list_category" 		=> $this->{$this->model}->get_all_category($desa_id),
			"list_activity_by_desa" => $this->{$this->model}->get_all_activity($id, $desa_id),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/rab', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function rab_activity()
	{
        $assets = array(
            "title_page" => "INPUT RAB"
		);

		$id 	   				  = $this->uri->segment(4);
		$rab_id 	   			  = $this->uri->segment(5);
		$activity_id 	   		  = $this->uri->segment(6);
		$queryProposalsSubmission = $this->{$this->model}->get_detail($id);
		$desa_id			      = $queryProposalsSubmission['desa_id'];
		$detail_all				  = $this->{$this->model}->get_detail_all($id, $rab_id, $activity_id);
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			// unset($post['save']);
			// pre($post);

			$paramRAB = array(
				"proposal_id" => $id,					
				"activity_id" => $post["activity_id"],		
				"dimension"   => $post["dimension"],
				"total_cost"  => $post["total_cost"],
				"status"      => (@$post["verification"] == "yes") ? "verification" : "pending",
				"created_by"  => $this->session->userdata("id"),
			);

			$image1 = upload_file( "rab1" . date('Ymdhis'), "rab/", "image1" );
			if ( $image1 != "default.jpg" ) {
				$paramRAB['image1'] = $image1;
			}

			$image2 = upload_file( "rab2" . date('Ymdhis'), "rab/", "image2" );
			if ( $image2 != "default.jpg" ) {
				$paramRAB['image2'] = $image2;
			}

			$image3 = upload_file( "rab3" . date('Ymdhis'), "rab/", "image3" );
			if ( $image3 != "default.jpg" ) {
				$paramRAB['image3'] = $image3;
			}

			$image4 = upload_file( "rab4" . date('Ymdhis'), "rab/", "image4" );
			if ( $image4 != "default.jpg" ) {
				$paramRAB['image4'] = $image4;
			}

			$image5 = upload_file( "rab5" . date('Ymdhis'), "rab/", "image5" );
			if ( $image5 != "default.jpg" ) {
				$paramRAB['image5'] = $image5;
			}

			if ( $detail_all['rab']['status'] == "verified" || $detail_all['rab']['status'] == "done" ) {
				unset($paramRAB['status']);
			}

			$check = $this->{$this->model}->check_rab_by_rab_id($id, $rab_id);
			if ( ($check['all_total_cost'] + $paramRAB['total_cost']) > 1000000000 ) {
				$this->session->set_flashdata('warning', '
					Pengajuan anggaran RAB tidak boleh lebih dari 1 Milyar<br>
					Sisa anggaran RAB yang dapat di ajukan: Rp. ' . number_format($check['rest_total_cost'],0,',','.')
				);
			} else {

				$save = $this->{$this->model}->update("rab", $paramRAB, array('id' => $rab_id));
				if ( $save ) {

					if ( isset( $post['name'] ) ) {
						foreach ( $post['name'] as $index => $value ) {
							foreach ( $value as $indexItem => $data) {
								$paramRABItem = array(
									"rab_id" 	  => $rab_id,				
									"category_id" => $index,				
									"name" 	  	  => $data,		
									"unit" 		  => $post["unit"][$index][$indexItem],		
									"volume"      => (float)$post["volume"][$index][$indexItem],
									"unit_price"  => $post["unit_price"][$index][$indexItem],
									"total_price" => $post["total_price"][$index][$indexItem],
									"created_by"  => $this->session->userdata("id"),
									"is_worker"   => $post["is_worker"][$index][$indexItem],
								);
		
								$saveRABItem = $this->{$this->model}->add("rab_item", $paramRABItem);
							}
						}
					}

					if ( isset( $post['idUpdate'] ) ) {
						foreach ( $post['idUpdate'] as $index => $value ) {
							foreach ( $value as $indexItem => $data) {
								$paramRABItem = array(
									"rab_id" 	  => $rab_id,				
									"category_id" => $index,				
									"name" 	  	  => $post["nameUpdate"][$index][$indexItem],		
									"unit" 		  => $post["unitUpdate"][$index][$indexItem],		
									"volume"      => $post["volumeUpdate"][$index][$indexItem],
									"unit_price"  => $post["unit_priceUpdate"][$index][$indexItem],
									"total_price" => $post["total_priceUpdate"][$index][$indexItem],
									"created_by"  => $this->session->userdata("id"),
									"is_worker"   => $post["is_workerUpdate"][$index][$indexItem],
								);
		
								$saveRABItem = $this->{$this->model}->update("rab_item", $paramRABItem, array("id" => $post["idUpdate"][$index][$indexItem]));
							}
						}
					}

					$this->session->set_flashdata('success', 'Berhasil meyimpan data.');
					redirect('adminsamisade/proposals_submission/rab_activity/' . $id . '/' . $rab_id . '/' . $activity_id);
					
				} else {
					$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
				}
			}
		}

		if ( isset( $post['reject'] ) ) {
			$paramRAB = array(
				"status" 	  => "reject",
			);

			$this->{$this->model}->update("rab", $paramRAB, array("id" => $rab_id));
			$this->session->set_flashdata('success', 'Pengajuan RAB berhasil ditolak.');
			redirect('adminsamisade/proposals_submission/rab_activity/' . $id . '/' . $rab_id . '/' . $activity_id);
		}

		$data = array(
			"post"					=> $post,
			"detail"				=> $queryProposalsSubmission,
			"list_kecamatan" 		=> $this->{$this->model_kecamatan}->get_all(),
			"list_activity" 		=> $this->{$this->model_activity}->get_all(),
			"list_category" 		=> $this->{$this->model}->get_all_category($desa_id),
			"list_activity_by_desa" => $this->{$this->model}->get_all_activity($id, $desa_id),
			"detail_all" 			=> $detail_all,
		);

		$foto_location = array();
		array_push($foto_location, array("progress" => "0%", "foto" => $data['detail_all']['rab']['image1']));
		array_push($foto_location, array("progress" => "25%", "foto" => $data['detail_all']['rab']['image2']));
		array_push($foto_location, array("progress" => "50%", "foto" => $data['detail_all']['rab']['image3']));
		array_push($foto_location, array("progress" => "75%", "foto" => $data['detail_all']['rab']['image4']));
		array_push($foto_location, array("progress" => "100%", "foto" => $data['detail_all']['rab']['image5']));
		$data['foto_location'] = $foto_location;

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/rab_activity', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function rab_realization_step1()
	{
        $assets = array(
            "title_page" => "REALISASI RAB TAHAP 1"
		);

		$id 	   				  = $this->uri->segment(4);
		$rab_id 	   			  = $this->uri->segment(5);
		$activity_id 	   		  = $this->uri->segment(6);
		$queryProposalsSubmission = $this->{$this->model}->get_detail($id);
		$desa_id			      = $queryProposalsSubmission['desa_id'];

		$data = array(
			"detail"				=> $queryProposalsSubmission,
			"list_activity_by_desa" => $this->{$this->model}->get_all_activity($id, $desa_id),
			"detail_all" 			=> $this->{$this->model}->get_detail_all($id, $rab_id, $activity_id),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/rab_realization_step1', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function rab_realization_step2()
	{
        $assets = array(
            "title_page" => "REALISASI RAB TAHAP 2"
		);

		$id 	   				  = $this->uri->segment(4);
		$rab_id 	   			  = $this->uri->segment(5);
		$activity_id 	   		  = $this->uri->segment(6);
		$queryProposalsSubmission = $this->{$this->model}->get_detail($id);
		$desa_id			      = $queryProposalsSubmission['desa_id'];

		$data = array(
			"detail"				=> $queryProposalsSubmission,
			"list_activity_by_desa" => $this->{$this->model}->get_all_activity($id, $desa_id),
			"detail_all" 			=> $this->{$this->model}->get_detail_all($id, $rab_id, $activity_id),
		);

		// validate
		if ( check_percentage($data['detail_all']['rab']['id'], 1)['percentage'] < 75 ) {
			redirect('adminsamisade/proposals_submission/rab_realization_step1/' . $id . "/" . $rab_id . "/" . $activity_id);
		}

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/rab_realization_step2', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function popup_rab_gallery_activities()
	{
		$assets = array(
            "title_page" => "RAB GALERI"
		);

		$rab_id      = $this->uri->segment(4);
		$rab_item_id = $this->uri->segment(5);
		$step        = $this->uri->segment(6);

		$data = array(
			'list_gallery' => $this->{$this->model}->get_all_rab_gallery($rab_item_id, 'activities', $step),
			'rab_id'       => $rab_id,
			'rab_item_id'  => $rab_item_id,
			'step'         => $step,
		);
		
		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/' . $this->url . '/popup/popup_rab_gallery_activities', $data);	
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}

	public function popup_rab_gallery_receipt()
	{
		$assets = array(
            "title_page" => "RAB GALERI"
		);

		$rab_id      = $this->uri->segment(4);
		$rab_item_id = $this->uri->segment(5);
		$step        = $this->uri->segment(6);
		$total_price = $this->uri->segment(7);

		$data = array(
			'list_gallery' => $this->{$this->model}->get_all_rab_gallery($rab_item_id, 'receipt', $step),
			'rab_id'       => $rab_id,
			'rab_item_id'  => $rab_item_id,
			'step'         => $step,
			'total_price'  => $total_price,
		);

		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/' . $this->url . '/popup/popup_rab_gallery_receipt', $data);	
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}

	public function popup_rab_gallery_realization_activities()
	{
		$assets = array(
            "title_page" => "REALISASI RAB GALERI"
		);

		$rab_id      = $this->uri->segment(4);
		$rab_item_id = $this->uri->segment(5);
		$step        = $this->uri->segment(6);

		$data = array(
			'list_gallery' => $this->{$this->model}->get_all_rab_gallery_realization_activities($rab_item_id, $step)
		);
		
		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/' . $this->url . '/popup/popup_rab_gallery_realization_activities', $data);	
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

	public function delete_rab()
	{
		$id     = $this->uri->segment(4);
		$delete = $this->{$this->model}->delete_rab($id);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

	public function delete_rab_item()
	{
		$id     = $this->uri->segment(4);
		$delete = $this->{$this->model}->delete_rab_item($id);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

	public function get_all_desa_by_kecamatan()
	{
		$id   = $this->uri->segment(4);
		$data = $this->{$this->model}->get_all_desa_by_kecamatan($id);

		echo json_encode($data);
	}

	public function get_rab_item_by_category() 
	{
		$rab_id      = $this->uri->segment(4);
		$category_id = $this->uri->segment(5);
		$data        = $this->{$this->model}->get_rab_item_by_category($rab_id, $category_id);

		echo json_encode($data);
	}

	public function set_image_realization_activities() 
	{
		$rab_item_gallery_id = explode(",", rtrim($_POST['rab_item_gallery_id'], ","));
		foreach ( $rab_item_gallery_id as $val ) {
			$check = $this->{$this->model}->get_detail_rab_item_realization_activities($_POST["rab_item_id"], $_POST["step"], $val);
			if ( $check ) {
				$param = array(
					"rab_item_id" 		  => $_POST["rab_item_id"],			
					"step" 	  	  		  => $_POST["step"],
					"rab_item_gallery_id" => $val,
					"created_by"  		  => $this->session->userdata("id"),
				);

				$this->{$this->model}->update("rab_item_realization_activities", $param, $check['id']);
			} else {
				$param = array(
					"rab_item_id" 		  => $_POST["rab_item_id"],			
					"step" 	  	  		  => $_POST["step"],
					"rab_item_gallery_id" => $val,
					"created_by"  		  => $this->session->userdata("id"),
				);

				$this->{$this->model}->add("rab_item_realization_activities", $param);
			}
		}
		echo "success";
	}

	public function set_image_realization_receipt() 
	{
		$check = $this->{$this->model}->get_detail_rab_item_realization_receipt($_POST["rab_item_id"], $_POST["step"], $_POST["rab_item_gallery_id"]);
		if ( $check ) {
			$param = array(
				"rab_item_id" 		  => $_POST["rab_item_id"],			
				"step" 	  	  		  => $_POST["step"],
				"rab_item_gallery_id" => $_POST["rab_item_gallery_id"],
				"total_price" 		  => $_POST["total_price"],
				"status" 		  	  => "verification",
				"created_by"  => $this->session->userdata("id"),
			);

			$save = $this->{$this->model}->update("rab_item_realization_receipt", $param, array("id" => $check['id']));
			if ($save) {
				echo "success";
			} else {
				echo "failed";
			}
		} else {
			$param = array(
				"rab_item_id" 		  => $_POST["rab_item_id"],			
				"step" 	  	  		  => $_POST["step"],
				"rab_item_gallery_id" => $_POST["rab_item_gallery_id"],
				"total_price" 		  => $_POST["total_price"],
				"status" 		  	  => "verification",
				"created_by"  => $this->session->userdata("id"),
			);

			$save = $this->{$this->model}->add("rab_item_realization_receipt", $param);
			if ($save) {
				echo "success";
			} else {
				echo "failed";
			}
		}
	}

	// function send_data_realization() 
	// {
	// 	// pre($_POST);
	// 	$send = $this->{$this->model}->send_data_realization($_POST);
	// 	if ($send) {
	// 		echo "success";
	// 	} else {
	// 		echo "error";
	// 	}
	// }

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */