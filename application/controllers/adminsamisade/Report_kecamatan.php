<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_kecamatan extends CI_Controller {

	var $url   = 'report_kecamatan';
	var $model = 'Model_report_kecamatan';
	var $model_desa = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_report_kecamatan');
		$this->load->model('Model_desa');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "INPUT DATA > DAFTAR LAPORAN KECAMATAN"
		);
		
        $data = array(
            "list_report_kecamatan" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "INPUT DATA > TAMBAH LAPORAN KECAMATAN"
		);
		
		$post = $this->input->post();
		if ( $post ) {

			$image1 = upload_file( "dokumentasi1" . date('Ymdhis'), "report_kecamatan/", "image1" );
			if ( $image1 != "default.jpg" ) {
				$post['image1'] = $image1;
			} else {
				unset($post['image1']);
			}

			$image2 = upload_file( "dokumentasi2" . date('Ymdhis'), "report_kecamatan/", "image2" );
			if ( $image2 != "default.jpg" ) {
				$post['image2'] = $image2;
			} else {
				unset($post['image2']);
			}

			$image3 = upload_file( "dokumentasi3" . date('Ymdhis'), "report_kecamatan/", "image3" );
			if ( $image3 != "default.jpg" ) {
				$post['image3'] = $image3;
			} else {
				unset($post['image3']);
			}

			$image4 = upload_file( "dokumentasi4" . date('Ymdhis'), "report_kecamatan/", "image4" );
			if ( $image4 != "default.jpg" ) {
				$post['image4'] = $image4;
			} else {
				unset($post['image4']);
			}

			$image5 = upload_file( "dokumentasi5" . date('Ymdhis'), "report_kecamatan/", "image5" );
			if ( $image5 != "default.jpg" ) {
				$post['image5'] = $image5;
			} else {
				unset($post['image5']);
			}

			$post['created_by'] = $this->session->userdata('id');

			$save = $this->{$this->model}->add('report_kecamatan', $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"list_desa" => $this->{$this->model_desa}->get_all(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "INPUT DATA > LIHAT LAPORAN KECAMATAN"
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {

			$image1 = upload_file( "dokumentasi1" . date('Ymdhis'), "report_kecamatan/", "image1" );
			if ( $image1 != "default.jpg" ) {
				$post['image1'] = $image1;
			} else {
				unset($post['image1']);
			}

			$image2 = upload_file( "dokumentasi2" . date('Ymdhis'), "report_kecamatan/", "image2" );
			if ( $image2 != "default.jpg" ) {
				$post['image2'] = $image2;
			} else {
				unset($post['image2']);
			}

			$image3 = upload_file( "dokumentasi3" . date('Ymdhis'), "report_kecamatan/", "image3" );
			if ( $image3 != "default.jpg" ) {
				$post['image3'] = $image3;
			} else {
				unset($post['image3']);
			}

			$image4 = upload_file( "dokumentasi4" . date('Ymdhis'), "report_kecamatan/", "image4" );
			if ( $image4 != "default.jpg" ) {
				$post['image4'] = $image4;
			} else {
				unset($post['image4']);
			}

			$image5 = upload_file( "dokumentasi5" . date('Ymdhis'), "report_kecamatan/", "image5" );
			if ( $image5 != "default.jpg" ) {
				$post['image5'] = $image5;
			} else {
				unset($post['image5']);
			}

			$post['created_by'] = $this->session->userdata('id');
			$post['updated_at'] = date("Y-m-d h:i:s");

			$save = $this->{$this->model}->update('report_kecamatan', $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"report_kecamatan" => $this->{$this->model}->get_detail($id),
			"list_desa" => $this->{$this->model_desa}->get_all(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }

	public function popup()
	{
        $assets = array(
            "title_page" => "INPUT DATA > LAPORAN KECAMATAN"
		);

		$id = $this->uri->segment(4); 
		
        $data = array(
            "report_kecamatan" => $this->{$this->model}->get_detail($id),
		);
		
		$this->load->view('adminsamisade/template/blank/header', $assets);	
		$this->load->view('adminsamisade/' . $this->url . '/popup/view_report', $data);	
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */