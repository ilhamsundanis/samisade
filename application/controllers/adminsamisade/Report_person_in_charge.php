<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_person_in_charge extends CI_Controller {

	var $url   = 'report_person_in_charge';
	var $model = 'Model_report_person_in_charge';
	var $model_desa = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_report_person_in_charge');
		$this->load->model('Model_desa');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "INPUT DATA > DAFTAR LAPORAN PENANGGUNG JAWAB"
		);
		
        $data = array(
            "list_report_person_in_charge" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "INPUT DATA > TAMBAH LAPORAN PENANGGUNG JAWAB"
		);
		
		$post = $this->input->post();
		if ( $post ) {

			$file = upload_file_pdf( "lpj" . date('Ymdhis'), "report_person_in_charge/", "file" );
			if ( $file['error'] == false ) {
				$post['file'] 		= $file['url'];
				$post['created_by'] = $this->session->userdata('id');

				$save = $this->{$this->model}->add('report_person_in_charge', $post);
				if ( $save ) {
					$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
				} else {
					$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
				}
			} else {
				$this->session->set_flashdata('warning', error_message_upload($file['message']));
				redirect('adminsamisade/report_person_in_charge/add');
			}
		}

		$data = array(
            "list_proposal" => $this->{$this->model}->get_all_proposal(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "INPUT DATA > LIHAT LAPORAN PENANGGUNG JAWAB"
		);

		$id = $this->uri->segment(4); 
		
		$post = $this->input->post();
		if ( $post ) {

			$file = upload_file_pdf( "lpj" . date('Ymdhis'), "report_person_in_charge/", "file" );
			if ( $file['error'] == false ) {
				$post['file'] 		= $file['url'];

				$post['created_by'] = $this->session->userdata('id');
				$post['updated_at'] = date("Y-m-d h:i:s");

				$save = $this->{$this->model}->update('report_person_in_charge', $post, array('id' => $id));
				if ( $save ) {
					$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
				} else {
					$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
				}
			} else {
				$this->session->set_flashdata('warning', error_message_upload($file['message']));
				redirect('adminsamisade/report_person_in_charge/view/' . $id);
			}
		}

		if ( $this->session->userdata('role') == 1 ) {
			$disabled = "disabled";
		} else {
			$disabled = "";
		}

		$data = array(
			"report_person_in_charge" => $this->{$this->model}->get_detail($id),
			"list_proposal" 		  => $this->{$this->model}->get_all_proposal(),
			"disabled"				  => $disabled,
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }

	public function popup()
	{
        $assets = array(
            "title_page" => "INPUT DATA > LAPORAN PENANGGUNG JAWAB"
		);

		$id = $this->uri->segment(4); 
		
        $data = array(
            "report_person_in_charge" => $this->{$this->model}->get_detail($id),
		);
		
		$this->load->view('adminsamisade/template/blank/header', $assets);	
		$this->load->view('adminsamisade/' . $this->url . '/popup/view_report', $data);	
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */