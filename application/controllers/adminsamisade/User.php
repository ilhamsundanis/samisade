<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	var $url   			 = 'user';
	var $model 			 = 'Model_user';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_desa 	 = 'Model_desa';

	public function __construct()
	{
		parent::__construct();
		// check_login();
		$this->load->model('Model_user');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR PENGGUNA"
		);
		
        $data = array(
            "list_user" => $this->{$this->model}->get_all(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function add()
	{
        $assets = array(
            "title_page" => "TAMBAH PENGGUNA"
		);
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			$post['password']   = md5($post['password']);
			$post['created_by'] = $this->session->userdata('id');

			$filter = array(
				"email" => $post['email']
			);
			$check = $this->{$this->model}->get_detail_by_post($filter);
			if ( $check ) {
				$this->session->set_flashdata('warning', 'Email yang anda input sudah di gunakan, silahkan menggunakan email yang lain.');
			} else {
				$save = $this->{$this->model}->add($this->url, $post);
				if ( $save ) {
					$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
					redirect(base_url() . 'adminsamisade/user/add');
				} else {
					$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
				}
			}
		}

		$data = array(
			"post"			 => $post,
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
			'user_role' 	 => user_role(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/add_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "LIHAT PENGGUNA"
		);

		$id = $this->uri->segment(4); 
		$queryUser = $this->{$this->model}->get_detail($id);
		
		$post = $this->input->post();
		if ( $post ) {
			$user = $post;
		} else {
			$user = $queryUser;
		}
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			$post['password']   = md5($post['password']);
			$post['created_by'] = $this->session->userdata('id');
			$post['updated_at'] = date("Y-m-d h:i:s");

			if ( empty( $post['password'] ) ) {
				unset($post['password']);
			}

			$save = $this->{$this->model}->update($this->url, $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
				redirect(base_url() . 'adminsamisade/user/view/' . $id);
				
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"user" 			 => $user,
			"list_kecamatan" => $this->{$this->model_kecamatan}->get_all(),
			"list_desa" 	 => $this->{$this->model_desa}->get_all(),
			'user_role' 	 => user_role(),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/' . $this->url . '/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
    }
    
    public function delete()
	{
		$delete = $this->{$this->model}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */