<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proposal extends CI_Controller {

	var $url   			 = 'proposals_submission';
	var $model 			 = 'Model_proposals_submission';
	var $model_desa 	 = 'Model_desa';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_activity  = 'Model_activity';

	public function __construct()
	{
        parent::__construct();
        
		$this->load->model('Model_proposals_submission');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_activity');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR PROPOSAL"
		);
		
        $data = array(
            "list_proposals_submission" => $this->{$this->model}->get_all_proposal(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/verification/proposal/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "LIHAT PROPOSAL"
		);

		$id = $this->uri->segment(5); 
		$queryUser = $this->{$this->model}->get_detail($id);
		
		$post = $this->input->post();
		if ( $post ) {
			$proposals_submission = $post;
		} else {
			$proposals_submission = $queryUser;
		}
		
		if ( isset( $post['save'] ) ) {
			unset($post['save']);

			$post['updated_at'] = date("Y-m-d h:i:s");
			$save = $this->{$this->model}->update('proposal', $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
                redirect('adminsamisade/verification/proposal');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"proposals_submission" 	=> $proposals_submission,
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/verification/proposal/view_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */