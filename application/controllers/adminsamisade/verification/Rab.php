<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rab extends CI_Controller {

	var $url   			 = 'rab';
	var $model 			 = 'Model_proposals_submission';
	var $model_desa 	 = 'Model_desa';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_activity  = 'Model_activity';
	var $model_employee  = 'Model_employee';

	public function __construct()
	{
        parent::__construct();
        
		$this->load->model('Model_proposals_submission');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_activity');
		$this->load->model('Model_employee');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR INPUT RAB"
		);
		
        $data = array(
            "list_proposals_submission" => $this->{$this->model}->get_all_rab(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/verification/rab/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view()
	{	
        $assets = array(
            "title_page" => "LIHAT INPUT RAB"
		);

		$id 	   				  = $this->uri->segment(5);
		$rab_id 	   			  = $this->uri->segment(6);
		$activity_id 	   		  = $this->uri->segment(7);
		$queryProposalsSubmission = $this->{$this->model}->get_detail($id);
		$desa_id			      = $queryProposalsSubmission['desa_id'];
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			// unset($post['save']);
			// pre($post);

			$paramRAB = array(
				"status"      => $post["status"],
				"note"        => $post["note"],
				"created_by"  => $this->session->userdata("id"),
			);

			$save = $this->{$this->model}->update("rab", $paramRAB, array('id' => $rab_id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil meyimpan data.');
				redirect('adminsamisade/verification/rab');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			// "list_activity" 		=> $this->{$this->model_activity}->get_all(),
			"list_category" 		=> $this->{$this->model}->get_all_category($desa_id),
			"list_activity_by_desa" => $this->{$this->model}->get_all_activity($id, $desa_id),
			"detail_all" 			=> $this->{$this->model}->get_detail_all($id, $rab_id, $activity_id),
		);

		$foto_location = array();
		array_push($foto_location, array("progress" => "0%", "foto" => $data['detail_all']['rab']['image1']));
		array_push($foto_location, array("progress" => "25%", "foto" => $data['detail_all']['rab']['image2']));
		array_push($foto_location, array("progress" => "50%", "foto" => $data['detail_all']['rab']['image3']));
		array_push($foto_location, array("progress" => "75%", "foto" => $data['detail_all']['rab']['image4']));
		array_push($foto_location, array("progress" => "100%", "foto" => $data['detail_all']['rab']['image5']));
		$data['foto_location'] = $foto_location;

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/verification/rab/rab_activity', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function popup_employee()
	{

        $assets = array();
		
		$rab_item_id = $this->uri->segment(5);
		
        $data = array(
            "list_employee" => $this->{$this->model_employee}->get_all_by_rab_item($rab_item_id),
		);

		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/verification/popup/popup_employee', $data);
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}

	public function popup_add_employee()
	{

        $assets = array();
		$rab_item_id = $this->uri->segment(5);
		$data = array(
			"list_rab_item" => $this->{$this->model_employee}->get_rab_item_id_by_is_worker($rab_item_id),
		);

		$post = $this->input->post();
		if ( $post ) {
			$post['created_by'] = $this->session->userdata('id');
			$save = $this->{$this->model_employee}->add("rab_item_employee", $post);
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/verification/popup/popup_add_employee', $data);
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}

	public function popup_view_employee()
	{

        $assets = array();
		$rab_item_id = $this->uri->segment(5); 
		$id 		 = $this->uri->segment(6); 
		
		$post = $this->input->post();
		if ( $post ) {
			$save = $this->{$this->model_employee}->update("rab_item_employee", $post, array('id' => $id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil menyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"detail" 	 	=> $this->{$this->model_employee}->get_detail($id),
			"list_rab_item" => $this->{$this->model_employee}->get_rab_item_id_by_is_worker($rab_item_id),
		);

		$this->load->view('adminsamisade/template/blank/header', $assets);
		$this->load->view('adminsamisade/verification/popup/popup_view_employee', $data);
		$this->load->view('adminsamisade/template/blank/footer', $assets);
	}

	public function delete()
	{
		$delete = $this->{$this->model_employee}->delete($_POST['id']);
		if ($delete) {
			echo "success";
		} else {
			echo "error";
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */