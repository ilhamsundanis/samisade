<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realization extends CI_Controller {

	var $url   			 = 'realization';
	var $model 			 = 'Model_proposals_submission';
	var $model_desa 	 = 'Model_desa';
	var $model_kecamatan = 'Model_kecamatan';
	var $model_activity  = 'Model_activity';

	public function __construct()
	{
        parent::__construct();
        
		$this->load->model('Model_proposals_submission');
		$this->load->model('Model_desa');
		$this->load->model('Model_kecamatan');
		$this->load->model('Model_activity');
	}


	public function index()
	{
        $assets = array(
            "title_page" => "DAFTAR REALISASI RAB"
		);
		
        $data = array(
            "list_proposals_submission" => $this->{$this->model}->get_all_realization(),
		);
		
		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/verification/realization/list_' . $this->url, $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}
	
	public function view_step1()
	{	
        $assets = array(
            "title_page" => "LIHAT REALISASI RAB"
		);

		$id 	   				  = $this->uri->segment(5);
		$rab_id 	   			  = $this->uri->segment(6);
		$activity_id 	   		  = $this->uri->segment(7);
		$queryProposalsSubmission = $this->{$this->model}->get_detail($id);
		$desa_id			      = $queryProposalsSubmission['desa_id'];
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			$save = $this->{$this->model}->update('rab', array('step1_notes' => $post['note']), array('id' => $rab_id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil meyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"detail"				=> $queryProposalsSubmission,
			"list_activity_by_desa" => $this->{$this->model}->get_all_activity($id, $desa_id),
			"detail_all" 			=> $this->{$this->model}->get_detail_all($id, $rab_id, $activity_id),
		);

		// pre($data['detail_all']);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/verification/realization/rab_realization_step1', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	public function view_step2()
	{	
        $assets = array(
            "title_page" => "LIHAT REALISASI RAB"
		);

		$id 	   				  = $this->uri->segment(5);
		$rab_id 	   			  = $this->uri->segment(6);
		$activity_id 	   		  = $this->uri->segment(7);
		$queryProposalsSubmission = $this->{$this->model}->get_detail($id);
		$desa_id			      = $queryProposalsSubmission['desa_id'];
		
		$post = $this->input->post();
		if ( isset( $post['save'] ) ) {
			$save = $this->{$this->model}->update('rab', array('step2_notes' => $post['note']), array('id' => $rab_id));
			if ( $save ) {
				$this->session->set_flashdata('success', 'Berhasil meyimpan data.');
			} else {
				$this->session->set_flashdata('warning', 'Gagal meyimpan data.');
			}
		}

		$data = array(
			"detail"				=> $queryProposalsSubmission,
			"list_activity_by_desa" => $this->{$this->model}->get_all_activity($id, $desa_id),
			"detail_all" 			=> $this->{$this->model}->get_detail_all($id, $rab_id, $activity_id),
		);

		$this->load->view('adminsamisade/template/home/header', $assets);		
		$this->load->view('adminsamisade/template/home/menu');		
		$this->load->view('adminsamisade/verification/realization/rab_realization_step2', $data);	
		$this->load->view('adminsamisade/template/home/footer', $assets);
	}

	function verified_realization() {
		$resp  = array();
		$param = array(
			"status" => $_POST['status']
		);
		$save = $this->{$this->model}->update("rab_item_realization_receipt", $param, array("id" => $_POST["rab_item_realization_id"]));
		if ( $save ) {
			if ( $_POST['status'] == 'verified' ) {
				$paramRAB = array();
				if ( $_POST['step'] == 1 ) {
					$paramRAB['step1_percentage'] = check_percentage_realization($_POST['rab_id'], $_POST['step'])['percentage'];
					$paramRAB['step1_status']     = (check_percentage_realization($_POST['rab_id'], $_POST['step'])['percentage'] >= 75) ? "verified" : "pending";
					$paramRAB['step1_balance']    = check_percentage_realization($_POST['rab_id'], $_POST['step'])['saldo'];
				} else {
					$paramRAB['step2_percentage'] = check_percentage_realization($_POST['rab_id'], $_POST['step'])['percentage'];
					$paramRAB['step2_status']     = (check_percentage_realization($_POST['rab_id'], $_POST['step'])['percentage'] == 100) ? "verified" : "pending";
					$paramRAB['step2_balance']    = check_percentage_realization($_POST['rab_id'], $_POST['step'])['saldo'];
				}

				$step1 = check_percentage_realization($_POST['rab_id'], 1)['percentage'];
				$step2 = check_percentage_realization($_POST['rab_id'], 2)['percentage'];
				if ( $step1 == 100 && $step2 == 100 ) {
					$paramRAB['step1_status'] = "done";
					$paramRAB['step2_status'] = "done";
					$paramRAB['status'] = "done";
				}
				
				$this->{$this->model}->update("rab", $paramRAB, array("id" => $_POST["rab_id"]));
			}
			$resp['status'] 					   = "success";
			$resp['data']['status'] 			   = parsing_status()[$_POST['status']];
			$resp['data']['percentage'] 		   = check_percentage_realization($_POST['rab_id'], $_POST['step'])['percentage'];
			$resp['data']['grandtotal_pembayaran'] = check_percentage_realization($_POST['rab_id'], $_POST['step'])['grandtotal_pembayaran'];
			$resp['data']['saldo']   			   = check_percentage_realization($_POST['rab_id'], $_POST['step'])['saldo'];
			echo json_encode($resp);
		} else {
			$resp['status'] 		= "failed";
			$resp['data']['status'] = parsing_status()[$_POST['status']];
			echo json_encode($resp);
		}
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/adminsamisade/Auth.php */