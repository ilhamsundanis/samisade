<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// display raw data
if ( ! function_exists('pre') ) {
	function pre( $array = array() ) {
        echo '<pre>';
        	print_r($array);
        echo '<pre>';
        exit;
    }
}

if ( ! function_exists('unexit_pre') ) {
	function unexit_pre( $variable ) {
		echo '<pre>';
		echo print_r( $variable );
		echo '</pre>';
	}
}

if ( ! function_exists('month') ) {
	function month() {
		$bulan = array (
			"01" => 'Januari',
			"02" => 'Februari',
			"03" => 'Maret',
			"04" => 'April',
			"05" => 'Mei',
			"06" => 'Juni',
			"07" => 'Juli',
			"08" => 'Agustus',
			"09" => 'September',
			"10" => 'Oktober',
			"11" => 'November',
			"12" => 'Desember'
		);
		return $bulan;
	}
}

// if ( ! function_exists('encrypt') ) {
// 	function encrypt( $string, $action = 'e' ) {

// 	    // you may change these values to your own
// 	    $secret_key = md5('rentist');
// 	    $secret_iv  = md5('rentist spesialist indonesia');
	 
// 	    $output         = false;
// 	    $encrypt_method = "AES-256-CBC";
// 	    $key            = hash( 'sha256', $secret_key );
// 	    $iv             = substr( hash( 'sha256', $secret_iv ), 0, 16 );
	 
// 	    if( $action == 'e' ) {
// 	        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
// 	    }
// 	    else if( $action == 'd' ){
// 	        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
// 	    }
	 
// 	    return $output;
// 	}
// }

if ( ! function_exists('alert') ) {
	function alert() {
		$ci =& get_instance();
		
		if ( $ci->session->flashdata('success') ) {
			echo "<div class='alert alert-success alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('success');
			echo "</div>";
		} elseif ( $ci->session->flashdata('info') ) {
			echo "<div class='alert alert-info alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('info');
			echo "</div>";
        } elseif ( $ci->session->flashdata('warning') ) {
			echo "<div class='alert alert-warning alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('warning');
			echo "</div>";
		} elseif ( $ci->session->flashdata('danger') ) {
			echo "<div class='alert alert-danger alert-dismissable'>";
			    echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
			    echo "<i data-icon='G' class='linea-icon linea-basic fa-fw'></i>";
				echo $ci->session->flashdata('danger');
			echo "</div>";
		}
	}
}

if ( ! function_exists('date_format_indo') ) {
	function date_format_indo($date, $print_day = false, $time = false){
		if ( ! empty($date) AND $date != "0000-00-00 00:00:00" AND $date != "0000-00-00" ) {
			// pre(ok);
			$day = array ( 1 =>    'Senin',
						'Selasa',
						'Rabu',
						'Kamis',
						'Jumat',
						'Sabtu',
						'Minggu'
					);
					
			$bulan = array (1 =>   'Januari',
						'Februari',
						'Maret',
						'April',
						'Mei',
						'Juni',
						'Juli',
						'Agustus',
						'September',
						'Oktober',
						'November',
						'Desember'
					);
			$split 	  = explode('-', $date);
			$date_indonesia = substr($split[2], 0, 2) . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
			
			if ($print_day) {
				$num = date('N', strtotime($date));
				if ($time) {
					return $day[$num] . ', ' . $date_indonesia . ' ' . date('H:i', strtotime($date));
				} else {
					return $day[$num] . ', ' . $date_indonesia;
				}
			}
			return $date_indonesia;
		}
	}
}

// if ( ! function_exists('role') ) {
// 	function role( $id_role ){

// 		$result = "";
// 		if ( $id_role == 1 ) {
// 			$result = "Administrasi";
// 		} elseif ($id_role == 2) {
// 			$result = "Petugas Kecamatan";
// 		} elseif ($id_role == 3) {
// 			$result = "Petugas Desa";
// 		}

// 		return $result;
// 	}
// }


if ( ! function_exists('swal') ) {
	function swal( $message, $type, $url = "" ) {
		$ci =& get_instance();

		$temp = "<script type='text/javascript'>
				  	 $(document).ready(function() {
				  	 	swal('" . $message . "', {
			                icon: '" . $type . "',
			                buttons: false,
			                timer: 2000,
			            });
			            setTimeout(function() {
                      
		                    if(url == ''){
		                    	location.reload(false);
		                    } else {
		                	    window.location = url;    
		                    }
		                      
		                }, 2000);
				  	 });
				  </script>";

		echo $temp;
	}
}

// if ( ! function_exists('get_fee_asset_category') ) {
// 	function get_fee_asset_category( $id_asset_category ) {
		
// 		$ci = get_instance();

// 		$ci->db->where('id_asset_category', $id_asset_category);
// 		$query = $ci->db->get('tb_fee_general')->row_array();

// 		return $query['partner_percentage'];

// 	}
// }

if ( ! function_exists('upload_file') ) {
	function upload_file( $name_file, $path, $field_name ) {
		
		$ci = get_instance();

		$config['upload_path']          = './images/' . $path;
		$config['allowed_types']        = '*';
		$config['file_name']            = $name_file;
		$config['overwrite']			= true;
		// $config['max_size']             = 1024; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$ci->load->library('upload', $config);
		$ci->upload->initialize($config);
		if ($ci->upload->do_upload($field_name)) {
			return base_url() . 'images/' . $path . $ci->upload->data("file_name");
		} else {
			// return $ci->upload->display_errors();
		}
		
		return "default.jpg";

	}
}

if ( ! function_exists('upload_file_pdf') ) {
	function upload_file_pdf( $name_file, $path, $field_name ) {
		
		$ci = get_instance();

		$config['upload_path']          = './images/' . $path;
		$config['allowed_types']        = 'pdf';
		$config['file_name']            = $name_file;
		$config['overwrite']			= true;
		$config['max_size']             = 1024*6; // 6MB

		$ci->load->library('upload', $config);
		$ci->upload->initialize($config);
		if ($ci->upload->do_upload($field_name)) {
			$data = array(
				"error"   => false,
				"message" => "Success",
				"url"     => base_url() . 'images/' . $path . $ci->upload->data("file_name"),
			);
		} else {
			$data = array(
				"error"   => true,
				"message" => $ci->upload->display_errors(),
				"url"     => "",
			);
		}
		
		return $data;

	}
}

if ( ! function_exists('upload_all_file') ) {
	function upload_all_file( $name_file, $path, $field_name ) {
		
		$ci = get_instance();

		$config['upload_path']          = './images/' . $path;
		$config['allowed_types']        = '*';
		$config['file_name']            = $name_file;
		$config['overwrite']			= true;
		// $config['max_size']             = 1024; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$ci->load->library('upload', $config);
		$ci->upload->initialize($config);
		if ($ci->upload->do_upload($field_name)) {
			$temp 		 = $ci->upload->data();
			$temp['url'] = base_url() . 'images/' . $path . $ci->upload->data("file_name");
			return $temp;
			// return base_url() . 'images/' . $path . $ci->upload->data("file_name");
		} else {
			// return $ci->upload->display_errors();
			$temp['url'] = "default.jpg";
		}
		
		return $temp;

	}
}

if ( ! function_exists('check_login') ) {
	function check_login(){
		$ci =& get_instance();
		
		$session = $ci->session->userdata('id');
		if ( !$session ) {
			redirect('adminsamisade/auth');
		} else {
			// if ( $ci->uri->segment(2) == 'auth' ) {
			// 	redirect('rcmadmin/home');
			// }
		}
	}
}

if ( ! function_exists('user_role') ) {
	function user_role(){
		$ci =& get_instance();

		if ( $ci->session->userdata('role') == '1' ) {
			$result = array(
				"1" => "ADMINISTRATOR",
				"2" => "KECAMATAN",
				"3" => "DESA",
			);
		} else {
			$result = array(
				"3" => "DESA",
			);
		}
		
		return $result;
	}
}

if ( ! function_exists('parsing_status') ) {
	function parsing_status(){
		$ci =& get_instance();

		$result = array(
			"pending" 	   => "Belum Diverifikasi",
			"verification" => "Sedang Diverifikasi",
			"verified"     => "Berhasil Diverifikasi",
			"reject"       => "Pengajuan Ditolak",
			"done"         => "Selesai",
		);
		
		return $result;
	}
}

if ( ! function_exists('parsing_status_class') ) {
	function parsing_status_class(){
		$ci =& get_instance();

		$result = array(
			"pending" 	   => "danger",
			"verification" => "info",
			"verified"     => "success",
			"done"         => "success",
			"reject"       => "danger",
		);
		
		return $result;
	}
}

if ( ! function_exists('count_verification') ) {
	function count_verification(){
		$ci =& get_instance();

		// proposal
		if ( $ci->session->userdata('role') == 2 ) {
			$ci->db->where('desa.kec_id', $ci->session->userdata('kec_id'));
		}
		$ci->db->where("status", "verification");
		$ci->db->select('proposal.*, desa.name as desa_name');
		$ci->db->join('desa', 'proposal.desa_id = desa.id');
		$query = $ci->db->get("proposal")->result_array();

		// rab
		if ( $ci->session->userdata('role') == 2 ) {
			$ci->db->where('desa.kec_id', $ci->session->userdata('kec_id'));
		}
		$ci->db->where("rab.status", "verification");
		$ci->db->select("rab.*, proposal.title, activity.name as activity_name, desa.name as desa_name");
		$ci->db->join("activity", "rab.activity_id = activity.id");
		$ci->db->join('proposal', 'rab.proposal_id = proposal.id');
		$ci->db->join('desa', 'proposal.desa_id = desa.id');
		$queryRAB = $ci->db->get("rab")->result_array();

		// rab_item_realization
		if ( $ci->session->userdata('role') == 2 ) {
			$ci->db->where('desa.kec_id', $ci->session->userdata('kec_id'));
		}
		$ci->db->where('rab_item_realization_receipt.status', 'verification');
		$ci->db->select('rab_item_realization_receipt.step, rab_item.rab_id');
		$ci->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
		$ci->db->join('rab', 'rab_item.rab_id = rab.id');
		$ci->db->join('proposal', 'rab.proposal_id = proposal.id');
		$ci->db->join('desa', 'proposal.desa_id = desa.id');
		$ci->db->group_by('rab_item_realization_receipt.step, rab_item.rab_id');
		$queryRABItemRealization = $ci->db->get("rab_item_realization_receipt")->result_array();

		$result = array(
			"proposal" 			   => count($query),
			"rab"       		   => count($queryRAB),
			"rab_item_realization" => count($queryRABItemRealization),
			"total"                => count($query) + count($queryRAB) + count($queryRABItemRealization),
		);
		
		return $result;
	}
}

if ( ! function_exists('check_percentage') ) {
	function check_percentage( $rab_id, $step ){
		$ci =& get_instance();

		$ci->db->where('id', $rab_id);
		$query = $ci->db->get('rab')->row_array();
		if ( $step == 1 ) {
			$total_cost = ($query['total_cost'] * 0.4);
		} else {
			$total_cost = ($query['total_cost'] * 0.6);
		}
	
		$ci->db->where('rab_item.rab_id', $rab_id);
		$ci->db->where('rab_item_realization_receipt.step', $step);
		$ci->db->where_in('rab_item_realization_receipt.status', array("verified", "done"));
		$ci->db->select('rab_item_realization_receipt.total_price');
		$ci->db->join('rab_item_realization_receipt', 'rab_item.id = rab_item_realization_receipt.rab_item_id');
		$queryRABItem = $ci->db->get('rab_item')->result_array();
		$total_price = 0;
		foreach ( $queryRABItem as $data ) {
			$total_price += $data['total_price'];
		}

		@$percentage = ($total_price * 100) / $total_cost;
		if ( $percentage <= 1 ) {
			$percentage = 0;
		} elseif ( $percentage > 100 ) {
			$percentage = 100;
		}

		$resp = array(
			"percentage" 	        => number_format($percentage, 2, '.', ''),
			"grandtotal_pembayaran" => number_format($total_price, 0,',','.'),
		);
		return $resp;
	}
}

if ( ! function_exists('check_percentage_realization') ) {
	function check_percentage_realization( $rab_id, $step ){
		$ci =& get_instance();

		$ci->db->where('id', $rab_id);
		$query = $ci->db->get('rab')->row_array();
		if ( $step == 1 ) {
			$total_cost = ($query['total_cost'] * 0.4);
		} else {
			$total_cost = ($query['total_cost'] * 0.6);
		}
	
		$ci->db->where('rab_item.rab_id', $rab_id);
		$ci->db->where('rab_item_realization_receipt.step', $step);
		$ci->db->where_in('rab_item_realization_receipt.status', array("verified", "done"));
		$ci->db->select('rab_item_realization_receipt.total_price');
		$ci->db->join('rab_item_realization_receipt', 'rab_item.id = rab_item_realization_receipt.rab_item_id');
		$queryRABItem = $ci->db->get('rab_item')->result_array();
		$total_price = 0;
		foreach ( $queryRABItem as $data ) {
			$total_price += $data['total_price'];
		}

		$percentage = ($total_price * 100) / $total_cost;
		if ( $percentage <= 1 ) {
			$percentage = 0;
		} elseif ( $percentage > 100 ) {
			$percentage = 100;
		}

		$resp = array(
			"percentage" 	        => number_format($percentage, 2, '.', ''),
			"grandtotal_pembayaran" => number_format($total_price, 0,',','.'),
			"saldo"					=> $total_cost - $total_price,
		);
		return $resp;
	}
}

if ( ! function_exists('check_saldo') ) {
	function check_saldo( $rab_id, $step ){
		$ci =& get_instance();

		$ci->db->where('id', $rab_id);
		$query = $ci->db->get('rab')->row_array();
		if ( $step == 1 ) {
			$total_cost = ($query['total_cost'] * 0.4);
		} else {
			$total_cost = ($query['total_cost'] * 0.6);
		}
	
		$ci->db->where('rab_item.rab_id', $rab_id);
		$ci->db->where('rab_item_realization_receipt.step', $step);
		$ci->db->where('rab_item_realization_receipt.status !=', "reject");
		$ci->db->select('rab_item_realization_receipt.total_price');
		$ci->db->join('rab_item_realization_receipt', 'rab_item.id = rab_item_realization_receipt.rab_item_id');
		$queryRABItem = $ci->db->get('rab_item')->result_array();
		$total_price = 0;
		foreach ( $queryRABItem as $data ) {
			$total_price += $data['total_price'];
		}

		$percentage = ($total_price * 100) / $total_cost;
		if ( $percentage <= 1 ) {
			$percentage = 0;
		} elseif ( $percentage > 100 ) {
			$percentage = 100;
		}

		return ($total_cost - $total_price);
	}
}

if ( ! function_exists('step_menu_proposals_submission') ) {
	function step_menu_proposals_submission( $proposal_id ){
		$ci =& get_instance();

		$resp = array();

		// proposals_submission
		$ci->db->where('id', $proposal_id);
		$query = $ci->db->get('proposal')->row_array();
		// $resp['proposal']['active'] = "active";
		// $resp['proposal']['link']   = base_url() . "adminsamisade/proposals_submission/view/" . $proposal_id;

		// rab
		$ci->db->where('proposal_id', $proposal_id);
		$queryRAB = $ci->db->get('rab')->result_array();
		$tempStatusRAB    = count($queryRAB);
		$noStatusRAB      = 0;
		$realisasi		  = 0;
		foreach ( $queryRAB as $data ) {
			if ( $data['status'] == 'verified' || $data['status'] == 'done' ) {
				$noStatusRAB++;
			}

			if ( ($data['step1_percentage'] == 100 && $data['step1_status'] == "done") && ( $data['step2_percentage'] == 100 && $data['step2_status'] == "done" ) ) {
				$realisasi++;
			}
		}

		if ( $noStatusRAB == 0 ) {
			$link = "rab/" . $proposal_id;
		} else {
			$link = "rab_activity/" . $proposal_id . "/" . $queryRAB[0]['id'] . "/" . $queryRAB[0]['activity_id'];
		}

		$resp['rab']['active'] = ( $query['status'] == "pending" || $query['status'] == "reject" || $query['status'] == "verification" ) ? "" : "active";
		$resp['rab']['link']   = ( $query['status'] == "pending" || $query['status'] == "reject" || $query['status'] == "verification" ) ? "javascript:void();" : base_url() . "adminsamisade/proposals_submission/" . $link;

		// realisasi
		$resp['realisasi']['active'] = ( $tempStatusRAB != $noStatusRAB || $noStatusRAB == 0 ) ? "" : "active";
		$resp['realisasi']['link']   = ( $tempStatusRAB != $noStatusRAB || $noStatusRAB == 0 ) ? "javascript:void();" : base_url() . "adminsamisade/proposals_submission/rab_realization_step1/" . $proposal_id . "/" . @$queryRAB[0]['id'] . "/" . @$queryRAB[0]['activity_id'];

		// finish
		$resp['finish']['active'] = ( $tempStatusRAB != $realisasi || $noStatusRAB == 0 ) ? "" : "active";
		$resp['finish']['link']   = ( $tempStatusRAB != $realisasi || $noStatusRAB == 0 ) ? "javascript:void();" : "javascript:void();";

		return $resp;
	}
}

if ( ! function_exists('get_kecamatan') ) {
	function get_kecamatan( $desa_id ){
		$ci =& get_instance();
		
		$ci->db->where('desa.id', $desa_id);
		$ci->db->select('desa.*, kecamatan.name as kec_name');
		$ci->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$query = $ci->db->get('desa')->row_array();
		
		return $query;
	}
}

if ( ! function_exists('load_company') ) {
	function load_company() {
		
		$ci = get_instance();

		// company
		$queryCompany = $ci->db->get('company')->row_array();
		return $queryCompany;

	}
}

if ( ! function_exists('count_complaint') ) {
	function count_complaint() {
		
		$ci = get_instance();

		// complaint
		$ci->db->where('status', "pending");
		$queryCompany = $ci->db->get('complaint')->result_array();
		return count($queryCompany);

	}
}

if ( ! function_exists('status_complaint') ) {
	function status_complaint() {
		
		$ci = get_instance();

		// company
		$data = array(
			"pending" => "Belum Ditangani",
			"done" => "Sudah Ditangani",
		);
		return $data;

	}
}

if ( ! function_exists('check_proposal') ) {
	function check_proposal() {
		
		$ci = get_instance();

		// company
		$ci->db->where('desa_id', $ci->session->userdata('desa_id'));
		$proposal = $ci->db->get('proposal')->result_array();
		return $proposal;

	}
}

if ( ! function_exists('check_rab') ) {
	function check_rab($proposal_id) {
		
		$ci = get_instance();

		// company
		$ci->db->where('proposal_id', $proposal_id);
		$query = $ci->db->get('rab')->result_array();

		$resp  = array();
		$total = 0;
		foreach ( $query as $val ) {
			$total += $val['total_cost'];	
		}

		$resp['all_total_cost']  = $total;
		$resp['rest_total_cost'] = (1000000000 - $total);

		return $resp;

	}
}

if ( ! function_exists('disbursement_date') ) {
	function disbursement_date($step) {
		
		$ci = get_instance();

		if ( $step == 1 ) {
			$ci->db->where('rab."status"', 'verified');
			$ci->db->where('rab_disbursement_date."realization_date_step1" is null', null);
		} else {
			$ci->db->where('rab."status"', 'verified');
			$ci->db->where('rab."step1_percentage" >= ', 75);
			$ci->db->where('rab_disbursement_date."realization_date_step2" is null', null);
		}

		$ci->db->select('rab.*, 
							rab_disbursement_date."realization_date_step1", 
							rab_disbursement_date."realization_date_step2",
							proposal.title as proposal_name,
							activity.name as activity_name,
							desa.name as desa_name,
							kecamatan.name as kec_name');
		$ci->db->join('rab_disbursement_date', 'rab.id = rab_disbursement_date.rab_id', 'left');
		$ci->db->join('proposal', 'rab.proposal_id = proposal.id');
		$ci->db->join('desa', 'proposal.desa_id = desa.id');
		$ci->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$ci->db->join('activity', 'rab.activity_id = activity.id');
		$query = $ci->db->get('rab')->result_array();
	
		return $query;

	}
}

if ( ! function_exists('error_message_upload') ) {
	function error_message_upload($param) {
		
		$param = str_replace(array("<p>", "</p>"), "", $param);

		$data = array(
			"upload_invalid_filetype" => "Format dokumen yang anda upload salah.",
			"upload_no_file_selected" => "Silahkan pilih dokumen terlebih dahulu untuk di upload.",
			"upload_invalid_filesize" => "Ukuran dokumen melebihi batas yang di tentukan.",
		);
	
		return $data[$param];

	}
}