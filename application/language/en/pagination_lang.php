<?php

	//alerts
	$lang['alert_delete']                   = 'Apa anda yakin ingin menghapus data ini?';
	$lang['alert_delete_choose_one']        = 'Harap Pilih setidaknya satu kotak centang';
	$lang['alert_delete_success']           = 'Data berhasil dihapus';
	$lang['alert_delete_failed']            = 'Hapus Gagal, coba lagi nanti';

	//buttons
	$lang['button_login']      = 'Masuk';
	$lang['button_reset']      = 'Atur Ulang';
	$lang['button_next']       = 'Selanjutnya';
	$lang['button_previous']   = 'Sebelumnya';
	$lang['button_finish']     = 'Selesai';
	$lang['button_back']       = 'Kembali';
	$lang['button_save']       = 'Simpan';
	$lang['button_activated']  = 'Actif';
	$lang['button_skip']       = 'Lewati';
	$lang['button_delete']     = 'Hapus';
	$lang['button_add']        = 'Tambah';
	$lang['button_create_new'] = 'Buat Baru';
	$lang['button_find']       = 'Cari';