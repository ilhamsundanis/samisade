<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_auth extends CI_Model {

	public function login($email, $password)
	{
		$this->db->select('user.*, kecamatan.name as name_kecamatan, desa.name as name_desa');
		$this->db->where('email', $email);
		$this->db->where('password', MD5($password));
		$this->db->join('kecamatan', 'kecamatan.id = user.kec_id', 'left');
		$this->db->join('desa', 'desa.id = user.desa_id', 'left');
		$query = $this->db->get('user')->row_array();

		return $query;
	}

	public function log_access($data = array())
	{
		$filter = array();
		// administrator
		if ( $data['role'] == 1 ) {
			$filter = array(
				'role' => 1,
				'date' => date('Y-m-d')
			);
		// kecamatan
		} else if ( $data['role'] == 2 ) {
			$filter = array(
				'role' => 2,
				'relation_id' => $data['kec_id'],
				'date' => date('Y-m-d')
			);
		// desa
		} else {
			$filter = array(
				'role' => 3,
				'relation_id' => $data['desa_id'],
				'date' => date('Y-m-d')
			);
		}

		$this->db->where($filter);
		$search = $this->db->get('log_access')->row_array();
		if ( !$search ) {
			unset($filter['date']);	
			$this->db->insert('log_access', $filter);
		}
	}
	
	public function get_user($old_password)
	{
		$this->db->where('email', $this->session->userdata('email'));
		$this->db->where('password', MD5($old_password));
		$query = $this->db->get('user')->row_array();

		return $query;
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */