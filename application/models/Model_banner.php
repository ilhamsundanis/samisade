<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_banner extends CI_Model {

	public function get_all()
	{
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('banner')->result_array();

		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('banner')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('banner');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */