<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_category extends CI_Model {

	public function get_all()
	{
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
		} else if ( $this->session->userdata('role') == 3 ) {
			$this->db->where('category.desa_id', $this->session->userdata('desa_id'));
		}

		$this->db->select('category.*, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('desa', 'category.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		
		$query = $this->db->get("category")->result_array();
		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('category.id', $id);
		$this->db->select('category.*, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('desa', 'category.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$query = $this->db->get('category')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('category');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	

	public function get_all_desa_by_kecamatan($kec_id)
	{
		$resp = array();

		$this->db->where('kec_id', $kec_id);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('desa')->result_array();

		return $query;
	}
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */