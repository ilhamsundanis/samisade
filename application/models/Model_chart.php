<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_chart extends CI_Model {

	public function get_chart_total_anggaran($post)
	{
		if ( !empty($post['kec_id']) ) {
			$this->db->where('desa.kec_id', $post['kec_id']);
		} 
		
		if ( !empty($post['desa_id']) ) {
			$this->db->where('desa.id', $post['desa_id']);
		}

		$this->db->select('proposal.*, desa.name as desa_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$query = $this->db->get('proposal')->result_array();

		$resp = array();
		$backgroundColor = array();
		$jumlah = array();
		foreach ($query as $index => $data) {
			$color = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$resp['labels'][]  = $data['desa_name'];
			$backgroundColor[] = $color;

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->select('sum(total_cost) as total_cost');
			$queryJumlah = $this->db->get('rab')->row_array();

			$jumlah[] = (int)$queryJumlah['total_cost'];
		}
		$resp['datasets'][0]['label']           = "Total Anggaran";
		$resp['datasets'][0]['backgroundColor'] = $backgroundColor;
		$resp['datasets'][0]['borderColor']     = $backgroundColor;
		$resp['datasets'][0]['data']            = $jumlah;

		return $resp;
	}

	public function get_table_total_anggaran($post)
	{
		if ( !empty($post['kec_id']) ) {
			$this->db->where('desa.kec_id', $post['kec_id']);
		} 
		
		if ( !empty($post['desa_id']) ) {
			$this->db->where('desa.id', $post['desa_id']);
		}

		$this->db->select('proposal.*, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$query = $this->db->get('proposal')->result_array();
		foreach ($query as $index => $data) {
			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->select('sum(total_cost) as total_cost');
			$queryJumlah = $this->db->get('rab')->row_array();

			$query[$index]['total_cost'] = (int)$queryJumlah['total_cost'];
		}

		// pre($query);

		return $query;
	}

	public function get_chart_jumlah_dana($post)
	{
		if ( !empty($post['kec_id']) ) {
			$this->db->where('desa.kec_id', $post['kec_id']);
		} 
		
		if ( !empty($post['desa_id']) ) {
			$this->db->where('desa.id', $post['desa_id']);
		}

		$this->db->select('proposal.*, desa.name as desa_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$query = $this->db->get('proposal')->result_array();

		$resp = array();
		$backgroundColor = array();
		$jumlahTahap1 = array();
		$jumlahTahap2 = array();
		foreach ($query as $index => $data) {
			$color = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$resp['labels'][]  = $data['desa_name'];
			$backgroundColor[] = $color;

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "1");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap1 = $this->db->get('rab_item_realization_receipt')->row_array();

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "2");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap2 = $this->db->get('rab_item_realization_receipt')->row_array();

			$jumlahTahap1[] = (int)$queryJumlahTahap1['total_price'];
			$jumlahTahap2[] = (int)$queryJumlahTahap2['total_price'];
		}
		$resp['datasets'][0]['label']           = "TAHAP 1";
		$resp['datasets'][0]['backgroundColor'] = "#03a9f3";
		$resp['datasets'][0]['borderColor']     = "#03a9f3";
		$resp['datasets'][0]['data']            = $jumlahTahap1;

		$resp['datasets'][1]['label']           = "TAHAP 2";
		$resp['datasets'][1]['backgroundColor'] = "#00c292";
		$resp['datasets'][1]['borderColor']     = "#00c292";
		$resp['datasets'][1]['data']            = $jumlahTahap2;

		return $resp;
	}

	public function get_table_jumlah_dana($post)
	{
		if ( !empty($post['kec_id']) ) {
			$this->db->where('desa.kec_id', $post['kec_id']);
		} 
		
		if ( !empty($post['desa_id']) ) {
			$this->db->where('desa.id', $post['desa_id']);
		}

		$this->db->select('proposal.*, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$query = $this->db->get('proposal')->result_array();
		foreach ($query as $index => $data) {

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "1");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap1 = $this->db->get('rab_item_realization_receipt')->row_array();

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "2");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap2 = $this->db->get('rab_item_realization_receipt')->row_array();

			$query[$index]['step1'] = (int)$queryJumlahTahap1['total_price'];
			$query[$index]['step2'] = (int)$queryJumlahTahap2['total_price'];
		}

		return $query;
	}

	public function get_chart_persentase_pembangunan($post)
	{
		if ( !empty($post['kec_id']) ) {
			$this->db->where('desa.kec_id', $post['kec_id']);
		} 
		
		if ( !empty($post['desa_id']) ) {
			$this->db->where('desa.id', $post['desa_id']);
		}

		$this->db->select('proposal.*, desa.name as desa_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$query = $this->db->get('proposal')->result_array();
		// pre($query);

		$resp = array();
		$backgroundColor = array();
		$jumlah = array();
		foreach ($query as $index => $data) {
			$color = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$resp['labels'][]  = $data['desa_name'];
			$backgroundColor[] = $color;

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->select('rab.*');
			$queryRAB = $this->db->get('rab')->result_array();
			$total_costStep1 = 0;
			$total_costStep2 = 0;
			foreach ( $queryRAB as $dataRAB ) {
				$total_costStep1 += ($dataRAB['total_cost'] * 0.4);
				$total_costStep2 += ($dataRAB['total_cost'] * 0.6);
			}

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "1");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap1 = $this->db->get('rab_item_realization_receipt')->row_array();

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "2");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap2 = $this->db->get('rab_item_realization_receipt')->row_array();

			if ( $queryJumlahTahap1['total_price'] == 0 || $total_costStep1 == 0) {
				$jumlahTahap1[] = 0;
			} else {
				$jumlahTahap1[] = ($queryJumlahTahap1['total_price'] * 100) / $total_costStep1;
			}

			if ( $queryJumlahTahap2['total_price'] == 0 || $total_costStep2 == 0) {
				$jumlahTahap2[] = 0;
			} else {
				$jumlahTahap2[] = ($queryJumlahTahap2['total_price'] * 100) / $total_costStep2;
			}
		}
		$resp['datasets'][0]['label']           = "TAHAP 1";
		$resp['datasets'][0]['backgroundColor'] = "#03a9f3";
		$resp['datasets'][0]['borderColor']     = "#03a9f3";
		$resp['datasets'][0]['data']            = @$jumlahTahap1;

		$resp['datasets'][1]['label']           = "TAHAP 2";
		$resp['datasets'][1]['backgroundColor'] = "#00c292";
		$resp['datasets'][1]['borderColor']     = "#00c292";
		$resp['datasets'][1]['data']            = @$jumlahTahap2;

		return $resp;
	}

	public function get_table_persentase_pembangunan($post)
	{
		if ( !empty($post['kec_id']) ) {
			$this->db->where('desa.kec_id', $post['kec_id']);
		} 
		
		if ( !empty($post['desa_id']) ) {
			$this->db->where('desa.id', $post['desa_id']);
		}

		$this->db->select('proposal.*, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$query = $this->db->get('proposal')->result_array();

		foreach ($query as $index => $data) {

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->select('rab.*');
			$queryRAB = $this->db->get('rab')->result_array();
			$total_costStep1 = 0;
			$total_costStep2 = 0;
			foreach ( $queryRAB as $dataRAB ) {
				$total_costStep1 += ($dataRAB['total_cost'] * 0.4);
				$total_costStep2 += ($dataRAB['total_cost'] * 0.6);
			}

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "1");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap1 = $this->db->get('rab_item_realization_receipt')->row_array();

			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->where('rab_item_realization_receipt.status', "verified");
			$this->db->where('rab_item_realization_receipt.step', "2");
			$this->db->select('sum(rab_item_realization_receipt.total_price) as total_price');
			$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
			$this->db->join('rab', 'rab_item.rab_id = rab.id');
			$queryJumlahTahap2 = $this->db->get('rab_item_realization_receipt')->row_array();

			if ( $queryJumlahTahap1['total_price'] == 0 || $total_costStep1 == 0) {
				$query[$index]['step1'] = 0;
			} else {
				$query[$index]['step1'] = ($queryJumlahTahap1['total_price'] * 100) / $total_costStep1;
			}

			if ( $queryJumlahTahap2['total_price'] == 0 || $total_costStep2 == 0) {
				$query[$index]['step2'] = 0;
			} else {
				$query[$index]['step2'] = ($queryJumlahTahap2['total_price'] * 100) / $total_costStep2;
			}
		}

		return $query;
	}

	public function get_all_desa_by_kecamatan($kec_id)
	{
		$resp = array();

		$this->db->where('kec_id', $kec_id);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('desa')->result_array();

		return $query;
	}

	public function get_chart_access($post) {
		$resp = array();
		$date = array();
		$day = cal_days_in_month(CAL_GREGORIAN,$post['month'],$post['year']);
		for ( $i = 1; $i <= $day; $i++) {
			if ( $i < 10 ) {
				$i = "0". $i;
			}
			$date[$i-1] = date("Y") . "-" . $post['month'] . "-" . $i;
		}

		$resp['labels'] = $date;
		$totalKecamatan = array();
		$totalDesa 		= array();
		foreach ( $date as $idx => $val ) {
			$this->db->select('count(id) as total');
			$this->db->where('date', $val);
			$this->db->where('role', 2);
			$queryKecamatan = $this->db->get('log_access')->row_array();
			$totalKecamatan[$idx] = $queryKecamatan['total'];

			$this->db->select('count(id) as total');
			$this->db->where('date', $val);
			$this->db->where('role', 3);
			$queryDesa = $this->db->get('log_access')->row_array();
			$totalDesa[$idx] = $queryDesa['total'];
		}	
		$resp['datasets'][0] = array(
			"label" 		  => "Kecamatan",
			"data" 			  => $totalKecamatan,
			"borderColor" 	  => "red",
			"backgroundColor" => "transparent",
		);
		$resp['datasets'][1] = array(
			"label" 		  => "Desa",
			"data" 			  => $totalDesa,
			"borderColor" 	  => "blue",
			"backgroundColor" => "transparent",
		);
		return (json_encode($resp));
	}

	public function get_all_access($post) {
		$resp = array();
		$day = cal_days_in_month(CAL_GREGORIAN,$post['month'],$post['year']);
		for ( $i = 1; $i <= $day; $i++) {
			if ( $i < 10 ) {
				$i = "0". $i;
			}
			$resp[$i-1]['date'] = date("Y") . "-" . $post['month'] . "-" . $i;

			// kecamatan
			$this->db->select('log_access.*, kecamatan.name');
			$this->db->where('date', date("Y") . "-" . $post['month'] . "-" . $i);
			$this->db->where('role', 2);
			$this->db->join('kecamatan', 'log_access.relation_id = kecamatan.id');
			$queryKecamatan = $this->db->get('log_access')->result_array();
			$resp[$i-1]['kecamatan'] = $queryKecamatan;

			// desa
			$this->db->select('log_access.*, desa.name');
			$this->db->where('date', date("Y") . "-" . $post['month'] . "-" . $i);
			$this->db->where('role', 3);
			$this->db->join('desa', 'log_access.relation_id = desa.id');
			$queryDesa = $this->db->get('log_access')->result_array();
			$resp[$i-1]['desa'] = $queryDesa;
		}
		return $resp;
	}
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */