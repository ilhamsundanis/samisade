<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_company extends CI_Model {

	public function find_by_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('company')->row_array();

		return $query;
	}
	
	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */