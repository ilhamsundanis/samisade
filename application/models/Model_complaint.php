<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_complaint extends CI_Model {

	public function get_all()
	{
		$this->db->select('complaint.*, kecamatan.name as kec_name, desa.name as desa_name');
		$this->db->join('desa', 'complaint.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->order_by('complaint.created_at', 'desc');
		
		$query = $this->db->get('complaint')->result_array();

		return $query;
	}	
	
	public function get_detail($id)
	{
		$this->db->select('complaint.*, kecamatan.name as kec_name, desa.name as desa_name');
		$this->db->join('desa', 'complaint.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->where('complaint.id', $id);
		$this->db->order_by('complaint.created_at', 'desc');
		$query = $this->db->get('complaint')->row_array();

		return $query;
    }	

	public function get_complaint_by_desa($desa_id)
	{
		$this->db->select('complaint.*, kecamatan.name as kec_name, desa.name as desa_name');
		$this->db->join('desa', 'complaint.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->where('complaint.desa_id', $desa_id);
		$this->db->order_by('complaint.created_at', 'desc');
		$query = $this->db->get('complaint')->result_array();

		return $query;
    }	

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */