<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_disbursement_date extends CI_Model {

	public function get_all()
	{
		$this->db->select('rab_disbursement_date.*,
							desa.name as desa_name,
							kecamatan.name as kec_name');
		$this->db->join('kecamatan', 'rab_disbursement_date.kec_id = kecamatan.id');
		$this->db->join('desa', 'rab_disbursement_date.desa_id = desa.id');
		$query = $this->db->get('rab_disbursement_date')->result_array();

		return $query;
	}

	public function get_all_disbursement_date()
	{
		$this->db->select('wave');
		$this->db->group_by('wave');
		$this->db->order_by('wave', 'asc');
		$query = $this->db->get('rab_disbursement_date')->result_array();
		foreach ( $query as $index => $data ) {
			for ( $i = 1; $i <= 2; $i++ ) {
				$this->db->where('wave', $data['wave']);
				$this->db->where('step', $i);
				$this->db->select('rab_disbursement_date.*,
							desa.name as desa_name,
							kecamatan.name as kec_name');
				$this->db->join('kecamatan', 'rab_disbursement_date.kec_id = kecamatan.id');
				$this->db->join('desa', 'rab_disbursement_date.desa_id = desa.id');
				$this->db->order_by('kecamatan.name, desa.name', 'asc');
				$query_rab_disbursement_date = $this->db->get('rab_disbursement_date')->result_array();
				$query[$index]['data']['step' . $i] = $query_rab_disbursement_date; 
			}
		}
		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('rab_disbursement_date.id', $id);
		$this->db->select('rab_disbursement_date.*,
							desa.name as desa_name,
							kecamatan.name as kec_name');
		$this->db->join('kecamatan', 'rab_disbursement_date.kec_id = kecamatan.id');
		$this->db->join('desa', 'rab_disbursement_date.desa_id = desa.id');
		$query = $this->db->get('rab_disbursement_date')->row_array();

		return $query;
    }
	
	public function get_detail_by_data($post)
	{
		$this->db->where($post);
		$query = $this->db->get('rab_disbursement_date')->row_array();

		return $query;
    }
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('rab_disbursement_date');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */