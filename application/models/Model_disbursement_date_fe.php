<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_disbursement_date_fe extends CI_Model {

	public function get_all_disbursement_date()
	{
		$this->db->select('wave');
		$this->db->group_by('wave');
		$this->db->order_by('wave', 'asc');
		$query = $this->db->get('rab_disbursement_date')->result_array();
		foreach ( $query as $index => $data ) {
			for ( $i = 1; $i <= 2; $i++ ) {
				$this->db->where('wave', $data['wave']);
				$this->db->where('step', $i);
				$this->db->select('rab_disbursement_date.*,
							desa.name as desa_name,
							kecamatan.name as kec_name');
				$this->db->join('kecamatan', 'rab_disbursement_date.kec_id = kecamatan.id');
				$this->db->join('desa', 'rab_disbursement_date.desa_id = desa.id');
				$this->db->order_by('kecamatan.name, desa.name', 'asc');
				$query_rab_disbursement_date = $this->db->get('rab_disbursement_date')->result_array();
				$query[$index]['data']['step' . $i] = $query_rab_disbursement_date; 
			}
		}
		// pre($query);
		return $query;
	}
	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */