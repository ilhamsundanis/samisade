<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_employee extends CI_Model {

	public function get_all($rab_id)
	{
		$this->db->where('rab_item.rab_id', $rab_id);
		
		$this->db->select('rab_item_employee.*, rab_item.name as name_rab_item'); 
		$this->db->join('rab_item', 'rab_item_employee.rab_item_id = rab_item.id');
		$query = $this->db->get('rab_item_employee')->result_array();

		return $query;
	}

	public function get_all_by_rab_item($rab_item_id)
	{
		$this->db->where('rab_item_employee.rab_item_id', $rab_item_id);
		
		$this->db->select('rab_item_employee.*, rab_item.name as name_rab_item'); 
		$this->db->join('rab_item', 'rab_item_employee.rab_item_id = rab_item.id');
		$query = $this->db->get('rab_item_employee')->result_array();

		return $query;
	}
	
	public function get_rab_item_by_is_worker($rab_id)
	{
		$this->db->where('is_worker', true);
		$this->db->where('rab_id', $rab_id);
		$query = $this->db->get('rab_item')->result_array();

		return $query;
	}

	public function get_rab_item_id_by_is_worker($rab_item_id)
	{
		$this->db->where('is_worker', true);
		$this->db->where('id', $rab_item_id);
		$query = $this->db->get('rab_item')->result_array();

		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('rab_item_employee')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('rab_item_employee');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */