<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_news_fe extends CI_Model {

	public function get_all($number, $offset)
	{
		$this->db->select('news.*, user.name as user_name');
		$this->db->join('user', 'news.created_by = user.id');
		$this->db->order_by('news.id', 'desc');
		$query = $this->db->get('news', $number, $offset)->result_array();

		return $query;
	}

	function count_news()
	{
		$this->db->order_by('news.id', 'desc');
		return $this->db->get('news')->num_rows();
	}
	
	public function get_detail($id)
	{
		$this->db->where('news.id', $id);

		$this->db->select('news.*, user.name as user_name');
		$this->db->join('user', 'news.created_by = user.id');
		$query = $this->db->get('news')->row_array();

		return $query;
	}
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */