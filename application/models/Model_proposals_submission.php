<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_proposals_submission extends CI_Model {

	public function get_all($filter, $number, $offset)
	{
		// if ( $this->session->userdata('role') == 2 ) {
		// 	$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
		// } else if ( $this->session->userdata('role') == 3 ) {
		// 	$this->db->where('proposal.desa_id', $this->session->userdata('desa_id'));
		// }

		if ( ! empty($filter['kec_id']) ) {
			$this->db->where('desa.kec_id', $filter['kec_id']);
		}

		if ( ! empty($filter['desa_id']) ) {
			$this->db->where('proposal.desa_id', $filter['desa_id']);
		}

		if ( ! empty($filter['proposal']) ) {
			$this->db->where('proposal.title', $filter['proposal']);
		}
		
		$this->db->select('proposal.*, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->order_by('proposal.id', 'desc');
		$query = $this->db->get('proposal', $number, $offset)->result_array();
		foreach ( $query as $index => $data ) {
			$this->db->where('rab.proposal_id', $data['id']);
			$this->db->select('rab.*, activity.name as activity_name');
			$this->db->join('activity', 'rab.activity_id = activity.id');
			$queryRAB = $this->db->get('rab')->result_array();
			$query[$index]['rab'] = $queryRAB;
		}

		return $query;
	}

	function count($filter)
	{
		// if ( $this->session->userdata('role') == 2 ) {
		// 	$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
		// } else if ( $this->session->userdata('role') == 3 ) {
		// 	$this->db->where('proposal.desa_id', $this->session->userdata('desa_id'));
		// }

		if ( ! empty($filter['kec_id']) ) {
			$this->db->where('desa.kec_id', $filter['kec_id']);
		}

		if ( ! empty($filter['desa_id']) ) {
			$this->db->where('proposal.desa_id', $filter['desa_id']);
		}

		if ( ! empty($filter['proposal']) ) {
			$this->db->where('proposal.title', $filter['proposal']);
		}

		$this->db->join('desa', 'proposal.desa_id = desa.id');
		return $this->db->get('proposal')->num_rows();
	}

	function check_proposal($desa_id)
	{
		$this->db->where('desa_id', $desa_id);
		return $this->db->get('proposal')->num_rows();
	}

	function check_rab($proposal_id)
	{
		$this->db->where('proposal_id', $proposal_id);
		$query = $this->db->get('rab')->result_array();

		$resp = array();
		$total = 0;
		foreach ( $query as $val ) {
			$total += $val['total_cost'];	
		}

		$resp['all_total_cost']  = $total;
		$resp['rest_total_cost'] = (1000000000 - $total);
		return $resp;
	}

	function check_rab_by_rab_id($proposal_id, $rab_id)
	{
		$this->db->where('proposal_id', $proposal_id);
		$this->db->where('id <>', $rab_id);
		
		$query = $this->db->get('rab')->result_array();

		$resp = array();
		$total = 0;
		foreach ( $query as $val ) {
			$total += $val['total_cost'];	
		}

		$resp['all_total_cost']  = $total;
		$resp['rest_total_cost'] = (1000000000 - $total);
		return $resp;
	}

	public function get_all_proposal()
	{
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
		}

		$this->db->where("status", "verification");
		$this->db->select('proposal.*, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$query = $this->db->get("proposal")->result_array();
		return $query;
	}

	public function get_all_rab()
	{
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
		}

		$this->db->where("rab.status", "verification");
		$this->db->select("rab.*, proposal.title, activity.name as activity_name, desa.name as desa_name, kecamatan.name as kec_name");
		$this->db->join("activity", "rab.activity_id = activity.id");
		$this->db->join('proposal', 'rab.proposal_id = proposal.id');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$queryRAB = $this->db->get("rab")->result_array();

		return $queryRAB;
	}

	public function get_all_realization()
	{
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
		}

		$this->db->where('rab_item_realization_receipt.status', 'verification');
		$this->db->select('rab_item_realization_receipt.step, rab_item.rab_id');
		$this->db->join('rab_item', 'rab_item_realization_receipt.rab_item_id = rab_item.id');
		$this->db->join('rab', 'rab_item.rab_id = rab.id');
		$this->db->join('proposal', 'rab.proposal_id = proposal.id');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->group_by('rab_item_realization_receipt.step, rab_item.rab_id');
		$query = $this->db->get("rab_item_realization_receipt")->result_array();
		foreach ( $query as $index => $data ) {
			$this->db->where('rab.id', $data['rab_id']);
			$this->db->select("rab.*, proposal.title, activity.name as activity_name, desa.name as desa_name, kecamatan.name as kec_name");
			$this->db->join("activity", "rab.activity_id = activity.id");
			$this->db->join('proposal', 'rab.proposal_id = proposal.id');
			$this->db->join('desa', 'proposal.desa_id = desa.id');
			$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
			$queryRAB = $this->db->get("rab")->row_array();
			$query[$index]['rab'] = $queryRAB;
		}

		return $query;
	}

	public function get_all_category($desa_id)
	{
		$this->db->where('category.desa_id', $desa_id);
		$query = $this->db->get('category')->result_array();

		return $query;
	}

	public function get_all_activity($proposal_id, $desa_id)
	{

		$this->db->where('rab.proposal_id', $proposal_id);
		$this->db->where('proposal.desa_id', $desa_id);
		
		$this->db->select('rab.*, activity.name');
		$this->db->join('activity', 'rab.activity_id = activity.id');
		$this->db->join('proposal', 'rab.proposal_id = proposal.id');
		$query = $this->db->get('rab')->result_array();

		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('proposal.id', $id);

		$this->db->select('proposal.*, desa.name as desa_name, kecamatan.id as kec_id, kecamatan.name as kec_name');		
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		
		$query = $this->db->get('proposal')->row_array();

		return $query;
	}	
	
	public function get_detail_all($proposal_id, $rab_id, $activity_id)
	{
		$this->db->where('proposal.id', $proposal_id);

		$this->db->select('proposal.*, proposal.desa_id, desa.name as desa_name, kecamatan.id as kec_id, kecamatan.name as kec_name');		
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		
		$query = $this->db->get('proposal')->row_array();
		// pre($query);

		// rab
		$this->db->where('rab.id', $rab_id);
		$this->db->where('rab.activity_id', $activity_id);
		$this->db->select('rab.*, activity.name as activity_name, activity.type as activity_type');
		$this->db->join('activity', 'rab.activity_id = activity.id');
		$query['rab'] = $this->db->get('rab')->row_array();

		// rab_disbursement_date step1
		$this->db->where('kec_id', $query['kec_id']);
		$this->db->where('desa_id', $query['desa_id']);
		$this->db->where('step', '1');
		$query['rab']['disbursement_date']['step1'] = $this->db->get('rab_disbursement_date')->row_array()['disbursement_date'];

		// rab_disbursement_date step2
		$this->db->where('kec_id', $query['kec_id']);
		$this->db->where('desa_id', $query['desa_id']);
		$this->db->where('step', '2');
		$query['rab']['disbursement_date']['step2'] = $this->db->get('rab_disbursement_date')->row_array()['disbursement_date'];

		// category
		$this->db->where('rab_item.rab_id', $rab_id);
		$this->db->select('category.id, category.name');
		$this->db->join('category', 'rab_item.category_id = category.id');
		$this->db->group_by('category.id, category.name');
		// $this->db->order_by('category.id', 'asc');
		$queryGetCategory = $this->db->get("rab_item")->result_array();
		$query['category'] = array();
		foreach ($queryGetCategory as $value) {
			$query['category'][] = $value['id'];
		}

		// rab item per category
		$query['rab_item'] = array();
		foreach ($queryGetCategory as $index => $value) {
			$query['rab_item'][$index]['category'] 		= $value['id'];
			$query['rab_item'][$index]['category_name'] = $value['name'];

			$this->db->where('category_id', $value['id']);
			$this->db->where('rab_id', $rab_id);
			$queryGetRABItem = $this->db->get('rab_item')->result_array();
			foreach ($queryGetRABItem as $indexItem => $valueItem) {
				$query['rab_item'][$index]['item'][$indexItem] = $valueItem;

				// step1
				$this->db->select('rab_item_realization_activities.*, rab_item_gallery.file');
				$this->db->where('rab_item_realization_activities.rab_item_id', $valueItem['id']);
				$this->db->where('rab_item_realization_activities.step', 1);
				$this->db->join('rab_item_gallery', 'rab_item_realization_activities.rab_item_gallery_id = rab_item_gallery.id');
				$queryGetRABItemRealizationActivities1 = $this->db->get('rab_item_realization_activities')->result_array();

				$this->db->select('rab_item_realization_receipt.*, rab_item_gallery.file');
				$this->db->where('rab_item_realization_receipt.rab_item_id', $valueItem['id']);
				$this->db->where('rab_item_realization_receipt.step', 1);
				$this->db->join('rab_item_gallery', 'rab_item_realization_receipt.rab_item_gallery_id = rab_item_gallery.id');
				$queryGetRABItemRealizationReceipt1 = $this->db->get('rab_item_realization_receipt')->result_array();

				$query['rab_item'][$index]['item'][$indexItem]['realization']['step1']['receipt']     = $queryGetRABItemRealizationReceipt1;
				$query['rab_item'][$index]['item'][$indexItem]['realization']['step1']['activities']  = $queryGetRABItemRealizationActivities1;


				// step2
				$this->db->select('rab_item_realization_activities.*, rab_item_gallery.file');
				$this->db->where('rab_item_realization_activities.rab_item_id', $valueItem['id']);
				$this->db->where('rab_item_realization_activities.step', 2);
				$this->db->join('rab_item_gallery', 'rab_item_realization_activities.rab_item_gallery_id = rab_item_gallery.id');
				$queryGetRABItemRealizationActivities2 = $this->db->get('rab_item_realization_activities')->result_array();

				$this->db->select('rab_item_realization_receipt.*, rab_item_gallery.file');
				$this->db->where('rab_item_realization_receipt.rab_item_id', $valueItem['id']);
				$this->db->where('rab_item_realization_receipt.step', 2);
				$this->db->join('rab_item_gallery', 'rab_item_realization_receipt.rab_item_gallery_id = rab_item_gallery.id');
				$queryGetRABItemRealizationReceipt2 = $this->db->get('rab_item_realization_receipt')->result_array();

				$query['rab_item'][$index]['item'][$indexItem]['realization']['step2']['receipt']     = $queryGetRABItemRealizationReceipt2;
				$query['rab_item'][$index]['item'][$indexItem]['realization']['step2']['activities']  = $queryGetRABItemRealizationActivities2;
			}
		}

		// rab_location
		$this->db->where('rab_id', $rab_id);
		$query['rab_location'] = $this->db->get('rab_location')->row_array();

		// pre($query);

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('proposal');
		}

		return $delete;
	}

	public function delete_rab( $rab_id )
	{
		// rab
		$this->db->where('id', $rab_id);
		$this->db->delete('rab');

		// rab_item
		$this->db->where('rab_id', $rab_id);
		$delete = $this->db->delete('rab_item');

		return $delete;
	}

	public function delete_rab_item( $id )
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete('rab_item');

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function add_last_id($table, $data)
	{
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

   		return  $insert_id;
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	

	public function get_all_desa_by_kecamatan($kec_id)
	{
		$resp = array();

		$this->db->where('kec_id', $kec_id);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('desa')->result_array();

		return $query;
	}

	public function get_rab_item_by_category($rab_id, $category_id)
	{
		$resp = array();

		$this->db->where('rab_id', $rab_id);
		$this->db->where('category_id', $category_id);
		$query = $this->db->get('rab_item')->result_array();

		return $query;
	}

	public function get_detail_rab_item_realization_activities($rab_item_id, $step, $rab_item_gallery_id)
	{
		$this->db->where('rab_item_id', $rab_item_id);
		$this->db->where('step', $step);
		$this->db->where('rab_item_gallery_id', (int)$rab_item_gallery_id);
		$query = $this->db->get('rab_item_realization_activities')->row_array();

		return $query;
	}
	
	public function get_detail_rab_item_realization_receipt($rab_item_id, $step, $rab_item_gallery_id)
	{
		$this->db->where('rab_item_id', $rab_item_id);
		$this->db->where('step', $step);
		$this->db->where('rab_item_gallery_id', $rab_item_gallery_id);
		$query = $this->db->get('rab_item_realization_receipt')->row_array();

		return $query;
	}

	public function get_all_rab_gallery($rab_item_id, $type, $step) 
	{
		$this->db->where('rab_item_id', $rab_item_id);
		$this->db->where('type', $type);
		$this->db->where('step', $step);
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get('rab_item_gallery')->result_array();
		foreach ( $query as $index => $data ) {
			$this->db->where('rab_item_id', $rab_item_id);
			$this->db->where('rab_item_gallery_id', $data['id']);
			$this->db->where('step', $step);
			$queryRealizationReceipt = $this->db->get('rab_item_realization_receipt')->row_array();
			$query[$index]['receipt'] = $queryRealizationReceipt;

			$this->db->where('rab_item_id', $rab_item_id);
			$this->db->where('rab_item_gallery_id', $data['id']);
			$this->db->where('step', $step);
			$queryRealizationReceipt = $this->db->get('rab_item_realization_activities')->row_array();
			$query[$index]['activities'] = $queryRealizationReceipt;
		}

		// pre($query);

		return $query;
	}

	public function get_all_rab_gallery_realization_activities($rab_item_id, $step)
	{
		$this->db->select('rab_item_realization_activities.*, rab_item_gallery.file');
		$this->db->where('rab_item_realization_activities.rab_item_id', $rab_item_id);
		$this->db->where('rab_item_realization_activities.step', $step);
		$this->db->join('rab_item_gallery', 'rab_item_realization_activities.rab_item_gallery_id = rab_item_gallery.id');
		$this->db->order_by('rab_item_realization_activities.created_at', 'desc');
		$query = $this->db->get('rab_item_realization_activities')->result_array();

		return $query;
	}

	// public function send_rab_realization($rab_id, $step) 
	// {
	// 	$this->db->where("rab_item.rab_id", $rab_id);
	// 	$this->db->where("rab_item_realization.status", "pending");
	// 	$this->db->where("rab_item_realization.step", $step);
	// 	$this->db->select("rab_item_realization.id");
	// 	$this->db->join("rab_item_realization", "rab_item.id = rab_item_realization.rab_item_id");
	// 	$query = $this->db->get("rab_item")->result_array();
	// 	$save = false;
	// 	foreach ( $query as $data ) {
	// 		$this->db->where("id", $data["id"]);
	// 	 	$save = $this->db->update("rab_item_realization", array("status" => "verification"));
	// 	}

	// 	return $save;
	// }

	// public function send_data_realization($post) 
	// {
	// 	$param = array(
	// 		"status"      => "verification",
	// 		"total_price" => $post['total_pembayaran'],
	// 	);
	// 	$this->db->where("rab_item_id", $post["rab_item_id"]);
	// 	$this->db->where("step", $post["step"]);
	// 	$save = $this->db->update("rab_item_realization", $param);
	// 	return $save;
	// }
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */