<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_realization_fe extends CI_Model {

	public function get_chart_activities($post)
	{
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('activity')->result_array();

		$resp = array();
		$backgroundColor = array();
		$jumlah = array();
		foreach ($query as $index => $data) {
			$color = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$resp['labels'][]  = $data['name'];
			$backgroundColor[] = $color;

			$this->db->where('rab.activity_id', $data['id']);

			if ( !empty($post['kec_id']) ) {
				$this->db->where('desa.kec_id', $post['kec_id']);
			} 
			
			if ( !empty($post['desa_id']) ) {
				$this->db->where('desa.id', $post['desa_id']);
			}

			if ( ! empty($post['activity_id']) ) {
				$this->db->where('rab.activity_id', $post['activity_id']);
			}

			$this->db->select('count(rab.activity_id) as total_activity');
			$this->db->join('proposal', 'rab.proposal_id = proposal.id');
			$this->db->join('desa', 'proposal.desa_id = desa.id');
			$queryJumlah = $this->db->get('rab')->row_array();

			$jumlah[] = (int)$queryJumlah['total_activity'];
		}
		$resp['datasets'][0]['label']           = "Total Pembangunan";
		$resp['datasets'][0]['backgroundColor'] = $backgroundColor;
		$resp['datasets'][0]['borderColor']     = $backgroundColor;
		$resp['datasets'][0]['data']            = $jumlah;

		// pre($resp);

		return $resp;
	}
	
	public function get_chart_top_total_cost($post)
	{

		if ( !empty($post['kec_id']) ) {
			$this->db->where('desa.kec_id', $post['kec_id']);
		} 

		$this->db->select('proposal_id, sum(total_cost) as total_cost');
		$this->db->join('proposal', 'rab.proposal_id = proposal.id');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->group_by('proposal_id');
		$this->db->order_by('total_cost', 'desc');
		$this->db->limit(10);
		$query = $this->db->get('rab')->result_array();

		$resp = array();
		$backgroundColor = array();
		$jumlah = array();
		foreach ($query as $index => $data) {
			$color = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$backgroundColor[] = $color;

			if ( !empty($post['kec_id']) ) {
				$this->db->where('desa.kec_id', $post['kec_id']);
			} 
			
			$this->db->where('proposal.id', $data['proposal_id']);
			$this->db->select('proposal.*, desa.name as desa_name');
			$this->db->join('desa', 'proposal.desa_id = desa.id');
			$queryJumlah = $this->db->get('proposal')->row_array();

			$resp['labels'][] = $queryJumlah['desa_name'];
			$jumlah[] 		  = (int)$data['total_cost'];
		}
		$resp['datasets'][0]['label']           = "10 Teratas Pendanaan Pembangunan Desa";
		$resp['datasets'][0]['backgroundColor'] = $backgroundColor;
		$resp['datasets'][0]['borderColor']     = $backgroundColor;
		$resp['datasets'][0]['data']            = $jumlah;

		// pre($resp);

		return $resp;
	}

	public function get_all_data($filter)
	{

		if ( ! empty($filter['kec_id']) ) {
			$this->db->where('desa.kec_id', $filter['kec_id']);
		}

		if ( ! empty($filter['desa_id']) ) {
			$this->db->where('proposal.desa_id', $filter['desa_id']);
		}

		if ( ! empty($filter['activity_id']) ) {
			$this->db->where('rab.activity_id', $filter['activity_id']);
		}

		$this->db->select('proposal_id, sum(total_cost) as total_cost');
		$this->db->join('proposal', 'rab.proposal_id = proposal.id');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->join('activity', 'rab.activity_id = activity.id');
		$this->db->group_by('proposal_id');

		if ( ! empty($filter['anggaran']) ) {
			$this->db->order_by('total_cost', $filter['anggaran']);
		}

		$query = $this->db->get('rab')->result_array();

		foreach ($query as $index => $data) {

			// rab
			if ( ! empty($filter['activity_id']) ) {
				$this->db->where('rab.activity_id', $filter['activity_id']);
			}

			$this->db->where('rab.proposal_id', $data['proposal_id']);
			$this->db->select('rab.*, activity.name as activity_name, activity.type as activity_type');
			$this->db->join('activity', 'rab.activity_id = activity.id');
			$queryRAB = $this->db->get('rab')->result_array();
			$query[$index]['rab'] = $queryRAB;
			foreach ( $queryRAB as $indexRAB => $dataRAB ) {

				$this->db->where('rab_id', $dataRAB['id']);
				$queryRABLocation = $this->db->get("rab_location")->row_array();
				$query[$index]['rab'][$indexRAB]['location'] = $queryRABLocation;
			}

			// proposal
			$this->db->where('proposal.id', $data['proposal_id']);
			$this->db->select('proposal.*, desa.name as desa_name, kecamatan.name as kec_name');
			$this->db->join('desa', 'proposal.desa_id = desa.id');
			$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
			$queryProposal = $this->db->get("proposal")->row_array();
			$query[$index]['proposal'] = $queryProposal;

		}
		// pre($query);

		return $query;
	}

	public function get_all_desa_by_kecamatan($kec_id)
	{
		$resp = array();

		$this->db->where('kec_id', $kec_id);
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('desa')->result_array();

		return $query;
	}

	public function get_all_activity()
	{
		$this->db->order_by('name', 'asc');
		$query = $this->db->get('activity')->result_array();

		return $query;
	}
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */