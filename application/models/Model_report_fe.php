<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_report_fe extends CI_Model {

	public function get_all($number, $offset)
	{
		$this->db->select('report_kecamatan.*, user.name as user_name');
		$this->db->join('user', 'report_kecamatan.created_by = user.id');
		$this->db->order_by('report_kecamatan.id', 'desc');
		$query = $this->db->get('report_kecamatan', $number, $offset)->result_array();

		return $query;
	}

	function count_report()
	{
		$this->db->order_by('report_kecamatan.id', 'desc');
		return $this->db->get('report_kecamatan')->num_rows();
	}
	
	public function get_detail($id)
	{
		$this->db->where('report_kecamatan.id', $id);

		$this->db->select('report_kecamatan.*, user.name as user_name');
		$this->db->join('user', 'report_kecamatan.created_by = user.id');
		$query = $this->db->get('report_kecamatan')->row_array();

		return $query;
	}
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */