<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_report_kecamatan extends CI_Model {

	public function get_all()
	{
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('kecamatan.id', $this->session->userdata('kec_id'));
		}
		
		$this->db->select('report_kecamatan.*, desa.name as desa_name, kecamatan.name as kec_name');
		
		$this->db->join('desa', 'report_kecamatan.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->order_by('report_kecamatan.id', 'desc');
		$query = $this->db->get('report_kecamatan')->result_array();

		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('report_kecamatan.id', $id);
		$this->db->select('report_kecamatan.*, desa.name as desa_name, kecamatan.name as kec_name, user.name as name_user');
		$this->db->join('desa', 'report_kecamatan.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->join('user', 'report_kecamatan.created_by = user.id');
		$query = $this->db->get('report_kecamatan')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('report_kecamatan');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */