<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_report_person_in_charge extends CI_Model {

	public function get_all()
	{
		
		if ( $this->session->userdata('role') == 3 ) {
			$this->db->where('proposal.desa_id', $this->session->userdata('desa_id'));
		}
		
		$this->db->select('report_person_in_charge.*, proposal.title as proposal_title, desa.name as desa_name, kecamatan.name as kec_name');
		$this->db->join('proposal', 'report_person_in_charge.proposal_id = proposal.id');
		$this->db->join('desa', 'proposal.desa_id = desa.id');
		$this->db->join('kecamatan', 'desa.kec_id = kecamatan.id');
		$this->db->order_by('report_person_in_charge.id', 'desc');
		$query = $this->db->get('report_person_in_charge')->result_array();

		return $query;
	}

	public function get_all_proposal()
	{
		
		if ( $this->session->userdata('role') == 3 ) {
			$this->db->where('proposal.desa_id', $this->session->userdata('desa_id'));
		}
		
		$query = $this->db->get('proposal')->result_array();

		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('report_person_in_charge.id', $id);
		$query = $this->db->get('report_person_in_charge')->row_array();

		return $query;
    }	
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('report_kecamatan');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */