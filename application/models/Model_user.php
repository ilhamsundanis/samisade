<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {

	public function get_all()
	{
		if ( $this->session->userdata('role') == 2 ) {
			$this->db->where('desa.kec_id', $this->session->userdata('kec_id'));
			$this->db->select('id'); 
			$this->db->group_by("id");
			$queryDesa = $this->db->get('desa')->result_array();
			$desa = array();
			foreach ($queryDesa as $val) {
				$desa[] = $val['id'];
			}

			$this->db->where('user.kec_id', $this->session->userdata('kec_id'));
			$this->db->or_where_in('user.desa_id', $desa);
		} elseif ( $this->session->userdata('role') == 3 ) {
			$this->db->where('user.desa_id', $this->session->userdata('desa_id'));
		}
		
		$this->db->select('user.*, desa.name as name_desa, kecamatan.name as name_kecamatan'); 

		$this->db->join('desa', 'desa.id = user.desa_id', 'left');
		$this->db->join('kecamatan', 'kecamatan.id = user.kec_id', 'left');
		$this->db->order_by('user.id', 'desc');
		$query = $this->db->get("user")->result_array();

		return $query;
	}
	
	public function get_detail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('user')->row_array();

		return $query;
    }
	
	public function get_detail_by_post($data)
	{
		$this->db->where($data);
		$query = $this->db->get('user')->row_array();

		return $query;
    }
    
    public function delete( $post )
	{
		foreach ($post as $id) {
			$this->db->where('id', $id);
			$delete = $this->db->delete('user');
		}

		return $delete;
	}

	function add($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	function update($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}	
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */