<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_welcome extends CI_Model {

	public function get_all()
	{
		// company
		$queryCompany = $this->db->get('company')->row_array();

		// banner
		$queryBanner = $this->db->get('banner')->result_array();

		// report_kecamatan
		$this->db->select('report_kecamatan.*, user.name as user_name');
		$this->db->join('user', 'report_kecamatan.created_by = user.id');
		$this->db->order_by('report_kecamatan.id', 'desc');
		$this->db->limit(6);
		$queryReportKecamatan = $this->db->get('report_kecamatan')->result_array();

		// // related_link
		// $this->db->limit(9);
		// $queryRelatedLink = $this->db->get('related_link')->result_array();

		// news
		$this->db->select('news.*, user.name as user_name');
		$this->db->join('user', 'news.created_by = user.id');
		$this->db->order_by('news.id', 'desc');
		$this->db->limit(6);
		$querynews = $this->db->get('news')->result_array();

		$resp = array(
			"company" => $queryCompany,
			"banner" => $queryBanner,
			"report_kecamatan" => $queryReportKecamatan,
			"news" => $querynews,
			// "village_story" => $respVillageStory,
		);

		return $resp;
	}
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */