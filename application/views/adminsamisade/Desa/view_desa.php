<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/desa">
                List Desa
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                View Desa
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">
                                    KECAMATAN <span class="text-danger">*</span>
                                </label>
                                <select name="kec_id" id="kec_id" class="select2 form-control" required>
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_kecamatan as $data) {
                                            if ($desa['kec_id'] == $data['id']) {
                                                $selected1 = "selected";
                                            } else {
                                                $selected1 = "";
                                            }
                                    ?>
                                        <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                            <?php echo $data['name']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    DESA <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="name" value="<?php echo $desa['name']; ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

