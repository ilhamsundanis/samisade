<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            KATA SANDI LAMA <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="old_password" id="old_password" class="form-control" value="" required>
										<i class="fa fa-eye showpwd" onClick="showPwd('old_password', this)"></i>
                                    </div>
                                </div>

								<div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            KATA SANDI BARU <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="new_password" id="new_password" class="form-control" value="" required>
										<i class="fa fa-eye showpwd" onClick="showPwd('new_password', this)"></i>
                                    </div>
                                </div>

								<div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            KETIK ULANG KATA SANDI BARU <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="re_new_password" id="re_new_password" class="form-control" value="" required>
										<i class="fa fa-eye showpwd" onClick="showPwd('re_new_password', this)"></i>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
	.form-group i {
			position: absolute;
			top: 40px;
			right: 25px;
		}
	}
</style>

<script>
	function showPwd(id, el) {
		let x = document.getElementById(id);
		if (x.type === "password") {
			x.type = "text";
			el.className = 'fa fa-eye-slash showpwd';
		} else {
			x.type = "password";
			el.className = 'fa fa-eye showpwd';
		}
	}
</script>