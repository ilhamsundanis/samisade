<section id="wrapper">
    <div class="login-register" style="background-color:#05372b;">
        <div class="login-box card" style="box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.3)">
            <div style="border-bottom: 5px solid #05372b; text-align: center;">
                <img src="<?php echo base_url(); ?>images/logo/logo.png" style="width: 50%;">
            </div>
            <div class="card-body">
                <form method="post" role="form" id="rent-form" class="form-horizontal form-material">
                
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email" name="email" id="email"> 
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" placeholder="Password" name="password" id="password"> 
                            <i class="fa fa-eye showpwd" onClick="showPwd('password', this)"></i>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            <button class="btn btn-block btn-lg btn-info" type="submit">
                                MASUK
                            </button>
                        </div>
                    </div>

                    <?php echo alert(); ?>

                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            Copyright © 2020 <br> SAMISADE (Satu Milyar Satu Desa)
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<style>
	.form-group i {
			position: absolute;
			right: 25px;
            margin-top: 13px;
		}
	}
</style>

<script>
	function showPwd(id, el) {
		let x = document.getElementById(id);
		if (x.type === "password") {
			x.type = "text";
			el.className = 'fa fa-eye-slash showpwd';
		} else {
			x.type = "password";
			el.className = 'fa fa-eye showpwd';
		}
	}
</script>