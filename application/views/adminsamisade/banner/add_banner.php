<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/banner">
                DAFTAR BANNER
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                TAMBAH BANNER
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            BANNER <span class="text-danger">*</span>
                                        </label>
                                        <input type="file" name="image" id="image" class="form-control" value="" required>
                                        <img src="#" id="image-banner" height="300px;" style="margin-top: 10px;">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-banner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        readURL(this);
    });

</script>