<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/category">
                DAFTAR KATEGORI
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                TAMBAH KATEGORI
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <?php if ( $this->session->userdata('role') == 1 ) { ?>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Kecamatan <span class="text-danger">*</span>
                                            </label>
                                            <select id="kec_id" class="select2 form-control" required>
                                                <option value=""> --- Pilih --- </option>
                                                <?php
                                                    foreach ($list_kecamatan as $data) {
                                                ?>
                                                    <option value="<?php echo $data['id']; ?>">
                                                        <?php echo $data['name']; ?>
                                                    </option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Desa <span class="text-danger">*</span>
                                            </label>
                                            <select id="desa_id" name="desa_id" class="select2 form-control" required>
                                            </select>
                                        </div>
                                    </div>
                                <?php
                                    } else if ( $this->session->userdata('role') == 2 ) {
                                ?>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    DESA <span class="text-danger">*</span>
                                                </label>
                                                <select name="desa_id" id="desa_id" class="select2 form-control" required>
                                                    <option value=""> --- Pilih --- </option>
                                                    <?php
                                                        foreach ($list_desa as $data) {
                                                    ?>
                                                        <option value="<?php echo $data['id']; ?>">
                                                            <?php echo $data['name']; ?>
                                                        </option>
                                                    <?php
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>  
                                <?php
                                    } else{
                                ?>
                                    <input type="hidden" name="desa_id" value="<?php echo $this->session->userdata('desa_id'); ?>">
                                <?php } ?>
                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            NAMA KATEGORI <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="name" value="<?php echo @$post['name']; ?>" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/category/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });
</script>