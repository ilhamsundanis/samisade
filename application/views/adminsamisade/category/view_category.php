<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/category">
                DAFTAR KATEGORI
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                LIHAT KATEGORI
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <?php if ( $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2 ) { ?>
                                    <div class="col-md-12 form-radius">
                                        <div class="form-group">
                                            <label class="control-label">
                                                KECAMATAN <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" value="<?php echo @$category['kec_name']; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-12 form-radius">
                                        <div class="form-group">
                                            <label class="control-label">
                                                DESA <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" value="<?php echo @$category['desa_name']; ?>" disabled>
                                        </div>
                                    </div>
                                    <input type="hidden" name="desa_id" value="<?php echo $category['desa_id']; ?>">
                                <?php
                                    } else {
                                ?>
                                    <input type="hidden" name="desa_id" value="<?php echo $this->session->userdata('desa_id'); ?>">
                                <?php } ?>
                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            NAMA KATEGORI <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="name" value="<?php echo @$category['name']; ?>" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>