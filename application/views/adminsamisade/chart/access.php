<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php //echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                GRAFIK
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/chart/table">
                TABEL
            </a> 
        </li>
    </ul>
</div>

<div class="card">
    <div class="card-header">
        <strong>Filter Data:</strong>
    </div>
    <div class="card-body">
        <form method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            Bulan <span class="text-danger">*</span>
                        </label>
                        <select name="month" class="select2 form-control">
                            <option value=""> --- Pilih --- </option>
                            <?php
                                foreach (month() as $idx => $data) {
                                    if ( @$post['month'] == $idx ) {
                                        $selected = "selected";
                                    } else {
                                        $selected = "";
                                    }
                            ?>
                                <option value="<?php echo $idx; ?>" <?php echo $selected; ?>>
                                    <?php echo $data; ?>
                                </option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            Tahun <span class="text-danger">*</span>
                        </label>
                        <select name="year" class="select2 form-control">
                            <?php for ( $i = 2021; $i <= date("Y"); $i++ ) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-actions pull-right">
                        <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-check"></i> Filter
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <canvas id="chart"></canvas>
</div>

<script>
    var ctx = document.getElementById('chart').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'line',
        data: <?php echo $data_access; ?>,
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes:[{
                    ticks:{
                        beginAtZero:true,
                    }
                }]
            },
        },
    });
</script>