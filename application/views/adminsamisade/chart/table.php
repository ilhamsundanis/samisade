<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php //echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/chart/access">
                GRAFIK
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                TABEL
            </a> 
        </li>
    </ul>
</div>

<div class="card">
    <div class="card-header">
        <strong>Filter Data:</strong>
    </div>
    <div class="card-body">
        <form method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            Bulan <span class="text-danger">*</span>
                        </label>
                        <select name="month" class="select2 form-control">
                            <option value=""> --- Pilih --- </option>
                            <?php
                                foreach (month() as $idx => $data) {
                                    if ( $post['month'] == $idx ) {
                                        $selected = "selected";
                                    } else {
                                        $selected = "";
                                    }
                            ?>
                                <option value="<?php echo $idx; ?>" <?php echo $selected; ?>>
                                    <?php echo $data; ?>
                                </option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            Tahun <span class="text-danger">*</span>
                        </label>
                        <select name="year" class="select2 form-control">
                            <?php for ( $i = 2021; $i <= date("Y"); $i++ ) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-actions pull-right">
                        <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-check"></i> Filter
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <strong>Data Akses:</strong>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <?php
                        foreach ( $list_access as $data ) {
                            if ( $data['date'] == date('Y-m-d') ) {
                                $active = "active";
                            } else {
                                $active = "";
                            }
                    ?>
                        <a class="nav-link <?php echo $active; ?>" id="v-pills-home-<?php echo $data['date']; ?>-tab" data-toggle="pill" href="#v-pills-home-<?php echo $data['date']; ?>" role="tab" aria-controls="v-pills-home-<?php echo $data['date']; ?>" aria-selected="true" style="text-align: right;">
                            <?php echo date_format_indo($data['date'], true); ?>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content" id="v-pills-tabContent">
                    <?php
                        foreach ( $list_access as $data ) {
                            if ( $data['date'] == date('Y-m-d') ) {
                                $active = "show active";
                            } else {
                                $active = "";
                            }
                    ?>
                    <div class="tab-pane fade <?php echo $active; ?>" id="v-pills-home-<?php echo $data['date']; ?>" role="tabpanel" aria-labelledby="v-pills-home-<?php echo $data['date']; ?>-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kecamatan</th>
                                            <th>Waktu Akses</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $no = 0;
                                            foreach ( $data['kecamatan'] as $val ) {
                                                $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $val['name']; ?></td>
                                            <td><?php echo $val['time']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Desa</th>
                                            <th>Waktu Akses</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $no = 0;
                                            foreach ( $data['desa'] as $val ) {
                                                $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $val['name']; ?></td>
                                            <td><?php echo $val['time']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>