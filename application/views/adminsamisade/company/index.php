<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title; ?></h4>
    </div>
</div>

<div class="row">
	<!-- -->
	<div class="col-lg-12 col-xlg-12 col-md-12">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> 
                	<a class="nav-link active" href="javascript:void(0);">
                        PENGATURAN DASAR INSTANSI
                	</a> 
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="home" role="tabpanel">
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    NAMA <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="name" class="form-control" value="<?php echo @$company['name']; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    NAMA APLIKASI <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="name_app" class="form-control" value="<?php echo @$company['name_app']; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    LOGO <span class="text-danger">*</span>
                                                </label>
                                                <input type="file" name="logo" class="form-control">
                                                <img src="<?php echo @$company['logo']; ?>" style="margin-top: 10px;" height="70px">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    DESKRIPSI <span class="text-danger">*</span>
                                                </label>
                                                <textarea name="description" class="form-control" cols="30" rows="10"><?php echo @$company['description']; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    ALAMAT <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="address" class="form-control" value="<?php echo @$company['address']; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    EMAIL <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" class="form-control" name="email" value="<?php echo @$company['email']; ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    TELEPON <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="telephone" class="form-control" value="<?php echo @$company['telephone']; ?>">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

			                <div class="form-actions">
							    <button type="submit" class="btn btn-success"> 
							    	<i class="fa fa-check"></i> SIMPAN
							    </button>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>