<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                DAFTAR ADUAN MASYARAKAT
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <div class="row">
                	<div class="col-md-12">
						<div class="table-responsive">

				            <table id="table-rentist" class="table table-bordered table-striped">
				                <thead>
				                    <tr>
				                        <th>#</th>
					        			<th>Kecamatan</th>
					        			<th>Desa</th>
					        			<th>Nama Pelapor</th>
					        			<th>Email Pelapor</th>
					        			<th>No Telepon</th>
					        			<th>Tanggal Aduan</th>
					        			<th>Status</th>
					        			<th>#</th>
				                    </tr>
				                </thead>
				                <tbody>
				                	<?php
				                		$no = 0;
				                		foreach ($list_complaint as $data) {
				                			$no++;
				                	?>
					                    <tr>
				                            <td><?php echo $no; ?></td>
						        			<td>
                                                <?php echo ! empty($data['kec_name']) ? $data['kec_name'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['desa_name']) ? $data['desa_name'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['name']) ? $data['name'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['email']) ? $data['email'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['phone']) ? $data['phone'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['created_at']) ? $data['created_at'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $class = "";
                                                    if ( $data['status'] == "pending" ) {
                                                        $class = "warning";
                                                    } else {
                                                        $class = "success";
                                                    }
                                                ?>
                                                <label class="label label-<?php echo $class; ?>">
                                                    <?php echo status_complaint()[$data['status']] ?>
                                                </label>
                                            </td>
						        			<td>
						        				<a href="<?php echo base_url(); ?>adminsamisade/complaint/view/<?php echo $data['id']; ?>" class="btn btn-info" style="margin-top: -7px;">
				                                    <i class="fa fa-eye"></i>
				                                </a>
						        			</td>
					                    </tr>
				                    <?php
				                		}
				                	?>
				                </tbody>
				            </table>
				        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>