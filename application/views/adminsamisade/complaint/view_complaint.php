<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/complaint">
                DAFTAR ADUAN MASYARAKAT
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                LIHAT ADUAN MASYARAKAT
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>Kecamatan</td>
                            <td>:</td>
                            <td><?php echo $complaint['kec_name']; ?></td>
                        </tr>
                        <tr>
                            <td>Desa</td>
                            <td>:</td>
                            <td><?php echo $complaint['desa_name']; ?></td>
                        </tr>
                        <tr>
                            <td>Nama Pelapor</td>
                            <td>:</td>
                            <td><?php echo $complaint['name']; ?></td>
                        </tr>
                        <tr>
                            <td>Email Pelapor</td>
                            <td>:</td>
                            <td><?php echo $complaint['email']; ?></td>
                        </tr>
                        <tr>
                            <td>No Telepon</td>
                            <td>:</td>
                            <td><?php echo $complaint['phone']; ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?php echo $complaint['address']; ?></td>
                        </tr>
                        <tr>
                            <td>Aduan</td>
                            <td>:</td>
                            <td><?php echo $complaint['description']; ?></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>
                                <select name="status" class="form-control">
                                    <?php 
                                        foreach( status_complaint() as $index => $value ) { 
                                            if ( $complaint['status'] == $index ) {
                                                $selected = "selected";
                                            } else {
                                                $selected = "";
                                            }
                                    ?>
                                        <option value="<?php echo $index; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
                                    <?php }?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> <?php echo $this->lang->line('button_save'); ?>
                                </button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

