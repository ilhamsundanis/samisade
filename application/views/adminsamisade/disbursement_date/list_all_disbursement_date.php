<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/disbursement_date">
                DAFTAR TANGGAL PENCAIRAN DANA
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                DAFTAR SEMUA TANGGAL PENCAIRAN DANA
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/disbursement_date/add">
                TAMBAH TANGGAL PENCAIRAN DANA
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <?php 
                        foreach ( $list_disbursement_date as $index => $data ) { 
                            if ( $index == 0 ) {
                                $active = "active";
                            } else {
                                $active = "";
                            }
                    ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo $active; ?>" id="tabwave<?php echo $data['wave']; ?>-tab" data-toggle="tab" href="#tabwave<?php echo $data['wave']; ?>" role="tab" aria-controls="tabwave<?php echo $data['wave']; ?>" aria-selected="true">GEL <?php echo $data['wave']; ?></a>
                    </li>
                    <?php } ?>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <?php 
                        foreach ( $list_disbursement_date as $index => $data ) { 
                            if ( $index == 0 ) {
                                $active = "show active";
                            } else {
                                $active = "";
                            }
                    ?>
                    <div class="tab-pane fade <?php echo $active; ?>" id="tabwave<?php echo $data['wave']; ?>" role="tabpanel" aria-labelledby="tabwave<?php echo $data['wave']; ?>-tab">
                        <br>
                        
                        <div class="row">
                            <div class="col-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link active" id="wave<?php echo $data['wave']; ?>-step1-tab" data-toggle="pill" href="#wave<?php echo $data['wave']; ?>-step1" role="tab" aria-controls="wave<?php echo $data['wave']; ?>-step1" aria-selected="true">Tahap 1 (40%)</a>
                                    <a class="nav-link" id="wave<?php echo $data['wave']; ?>-step2-tab" data-toggle="pill" href="#wave<?php echo $data['wave']; ?>-step2" role="tab" aria-controls="wave<?php echo $data['wave']; ?>-step2" aria-selected="false">Tahap 2 (60%)</a>
                                </div>
                            </div>
                            <div class="col-9">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="wave<?php echo $data['wave']; ?>-step1" role="tabpanel" aria-labelledby="wave<?php echo $data['wave']; ?>-step1-tab">

                                        <table class="table table-bordered table-striped table-sm">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>KECAMATAN</th>
                                                    <th>DESA</th>
                                                    <th>DANA</th>
                                                    <th>TANGGAL</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $no = 0;
                                                $total = 0;
                                                foreach ( $data['data']['step1'] as $val ) {
                                                    $no++;
                                                    $total += $val['liquid_funds'];
                                            ?>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $val['kec_name']; ?></td>
                                                    <td><?php echo $val['desa_name']; ?></td>
                                                    <td class="text-right">Rp. <?php echo number_format($val['liquid_funds'],0,',','.'); ?></td>
                                                    <td><?php echo $val['disbursement_date']; ?></td>
                                                </tr>
                                            </tbody>
                                            <?php
                                                }
                                            ?>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3" class="text-right">Total Keseluruhan</td>
                                                    <td class="text-right">Rp. <?php echo number_format($total,0,',','.'); ?></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                    <div class="tab-pane fade" id="wave<?php echo $data['wave']; ?>-step2" role="tabpanel" aria-labelledby="wave<?php echo $data['wave']; ?>-step2-tab">
                                    
                                        <table class="table table-bordered table-striped table-sm">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>KECAMATAN</th>
                                                    <th>DESA</th>
                                                    <th>DANA</th>
                                                    <th>TANGGAL</th>
                                                </tr>
                                            </thead>
                                            <?php
                                                $no = 0;
                                                $total = 0;
                                                foreach ( $data['data']['step2'] as $val ) {
                                                    $no++;
                                                    $total += $val['liquid_funds'];
                                            ?>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $val['kec_name']; ?></td>
                                                    <td><?php echo $val['desa_name']; ?></td>
                                                    <td class="text-right">Rp. <?php echo number_format($val['liquid_funds'],0,',','.'); ?></td>
                                                    <td><?php echo $val['disbursement_date']; ?></td>
                                                </tr>
                                            </tbody>
                                            <?php
                                                }
                                            ?>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="3" class="text-right">Total Keseluruhan</td>
                                                    <td class="text-right">Rp. <?php echo number_format($total,0,',','.'); ?></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>