<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/disbursement_date">
                DAFTAR TANGGAL PENCAIRAN DANA
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/disbursement_date/all">
                DAFTAR SEMUA TANGGAL PENCAIRAN DANA
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                LIHAT TANGGAL PENCAIRAN DANA
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            kecamatan <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" value="<?php echo $disbursement_date['kec_name']; ?>" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Desa <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" value="<?php echo $disbursement_date['desa_name']; ?>" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Tahap <span class="text-danger">*</span>
                                        </label>
                                        <?php
                                            $selected = "";
                                            if ( $disbursement_date['step'] == 1 ) {
                                                $selected = "1 (40%)";
                                            } else {
                                                $selected = "2 (60%)";   
                                            }
                                        ?>
                                        <input type="text" value="<?php echo $selected; ?>" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Dana Cair <span class="text-danger">*</span>
                                        </label>
                                        <input type="number" name="liquid_funds" value="<?php echo $disbursement_date['liquid_funds']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Tanggal Dana Cair <span class="text-danger">*</span>
                                        </label>
                                        <input type="date" name="disbursement_date" value="<?php echo $disbursement_date['disbursement_date']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Gelombang <span class="text-danger">*</span>
                                        </label>
                                        <input type="number" name="wave" value="<?php echo $disbursement_date['wave']; ?>" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/proposals_submission/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });
</script>