<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/employee/index/<?php echo $this->uri->segment(4); ?>">
                DAFTAR PEGAWAI
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                LIHAT PEGAWAI
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Tipe <span class="text-danger">*</span>
                                    </label>
                                    <select name="rab_item_id" id="rab_item_id" class="form-control" required>
                                        <option value=""> --- Pilih --- </option>
                                        <?php
                                            foreach ($list_rab_item as $data) {
                                                if ( $detail['rab_item_id'] == $data['id'] ) {
                                                    $selected = "selected";
                                                } else {
                                                    $selected = "";
                                                }
                                        ?>
                                            <option value="<?php echo $data['id']; ?>" <?php echo $selected; ?>>
                                                <?php echo $data['name']; ?>
                                            </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        NIK <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="nik" value="<?php echo $detail['nik']; ?>" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Nama <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="name" value="<?php echo $detail['name']; ?>" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Jenis Kelamin <span class="text-danger">*</span>
                                    </label>
                                    <?php
                                        $selectedL = "";
                                        $selectedP = "";
                                        if ( $detail['gender'] == "l" ) {
                                            $selectedL = "selected";
                                        } else if ( $detail['gender'] == "p" ) {
                                            $selectedP = "selected";
                                        }
                                    ?>
                                    <select name="gender" class="form-control">
                                        <option value="l" <?php echo $selectedL; ?>>Laki - Laki</option>
                                        <option value="p" <?php echo $selectedP; ?>>Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Tanggal Lahir <span class="text-danger">*</span>
                                    </label>
                                    <input type="date" class="form-control" name="birthdate" value="<?php echo $detail['birthdate']; ?>" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Alamat <span class="text-danger">*</span>
                                    </label>
                                    <textarea name="address" class="form-control"><?php echo $detail['address']; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>