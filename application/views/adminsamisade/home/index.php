<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<style>
    .hscroll {
        width: 100%;
        overflow-x: auto;
        white-space: nowrap;
    }
</style>

<!-- $content -->
<div class="row page-titles">
    <div class="col-md-12 align-self-center">
        <center>
            <h4 class="text-themecolor"><strong>SELAMAT DATANG DI ADMIN SAMISADE (Satu Milyar Satu Desa)</strong></h4>
        </center>
    </div>
</div>

<div class="card-group">
    <?php
        if ( $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2 ) {
    ?>
    <div class="card">
        <div class="card-body">
            <div style="text-align: center;">
                <i class="fa fa-check-circle" style="font-size: 30px;"></i> <br>
                <strong>VERIFIKASI</strong>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4 text-center">
                    <strong>PROPOSAL</strong>
                    <h3 class="text-info">
                        <a href="<?php echo base_url(); ?>adminsamisade/verification/proposal">
                            <?php echo count_verification()['proposal']; ?>
                        </a>
                    </h3>
                </div>
                <div class="col-md-4 text-center">
                    <strong>RAB</strong>
                    <h3 class="text-info">
                        <a href="<?php echo base_url(); ?>adminsamisade/verification/rab">
                            <?php echo count_verification()['rab']; ?>
                        </a>
                    </h3>
                </div>
                <div class="col-md-4 text-center">
                    <strong>REALISASI</strong>
                    <h3 class="text-info">
                        <a href="<?php echo base_url(); ?>adminsamisade/verification/realization">
                            <?php echo count_verification()['rab_item_realization']; ?>
                        </a>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php
        if ( $this->session->userdata('role') == 1 ) {
    ?>
    <!-- <div class="card">
        <div class="card-body">
            <div style="text-align: center;">
                <i class="fa fa-calendar" style="font-size: 30px;"></i> <br>
                <strong>PENGATURAN PENCAIRAN DANA</strong>
                <hr>
                <div class="row">
                    <div class="col-md-6 text-center">
                        <strong>TAHAP 1</strong>
                        <h3 class="text-info">
                            <a href="<?php //echo base_url(); ?>adminsamisade/disbursement_date/index/1">
                                <?php //echo count(disbursement_date(1)); ?>
                            </a>
                        </h3>
                    </div>
                    <div class="col-md-6 text-center">
                        <strong>TAHAP 2</strong>
                        <h3 class="text-info">
                            <a href="<?php //echo base_url(); ?>adminsamisade/disbursement_date/index/2">
                                <?php //echo count(disbursement_date(2)); ?>
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="card">
        <div class="card-body">
            <div style="text-align: center;">
                <i class="fa fa-bullhorn" style="font-size: 30px;"></i> <br>
                <strong>ADUAN MASYARAKAT</strong>
                <hr>
                <div class="row">
                    <div class="col-md-12 text-center">
                    <strong>PELAPORAN</strong>
                        <h3 class="text-info">
                            <a href="<?php echo base_url(); ?>adminsamisade/complaint">
                                <?php echo count_complaint()?>
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong>Filter Data:</strong>
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="row">
                        <?php if ( $this->session->userdata('role') != 3 ) { ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    Kecamatan <span class="text-danger">*</span>
                                </label>
                                <select name="kec_id" id="kec_id" class="select2 form-control">
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_kecamatan as $data) {
                                    ?>
                                        <option value="<?php echo $data['id']; ?>-<?php echo $data['name']; ?>">
                                            <?php echo $data['name']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    Desa <span class="text-danger">*</span>
                                </label>
                                <select id="desa_id" name="desa_id" class="select2 form-control">
                                </select>
                            </div>
                        </div>
                        <?php } else { ?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Kecamatan <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['kec_name'];?>" disabled>
                                    <input type="hidden" name="kec_id" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['kec_id'];?>-<?php echo get_kecamatan($this->session->userdata('desa_id'))['kec_name'];?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Desa <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['name'];?>" disabled>
                                    <input type="hidden" name="desa_id" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['id'];?>-<?php echo get_kecamatan($this->session->userdata('desa_id'))['name'];?>">
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    Jenis Pembangunan <span class="text-danger">*</span>
                                </label>
                                <select name="activity_id" id="activity_id" class="select2 form-control">
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_activity as $data) {
                                    ?>
                                        <option value="<?php echo $data['id']; ?>-<?php echo $data['name']; ?>">
                                            <?php echo $data['name']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    Anggaran <span class="text-danger">*</span>
                                </label>
                                <select name="anggaran" id="anggaran" class="select2 form-control">
                                    <option value=""> --- Pilih --- </option>
                                        <option value="asc">
                                            Terendah
                                        </option>
                                        <option value="desc">
                                            Terbesar
                                        </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="col-md-12" style="margin-bottom: 20px;">
        <?php echo $result_search; ?>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <strong>Grafik Total Pembangunan</strong>
            </div>
            <div class="card-body">
                <canvas id="chart1" height="200"></canvas>
            </div>
        </div>
    </div>

    <?php if ( $this->session->userdata('role') != 3 ) { ?>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <strong>Grafik 10 Teratas Pendanaan Pembangunan Desa</strong>
            </div>
            <div class="card-body">
                <canvas id="chart2" height="200"></canvas>
            </div>
        </div>
    </div>
    <?php } ?>

    <?php foreach( $list_all_data as $data ) { ?>
    <div class="col-md-12">
        <!-- <div class="card-group"> -->
            <div class="card">
                <div class="card-header">
                    <strong style="float: left;">KECAMATAN <?php echo $data['proposal']['kec_name']; ?> | DESA <?php echo $data['proposal']['desa_name']; ?></strong>
                    <strong style="float: right;">TOTAL ANGGARAN Rp <?php echo number_format($data['total_cost'],0,',','.'); ?></strong>
                </div>
                <div class="card-group">
                    <?php foreach( $data['rab'] as $value ) { ?>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <p class="text-muted">
                                                Pembangunan: <?php echo $value['activity_name']; ?> <br>
                                                <label class="label label-<?php echo parsing_status_class()[$value['status']]; ?>">
                                                    <?php echo parsing_status()[$value['status']]; ?>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="ml-auto">
                                            <h4 class="text-primary">
                                                Rp. <?php echo number_format($value['total_cost'],0,',','.'); ?>
                                            </h4>
                                        </div>
                                    </div>
                                    <div>
                                        <table class="table table-bordered table-striped table-sm">
                                            <tbody>
                                                <tr>
                                                    <td width="50%" style="text-align: center;">
                                                        TAHAP 1 <br>
                                                        <h4 class="text-primary">
                                                            Rp. <?php echo number_format(($value['total_cost'] * 0.4),0,',','.'); ?>
                                                        </h4>
                                                    </td>
                                                    <td width="50%" style="text-align: center;">
                                                        TAHAP 2 <br>
                                                        <h4 class="text-primary">
                                                            Rp. <?php echo number_format(($value['total_cost'] * 0.6),0,',','.'); ?>
                                                        </h4>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>Persen</td>
                                                                <td>:</td>
                                                                <td><?php echo $value['step1_percentage']; ?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <label class="label label-<?php echo parsing_status_class()[$value['step1_status']]; ?>">
                                                                        <?php echo parsing_status()[$value['step1_status']]; ?>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Sisa Saldo</td>
                                                                <td>:</td>
                                                                <td>
                                                                    Rp. <?php echo number_format($value['step1_balance'],0,',','.'); ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>Persen</td>
                                                                <td>:</td>
                                                                <td><?php echo $value['step2_percentage']; ?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>:</td>
                                                                <td>
                                                                    <label class="label label-<?php echo parsing_status_class()[$value['step2_status']]; ?>">
                                                                        <?php echo parsing_status()[$value['step2_status']]; ?>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Sisa Saldo</td>
                                                                <td>:</td>
                                                                <td>
                                                                    Rp. <?php echo number_format($value['step2_balance'],0,',','.'); ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div>
                                        <?php
                                            if ( $value['activity_type'] == "maps" ) {
                                                $latitude = urlencode($value['location']['point1']);
                                            } else if ( $value['activity_type'] == "maps_direction" ) {
                                                $latitude = urlencode($value['location']['point1']) . "/" . urlencode($latitude = $value['location']['point2']);
                                            }
                                        ?>
                                        <a target="_blank" href="<?php echo base_url(); ?>adminsamisade/home/popup/<?php echo $value['activity_type']; ?>/<?php echo $latitude; ?>" class="btn btn-info">
                                            <i class="fa fa-map-marker"></i> Lihat Peta Pembangunan Desa
                                        </a>
                                        <a href="<?php echo base_url() ?>adminsamisade/home/export_data/<?php echo $data['proposal_id']; ?>/<?php echo $value['id']; ?>/<?php echo $value['activity_id']; ?>" target="_blank" class="btn btn-success">Export Data</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        <!-- </div> -->
    </div>
    <?php } ?>
</div>

<script>
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    var ctx = document.getElementById('chart1').getContext('2d');
    var chart1 = new Chart(ctx, {
        type: 'horizontalBar',
        data: <?php echo $get_chart_activities; ?>,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes:[
                    {
                    ticks:{
                        beginAtZero:true
                    }
                }]
            },
        }
    });

    <?php if ( $this->session->userdata('role') != 3 ) { ?>

    var ctx2 = document.getElementById('chart2').getContext('2d');
    var chart2 = new Chart(ctx2, {
        type: 'horizontalBar',
        data: <?php echo $get_chart_top_total_cost; ?>,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes:[
                    {
                    ticks:{
                        beginAtZero:true,
                        callback: function(value, index, values) {
                            return formatRupiah(value.toString(), "Rp. ");
                        }
                    }
                }]
            }
        }
    });
    
    <?php } ?>

    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/home/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "-" + data[i].name + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });
</script>