
        <?php
        
            if ( $type == "maps_direction" ) {
        ?>
            <div class="row" style=" margin-top: 30px;">
                <div class="col-md-8">
                    <div id="map" style="height: 500px; width: 100%;"></div>
                </div>
                <div class="col-md-4" style="padding: 15px;
                    width: 50%;
                    height: 500px;
                    overflow: scroll;
                    border: 1px solid #ccc;">
                    <div id="right-panel">
                        <p>Total Jarak: <span id="total"></span></p>
                    </div>
                </div>
            </div>
        <?php
            } elseif ( $type == "maps" ) {
        ?>
                <div id="map" style="height: 500px; width: 100%; margin-top: 30px;"></div>
        <?php
            }
        ?>

<?php
    if ( $type == "maps_direction" ) {
?>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly&language=id"
            async
        ></script>
        <script>
            function initMap() {
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 4,
                    center: { lat: -24.345, lng: 134.46 },
                });
                const directionsService = new google.maps.DirectionsService();
                const directionsRenderer = new google.maps.DirectionsRenderer({
                    map,
                    panel: document.getElementById("right-panel"),
                });
                directionsRenderer.addListener("directions_changed", () => {
                    computeTotalDistance(directionsRenderer.getDirections());
                });
                displayRoute(
                    "<?php echo $point1; ?>",
                    "<?php echo $point2; ?>",
                    directionsService,
                    directionsRenderer
                );
            }

            function displayRoute(origin, destination, service, display) {
                service.route(
                    {
                        origin: origin,
                        destination: destination,
                        travelMode: google.maps.TravelMode.WALKING,
                        avoidTolls: true,
                    },
                    (result, status) => {
                        if (status === "OK") {
                            display.setDirections(result);
                        } else {
                            alert("Could not display directions due to: " + status);
                        }
                    }
                );
            }

            function computeTotalDistance(result) {
                let total = 0;
                const myroute = result.routes[0];

                for (let i = 0; i < myroute.legs.length; i++) {
                    total += myroute.legs[i].distance.value;
                }
                total = total / 1000;
                document.getElementById("total").innerHTML = total + " km";
            }
        </script>
<?php
    } else if ($type == "maps") {
?>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly"
            async
        ></script>

        <script>
            function initMap() {
                const myLatLng = { lat: -25.363, lng: 131.044 };
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 15,
                    center: myLatLng,
                });
                var vMarker = new google.maps.Marker({
                    position: { lat: <?php echo explode(",", $point1)[0]; ?>, lng: <?php echo explode(",", $point1)[1]; ?> },
                    map,
                });

                // centers the map on markers coords
                map.setCenter(vMarker.position);

                // adds the marker on the map
                vMarker.setMap(map);
            }
        </script>
<?php
    }
?>