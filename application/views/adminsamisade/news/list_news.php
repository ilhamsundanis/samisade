<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                DAFTAR BERITA
            </a> 
        </li>
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/news/add">
                TAMBAH BERITA
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <div class="row">
                	<div class="col-md-12">
                		<div class="row">
							<div class="col-md-12">
								<a href="javascript:void(0)" class="btn btn-danger deleteall">
                                    <i class="fa fa-times"></i> <?php echo $this->lang->line('button_delete'); ?>
                                </a>
							</div>
						</div>
						<div class="table-responsive">						
	                		<input type="hidden" value="<?php echo base64_encode('adminsamisade/news/delete'); ?>">
				            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_choose_one')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_success')); ?>">
                            <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_failed')); ?>">

				            <table id="table-rentist" class="table table-bordered table-striped">
				                <thead>
				                    <tr>
				                        <th>#</th>
					        			<th>GAMBAR</th>
					        			<th>JUDUL</th>
					        			<th>DESKRIPSI</th>
					        			<th>#</th>
				                    </tr>
				                </thead>
				                <tbody>
				                	<?php
				                		$no = 0;
				                		foreach ($list_news as $data) {
				                			$no++;
				                	?>
					                    <tr id="id-<?php echo $data['id']; ?>">
				                            <td><input type="checkbox" value="<?php echo $data['id']; ?>"></td>
						        			<td>
                                                <a href="<?php echo $data['image']; ?>" data-fancybox data-caption="<?php echo $data['title']; ?>">
                                                    <img src="<?php echo $data['image']; ?>" height="50px;"/>
                                                </a>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['title']) ? $data['title'] : "-"; ?>
                                            </td>
                                            <td>
                                                <?php echo ! empty($data['partial_description']) ? $data['partial_description'] : "-"; ?>
                                            </td>
						        			<td>
						        				<a href="<?php echo base_url(); ?>adminsamisade/news/view/<?php echo $data['id']; ?>" class="btn btn-info" style="margin-top: -7px;">
				                                    <i class="fa fa-edit"></i>
				                                </a>
						        			</td>
					                    </tr>
				                    <?php
				                		}
				                	?>
				                </tbody>
				            </table>
				        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $('.deleteall').click(function(){
        var url    = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(0).val());
        var alert1 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(1).val());
        var alert2 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(2).val());
        var alert3 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(3).val());
        var alert4 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(4).val());

        swal({
            // title: alert1,
            text: alert1,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = [];

                $(this).parent().parent().parent().find('tbody :checkbox:checked').each(function(i){
                    id[i] = $(this).val();
                });

                if ( id.length === 0) {
            
                    swal(alert2, {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
            
                } else {
            
                    $.ajax({
                        url: '<?php echo base_url(); ?>' + url,
                        method:'POST',
                        data:{id:id},
                        success:function(data) {
                            if (data == "success") {
                                for ( var i = 0; i < id.length; i++ ) {
                                    $('tr#id-'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#id-'+id[i]+'').fadeOut('slow');
                                }

                                swal(alert3, {
                                    icon: "success",
                                    buttons: false,
                                    timer: 2000,
                                });
                            } else {
                                swal(alert4, {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000,
                                });
                            }
                        }
                    });
                }
            }
        });
    });

    function view_image(that) {
        alert("berhasil");
        $(that).fancybox({
            helpers: {
                title : {
                    ype : 'float'
                }
            }
        });
    }
</script>