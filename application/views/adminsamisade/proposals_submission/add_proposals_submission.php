<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

<style>
* {
    margin: 0;
    padding: 0
}


#heading {
    text-transform: uppercase;
    color: #673AB7;
    font-weight: normal
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #673AB7;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #673AB7;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #673AB7;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #673AB7;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    /* margin-bottom: 30px; */
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #00a687
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar #proposal_submission:before {
    font-family: FontAwesome;
    content: "\f1ea"
}

#progressbar #rab:before {
    font-family: FontAwesome;
    content: "\f022"
}

#progressbar #realisasi:before {
    font-family: FontAwesome;
    content: "\f093"
}

#progressbar #finish:before {
    font-family: FontAwesome;
    content: "\f05d"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #00a687
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #00a687
}

.fit-image {
    width: 100%;
    object-fit: cover
}
</style>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <form id="msform">
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active" id="proposal_submission"><strong>PENGAJUAN PROPOSAL</strong></li>
            <li id="rab"><strong>INPUT RAB</strong></li>
            <li id="realisasi"><strong>REALISASI</strong></li>
            <li id="finish"><strong>SELESAI</strong></li>
        </ul>
    </form>
</div>

<?php
    echo alert()
?>

<form method="post" enctype="multipart/form-data">
    <div class="card">
        <div class="card-header">
            <strong>PENGAJUAN PROPOSAL</strong>
        </div>
        <div class="card-body">
            

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">

                            <?php
                                if ( $this->session->userdata('role') == 1 ) {
                            ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Kecamatan <span class="text-danger">*</span>
                                        </label>
                                        <select id="kec_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_kecamatan as $data) {
                                            ?>
                                                <option value="<?php echo $data['id']; ?>">
                                                    <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Desa <span class="text-danger">*</span>
                                        </label>
                                        <select id="desa_id" name="desa_id" class="select2 form-control" required>
                                        </select>
                                    </div>
                                </div>
                            <?php
                                } else if ( $this->session->userdata('role') == 2 ) {
                            ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Desa <span class="text-danger">*</span>
                                        </label>
                                        <select id="desa_id" name="desa_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_desa as $data) {
                                                    if ($post['desa_id'] == $data['id']) {
                                                        $selected1 = "selected";
                                                    } else {
                                                        $selected1 = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                    <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php
                                } else {
                            ?>
                                    <input type="hidden" name="desa_id" value="<?php echo $this->session->userdata('desa_id'); ?>">
                            <?php } ?>

                            <div class="col-md-12 form-radius">
                                <div class="form-group">
                                    <label class="control-label">
                                        JUDUL <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="title" value="" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        DOKUMEN <span class="text-danger">*</span> <i>Max File 6Mb (.pdf)</i>
                                    </label>
                                    <input type="file" class="form-control" name="file" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions pull-right">
                            <button type="submit" name="save" class="btn btn-success"> 
                                <i class="fa fa-check"></i> SIMPAN
                            </button>
                        </div>
                    </div>
                </div>
            
        </div>
    </div>
</form>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/proposals_submission/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });
</script>