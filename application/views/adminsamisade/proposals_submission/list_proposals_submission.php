<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                DAFTAR PENGAJUAN PROPOSAL
            </a> 
        </li>
        <?php 
            if ( $this->session->userdata('role') == 3) { 
                if ( count(check_proposal()) == 0 ) {
        ?>
        <li class="nav-item"> 
        	<a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/proposals_submission/add">
                TAMBAH PENGAJUAN PROPOSAL
            </a> 
        </li>
        <?php 
                }
            } else { 
        ?>
            <li class="nav-item"> 
                <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/proposals_submission/add">
                    TAMBAH PENGAJUAN PROPOSAL
                </a> 
            </li>
        <?php } ?>
    </ul>
    <!-- Tab panes -->
</div>

<div class="row">
<div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong>Filter Data:</strong>
            </div>
            <div class="card-body">
                <form method="post">
                    <div class="row">
                        <?php if ( $this->session->userdata('role') != 3 ) { ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    Kecamatan <span class="text-danger">*</span>
                                </label>
                                <select name="kec_id" id="kec_id" class="select2 form-control">
                                    <option value=""> --- Pilih --- </option>
                                    <?php
                                        foreach ($list_kecamatan as $data) {
                                    ?>
                                        <option value="<?php echo $data['id']; ?>-<?php echo $data['name']; ?>">
                                            <?php echo $data['name']; ?>
                                        </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label">
                                    Desa <span class="text-danger">*</span>
                                </label>
                                <select id="desa_id" name="desa_id" class="select2 form-control">
                                </select>
                            </div>
                        </div>
                        <?php } else { ?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Kecamatan <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['kec_name'];?>" disabled>
                                    <input type="hidden" name="kec_id" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['kec_id'];?>-<?php echo get_kecamatan($this->session->userdata('desa_id'))['kec_name'];?>">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Desa <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['name'];?>" disabled>
                                    <input type="hidden" name="desa_id" value="<?php echo get_kecamatan($this->session->userdata('desa_id'))['id'];?>-<?php echo get_kecamatan($this->session->userdata('desa_id'))['name'];?>">
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    Proposal <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="proposal" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success" name="filter"> 
                                    <i class="fa fa-check"></i> Filter
                                </button>
                                <button type="submit" class="btn btn-danger" name="reset"> 
                                    <i class="fa fa-check"></i> Reset
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-bottom: 20px;">
        <?php echo $this->session->userdata('result_search'); ?>
    </div>

    <div class="col-md-12">
        <a href="javascript:void(0)" class="btn btn-danger deleteall">
            <i class="fa fa-times"></i> <?php echo $this->lang->line('button_delete'); ?>
        </a>
        <input type="hidden" value="<?php echo base64_encode('adminsamisade/proposals_submission/delete'); ?>">
        <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete')); ?>">
        <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_choose_one')); ?>">
        <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_success')); ?>">
        <input type="hidden" value="<?php echo base64_encode($this->lang->line('alert_delete_failed')); ?>">
    </div>
</div>
<br>
<div class="row">
    <?php
        $no = 0;
        foreach ($list_proposals_submission as $data) {
            $no++;
    ?>
        <div class="col-md-12" id="id-<?php echo $data['id']; ?>">
            <!-- <div class="card-group"> -->
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-1">
                                <input type="checkbox" value="<?php echo $data['id']; ?>" style="height: 30px; width: 30px;">
                            </div>
                            <div class="col-md-10" style="text-align: center;">
                                
                            </div>
                            <div class="col-md-1" style="text-align: right;">
                                <a href="<?php echo base_url(); ?>adminsamisade/proposals_submission/view/<?php echo $data['id']; ?>" class="btn btn-info">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="border-bottom: 1px solid #dfe2e5;">
                        <table class="table-sm table table-bordered table-striped display nowrap" width="100%">
                            <tr>
                                <td>Kecamatan</td>
                                <td>:</td>
                                <td><?php echo ! empty($data['kec_name']) ? $data['kec_name'] : "-"; ?></td>
                            </tr>
                            <tr>
                                <td>Desa</td>
                                <td>:</td>
                                <td><?php echo ! empty($data['desa_name']) ? $data['desa_name'] : "-"; ?></td>
                            </tr>
                            <tr>
                                <td>Judul</td>
                                <td>:</td>
                                <td><?php echo ! empty($data['title']) ? $data['title'] : "-"; ?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td>
                                    <label class="label label-<?php echo parsing_status_class()[$data['status']]; ?>">
                                        <?php echo ! empty($data['status']) ? parsing_status()[$data['status']] : "-"; ?>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>Tanggal Dibuat</td>
                                <td>:</td>
                                <td><?php echo ! empty($data['created_at']) ? $data['created_at'] : "-"; ?></td>
                            </tr>
                        </table>
                        <?php
                            $totalAkumulasiTahap1 = 0;
                            $totalAkumulasiTahap2 = 0;
                            $no = 0;
                            foreach ($data['rab'] as $dataRAB) {
                                $no++;
                                $totalAkumulasiTahap1 += ($dataRAB['total_cost'] * 0.4);
                                $totalAkumulasiTahap2 += ($dataRAB['total_cost'] * 0.6);
                            }

                            if ( $no > 1 ) {
                        ?>
                        <table width="100%" class="table-sm table table-bordered table-striped display nowrap" style="text-align: center;">
                            <tr>
                                <td colspan="2">
                                    TOTAL AKUMULASI PENCAIRAN DANA REALISASI
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    TAHAP 1 <br>
                                    <h4 class="text-primary">Rp. <?php echo number_format($totalAkumulasiTahap1,0,',','.'); ?></h4>
                                </td>
                                <td>
                                    TAHAP 2 <br>
                                    <h4 class="text-primary">Rp. <?php echo number_format($totalAkumulasiTahap2,0,',','.'); ?></h4>
                                </td>
                            </tr>
                        </table>
                        <?php } ?>
                    </div>
                    <div class="card-group">
                        <?php foreach( $data['rab'] as $value ) { ?>
                        <div class="card">
                            <div class="card-body">
                                <div style="overflow-x:auto;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="d-flex no-block align-items-center">
                                                <div>
                                                    <p class="text-muted">
                                                        Pembangunan: <?php echo $value['activity_name']; ?> <br>
                                                        <label class="label label-<?php echo parsing_status_class()[$value['status']]; ?>">
                                                            <?php echo parsing_status()[$value['status']]; ?>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="ml-auto">
                                                    <h4 class="text-primary">
                                                        Rp. <?php echo number_format($value['total_cost'],0,',','.'); ?>
                                                    </h4>
                                                </div>
                                            </div>
                                            <div>
                                                <table class="table table-bordered table-striped table-sm display nowrap">
                                                    <tbody>
                                                        <tr>
                                                            <td width="50%" style="text-align: center;">
                                                                TAHAP 1 <br>
                                                                <h4 class="text-primary">
                                                                    Rp. <?php echo number_format(($value['total_cost'] * 0.4),0,',','.'); ?>
                                                                </h4>
                                                            </td>
                                                            <td width="50%" style="text-align: center;">
                                                                TAHAP 2 <br>
                                                                <h4 class="text-primary">
                                                                    Rp. <?php echo number_format(($value['total_cost'] * 0.6),0,',','.'); ?>
                                                                </h4>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>Persen</td>
                                                                        <td>:</td>
                                                                        <td><?php echo $value['step1_percentage']; ?>%</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Status</td>
                                                                        <td>:</td>
                                                                        <td>
                                                                            <label class="label label-<?php echo parsing_status_class()[$value['step1_status']]; ?>">
                                                                                <?php echo parsing_status()[$value['step1_status']]; ?>
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Sisa Saldo</td>
                                                                        <td>:</td>
                                                                        <td>
                                                                            Rp. <?php echo number_format($value['step1_balance'],0,',','.'); ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>Persen</td>
                                                                        <td>:</td>
                                                                        <td><?php echo $value['step2_percentage']; ?>%</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Status</td>
                                                                        <td>:</td>
                                                                        <td>
                                                                            <label class="label label-<?php echo parsing_status_class()[$value['step2_status']]; ?>">
                                                                                <?php echo parsing_status()[$value['step2_status']]; ?>
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Sisa Saldo</td>
                                                                        <td>:</td>
                                                                        <td>
                                                                            Rp. <?php echo number_format($value['step2_balance'],0,',','.'); ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <a href="<?php echo base_url() ?>adminsamisade/home/export_data/<?php echo $data['id']; ?>/<?php echo $value['id']; ?>/<?php echo $value['activity_id']; ?>" target="_blank" class="btn btn-success">Export Data</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            <!-- </div> -->
        </div>
    <?php } ?>

    <div class="col-md-12">
        <?php 
            echo $this->pagination->create_links();
        ?>
    </div>
</div>
<script type="text/javascript">
  $('.deleteall').click(function(){
        <?php 
            if ( $this->session->userdata('role') == 3 ) {    
        ?>
            var url    = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(2).val());
            var alert1 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(3).val());
            var alert2 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(4).val());
            var alert3 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(5).val());
            var alert4 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(6).val());
        <?php } else { ?>
            var url    = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(0).val());
            var alert1 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(1).val());
            var alert2 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(2).val());
            var alert3 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(3).val());
            var alert4 = atob($(this).parent().parent().parent().find('input[type=hidden]').eq(4).val());
        <?php } ?>

        swal({
            // title: alert1,
            text: alert1,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = [];

                $(this).parent().parent().parent().find(':checkbox:checked').each(function(i){
                    id[i] = $(this).val();
                });

                if ( id.length === 0) {
            
                    swal(alert2, {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
            
                } else {
            
                    $.ajax({
                        url: '<?php echo base_url(); ?>' + url,
                        method:'POST',
                        data:{id:id},
                        success:function(data) {
                            if (data == "success") {
                                for ( var i = 0; i < id.length; i++ ) {
                                    $('#id-'+id[i]+'').fadeOut('slow');
                                }

                                swal(alert3, {
                                    icon: "success",
                                    buttons: false,
                                    timer: 2000,
                                });
                            } else {
                                swal(alert4, {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000,
                                });
                            }
                        }
                    });
                }
            }
        });
    });

    function view_image(that) {
        alert("berhasil");
        $(that).fancybox({
            helpers: {
                title : {
                    ype : 'float'
                }
            }
        });
    }

    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/home/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "-" + data[i].name + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });
</script>