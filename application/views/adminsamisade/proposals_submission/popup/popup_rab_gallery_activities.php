<center>
    <h2>
        Galeri Bukti Kegiatan
    </h2>
</center>
<hr>

<div class="row">
    <?php foreach( $list_gallery as $data ) { ?>
        <?php if ( empty($data['activities']) ) { ?>
            <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                    <div class="row">
                        <div class="col-md-2" style="padding: 5px; border: 1px solid black;">
                            <input type="checkbox" class="form-control" value="<?php echo $data['id']; ?>">
                        </div>
                        <div class="col-md-10" style="padding: 5px; border: 1px solid black;">
                            <a href="<?php echo $data['file']; ?>" data-fancybox>
                                <img src="<?php echo $data['file']; ?>" height="144px;" class="card-img-top" alt="...">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<div class="row">
    <div class="col-md-12">
        <button class="btn btn-success" style="width: 100%" onclick="parsing('<?php echo $rab_id; ?>', '<?php echo $rab_item_id; ?>', '<?php echo $step; ?>')">Simpan</button>
    </div>
</div>
<br>
<script>
    function parsing(rab_id, rab_item_id, step) {
        var rab_item_gallery_id = "";
        $(":checkbox:checked").each(function(i){
            rab_item_gallery_id += $(":checkbox:checked").eq(i).val() + ",";
        });

        if (rab_item_gallery_id == "") {
            swal("Silahkan pilih bukti nota/kegiatan terlebih dahulu.", {
                icon: "error",
                buttons: false,
                timer: 2000,
            });
        } else {
            $.ajax({
                url: '<?php echo base_url("adminsamisade/proposals_submission/set_image_realization_activities"); ?>',
                method:'POST',
                data:{rab_item_gallery_id:rab_item_gallery_id, rab_id:rab_id, rab_item_id:rab_item_id, step:step},
                success: function (data) {
                    if ( data == "success" ) {
                        // parent.callbackfancybox(rab_item_id, encodeURIComponent(rab_item_gallery_id), 'activities');
                        parent.$.fancybox.close();
                    }
                }
            });
        }
    }
</script>