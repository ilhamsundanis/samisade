<center>
    <h2>
        Galeri Bukti 
        <?php
            if ( $this->uri->segment(7) == "receipt" ) {
                echo "Nota";
            } else {
                echo "Kegiatan";
            }
        ?>    
    </h2>
</center>
<hr>

<div class="row">
    <?php foreach( $list_gallery as $data ) { ?>
        <div class="col-md-3">
            <div class="card" style="width: 18rem;">
                <a href="<?php echo $data['file']; ?>" data-fancybox>
                    <img src="<?php echo $data['file']; ?>" height="144px;" class="card-img-top" alt="...">
                </a>
            </div>
        </div>
    <?php } ?>
</div>