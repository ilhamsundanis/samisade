<center>
    <h2>
        Galeri Bukti Nota 
    </h2>
</center>
<hr>

<div class="row">
    <div class="col-md-12" style="text-align: right;">
        <h4 class="text-primary">Sisa Saldo: <span class="sisa-saldo">Rp. <?php echo number_format(check_saldo($rab_id, $step),0,',','.'); ?></span></h4>
    </div>
    <?php foreach( $list_gallery as $data ) { ?>
        <div class="col-md-3">
            <div class="card" style="width: 18rem; padding: 10px;">
                <a href="<?php echo $data['file']; ?>" data-fancybox>
                    <img src="<?php echo $data['file']; ?>" height="144px;" class="card-img-top" alt="...">
                </a>
                
                <?php if ( empty($data['receipt']) ) { ?>
                    <div class="total-pembayaran">
                        <input type="number" onkeyup="sum()" value="0" class="form-control" placeholder="Total Pembayaran" style="border: 1px solid black; margin: 10px 0;">
                    </div>
                    <div class="status-nota">
                        <span class="label label-danger" style="margin-bottom: 10px;">
                            <strong>Belum Diverifikasi</strong>
                        </span> 
                    </div>
                    <button class="btn btn-success" style="width: 100%" onclick="parsing(this, <?php echo $data['id']; ?>)">Kirim Data</button>
                <?php } else { ?>
                    <?php if ( $data['receipt']['status'] == "pending" || $data['receipt']['status'] == "reject" ) { ?>
                        <div class="total-pembayaran">
                            <input type="number" onkeyup="sum()" value="0" class="form-control" placeholder="Total Pembayaran" style="border: 1px solid black; margin: 10px 0;">
                        </div>
                        <div class="status-nota">
                            <span class="label label-<?php echo @parsing_status_class()[$data['receipt']['status']]; ?>" style="margin-bottom: 10px;">
                                <strong><?php echo @parsing_status()[$data['receipt']['status']]; ?></strong>
                            </span>
                        </div>
                        <button class="btn btn-success" style="width: 100%" onclick="parsing(this, <?php echo $data['id']; ?>)">Kirim Data</button>
                    <?php } else { ?>
                        <input type="hidden" value="<?php echo $data['receipt']['total_price']; ?>">
                        Total Pembayaran: <br>
                        <h4 class="text-primary">Rp. <?php echo number_format($data['receipt']['total_price'],0,',','.'); ?></h4>
                        <span class="label label-<?php echo @parsing_status_class()[$data['receipt']['status']]; ?>">
                            <strong><?php echo @parsing_status()[$data['receipt']['status']]; ?></strong>
                        </span>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
<br>
<script>
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    function sum() {
        var total = 0;
        var total_price = <?php echo $total_price; ?>;
        var saldo = <?php echo check_saldo($rab_id, $step); ?>;

        for (i = 0; i < $("input[type=number]").length; i++) {
            input = $("input[type=number]").eq(i).val();
            total += parseInt(input); 
        }

        var sisa_saldo = saldo - total;
        minus = "";
        if ( sisa_saldo < 0 ) {
            minus = "-";
            swal("Sisa saldo tidak boleh minus", {
                icon: "error",
                buttons: false,
                timer: 2000,
            });
        }

        for (i = 0; i < $("input[type=hidden]").length; i++) {
            input = $("input[type=hidden]").eq(i).val();
            total += parseInt(input); 
        }

        if ( total > total_price ) {
            swal("total Pembayaran telah melebihi Rp. <?php echo number_format($total_price,0,',','.'); ?>", {
                icon: "error",
                buttons: false,
                timer: 2000,
            });
        }

        $(".sisa-saldo").text(minus + "" + formatRupiah(sisa_saldo.toString(), "Rp. "));
    }

    function parsing(that, rab_item_gallery_id) {
        rab_item_id = <?php echo $rab_item_id; ?>;
        step        = <?php echo $step; ?>;
        total_price = $(that).parent().find("input[type=number]").val();

        if ( total_price == 0 ) {
            swal("Total Pembayaran tidak boleh 0", {
                icon: "error",
                buttons: false,
                timer: 2000,
            });
        } else {
            var total = 0;
            var limit_total_price = <?php echo $total_price; ?>;
            var saldo = <?php echo check_saldo($rab_id, $step); ?>;

            for (i = 0; i < $("input[type=number]").length; i++) {
                input = $("input[type=number]").eq(i).val();
                total += parseInt(input); 
            }

            var sisa_saldo = saldo - total;
            minus_saldo = false;
            if ( sisa_saldo < 0 ) {
                minus_saldo = true;
                swal("Sisa saldo tidak boleh minus", {
                    icon: "error",
                    buttons: false,
                    timer: 2000,
                });
            }

            for (i = 0; i < $("input[type=hidden]").length; i++) {
                input = $("input[type=hidden]").eq(i).val();
                total += parseInt(input); 
            }

            if (minus_saldo == false) {
                if ( total > limit_total_price ) {
                    swal("total Pembayaran telah melebihi Rp. <?php echo number_format($total_price,0,',','.'); ?>", {
                        icon: "error",
                        buttons: false,
                        timer: 2000,
                    });
                } else {
                    $.ajax({
                        url: '<?php echo base_url("adminsamisade/proposals_submission/set_image_realization_receipt"); ?>',
                        method:'POST',
                        data:{rab_item_gallery_id:rab_item_gallery_id, rab_item_id:rab_item_id, step:step, total_price:total_price},
                        success: function (data) {
                            if ( data == "success" ) {
                                $(that).parent().find(".status-nota").empty();
                                html = "<span class='label label-info' style='margin-bottom: 10px;'>" +
                                            "<strong>Sedang Diverifikasi</strong>" +
                                        "</span>";
                                $(that).parent().find(".status-nota").html(html);

                                $(that).parent().find(".total-pembayaran").empty();
                                html2 = "<input type='hidden' value='" + total_price + "'>" +
                                        "Total Pembayaran: <br>" +
                                        "<h4 class='text-primary'>" + formatRupiah(total_price.toString(), "Rp. ") + "</h4>";
                                $(that).parent().find(".total-pembayaran").html(html2);

                                $(that).remove()
                            }
                        }
                    });
                } 
            }
        }
    }
</script>