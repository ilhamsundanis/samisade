<style>
* {
    margin: 0;
    padding: 0
}


#heading {
    text-transform: uppercase;
    color: #673AB7;
    font-weight: normal
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #673AB7;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #673AB7;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #673AB7;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #673AB7;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    /* margin-bottom: 30px; */
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #00a687
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar #proposal_submission:before {
    font-family: FontAwesome;
    content: "\f1ea"
}

#progressbar #rab:before {
    font-family: FontAwesome;
    content: "\f022"
}

#progressbar #realisasi:before {
    font-family: FontAwesome;
    content: "\f093"
}

#progressbar #finish:before {
    font-family: FontAwesome;
    content: "\f05d"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #00a687
}

#progressbar li.open:before,
#progressbar li.open:after {
    background: #e8db08
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #00a687
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.open {
    color: #e8db08;
}

#map {
  height: 100%;
}

/* Optional: Makes the sample page fill the window. */
.controls {
  margin-top: 10px;
  border: 1px solid transparent;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 32px;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#origin-input,
#destination-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 200px;
}

#origin-input:focus,
#destination-input:focus {
  border-color: #4d90fe;
}

#mode-selector {
  color: #fff;
  background-color: #4d90fe;
  margin-left: 12px;
  padding: 5px 11px 0px 11px;
}

#mode-selector label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}
</style>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <form id="msform">
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active" id="proposal_submission">
                <a href="<?php echo base_url(); ?>adminsamisade/proposals_submission/view/<?php echo $this->uri->segment(4);?>">
                    <strong>PENGAJUAN PROPOSAL</strong>
                </a>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active']; ?> open" id="rab">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['link']; ?>">
                        <strong class="open">INPUT RAB</strong>
                    </a>
                <?php } else { ?>
                    <strong>INPUT RAB</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active']; ?>" id="realisasi">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['link']; ?>">
                        <strong>REALISASI</strong>
                    </a>
                <?php } else { ?>
                    <strong>REALISASI</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active']; ?>" id="finish">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['link']; ?>">
                        <strong>SELESAI</strong>
                    </a>
                <?php } else { ?>
                    <strong>SELESAI</strong>
                <?php } ?>
            </li>
        </ul>
    </form>
</div>

<?php
    echo alert()
?>

<div class="card">
    <div class="card-header">
        <strong>INFORMASI</strong>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <td>Total Anggaran RAB Maksimal Pengajuan</td>
                <td>:</td>
                <td>Rp. 1.000.000.000</td>
            </tr>
            <tr>
                <td>Anggaram RAB yang telah anda ajukan</td>
                <td>:</td>
                <td>Rp. <?php echo number_format(check_rab($this->uri->segment(4))['all_total_cost'],0,',','.'); ?></td>
            </tr>
            <tr>
                <td>Sisa anggaran yang dapat di ajukan</td>
                <td>:</td>
                <td>Rp. <?php echo number_format(check_rab($this->uri->segment(4))['rest_total_cost'],0,',','.'); ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <strong>INPUT RAB (RENCANA ANGGARAN BANGUNGAN)</strong>

        <button type="submit" name="save" class="btn btn-success" style="float:right;"> 
            <i class="fa fa-check"></i> SIMPAN
        </button>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <?php
                $no = 0;
                foreach ( $list_activity_by_desa as $data ) {
                    $no++;
            ?>
                    <li class="nav-item"> 
                        <a class="nav-link text-center" href="<?php echo base_url() . 'adminsamisade/proposals_submission/rab_activity/' . $this->uri->segment(4) . '/'  .$data['id'] . '/' . $data['activity_id']; ?>">
                            <?php echo "[No. "  .$no . "] " . strtoupper($data['name']); ?> <br>
                            <label class="label label-<?php echo parsing_status_class()[$data['status']]; ?>">
                                <?php echo parsing_status()[$data['status']]; ?>
                            </label>   
                        </a> 
                    </li>
            <?php
                }
            ?>
            <li class="nav-item"> 
                <a class="nav-link active" href="#tambah-rab">
                    TAMBAH RAB <br>
                    <label class="label label-danger">
                        TAMBAH RAB
                    </label>  
                </a> 
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tambah-rab" role="tabpanel">
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data">
                        <table class="table table-bordered table-sm">
                            <tr>
                                <td colspan="7" style="text-align: center; font-size: 16px;">
                                    <strong>RENCANA ANGGARAN BIAYA</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td>:</td>
                                <td>Jawa Barat</td>
                                <td width="40%"></td>
                                <td>Program</td>
                                <td>:</td>
                                <td>Samisade</td>
                            </tr>
                            <tr>
                                <td>Kabupaten</td>
                                <td>:</td>
                                <td>Bogor</td>
                                <td></td>
                                <td>Jenis Kegiatan</td>
                                <td>:</td>
                                <td>
                                    <select name="activity_id" id="activity_id" class="select2 form-control" onchange="submit()" required>
                                        <option value=""> --- Pilih --- </option>
                                        <?php
                                            foreach ($list_activity as $data) {
                                                if ( explode("-", @$post['activity_id'])[0] == $data['id'] ) {
                                                    $selected = "selected";
                                                } else {
                                                    $selected = "";
                                                }
                                        ?>
                                            <option value="<?php echo $data['id']."-".$data['type']; ?>" <?php echo $selected; ?>>
                                                <?php echo $data['name']; ?>
                                            </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>:</td>
                                <td><?php echo $detail['kec_name']; ?></td>
                                <td></td>
                                <td>Ukuran</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="dimension" class="form-control" value="<?php echo @$post['dimension']; ?>" required>
                                </td>
                            </tr>
                            <tr>
                                <td>Desa</td>
                                <td>:</td>
                                <td><?php echo $detail['desa_name']; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>

                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tabmaps-tab" data-toggle="tab" href="#tabmaps" role="tab" aria-controls="tabmaps" aria-selected="true">MAP</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tabfotomaps-tab" data-toggle="tab" href="#tabfotomaps" role="tab" aria-controls="tabfotomaps" aria-selected="false">FOTO LOKASI</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="tabmaps" role="tabpanel" aria-labelledby="tabmaps-tab">
                                        <br>
                                        <?php
                                            if ( isset( $post['activity_id'] ) ) {
                                                if ( @explode("-", @$post['activity_id'])[1] == "maps_direction" ) {
                                        ?>
                                                    <input type="hidden" id="point1_lat" name="point1_lat"/>
                                                    <input type="hidden" id="point1_lng" name="point1_lng"/>
                                                    <input type="hidden" id="point2_lat" name="point2_lat"/>
                                                    <input type="hidden" id="point2_lng" name="point2_lng"/>
                                                    <div style="display: none">
                                                        <input
                                                            id="origin-input"
                                                            class="controls"
                                                            type="text"
                                                            placeholder="Enter an origin location"
                                                        />

                                                        <input
                                                            id="destination-input"
                                                            class="controls"
                                                            type="text"
                                                            placeholder="Enter a destination location"
                                                        />

                                                        <div id="mode-selector" class="controls">
                                                            <input
                                                            type="radio"
                                                            name="type"
                                                            id="changemode-walking"
                                                            checked="checked"
                                                            />
                                                            <label for="changemode-walking">Walking</label>

                                                            <input type="radio" name="type" id="changemode-transit" />
                                                            <label for="changemode-transit">Transit</label>

                                                            <input type="radio" name="type" id="changemode-driving" />
                                                            <label for="changemode-driving">Driving</label>
                                                        </div>
                                                    </div>

                                                    <div id="map" style="height: 500px; width: 100%;"></div>
                                        <?php
                                                } else if (@explode("-", @$post['activity_id'])[1] == "maps") {
                                        ?>
                                                    <table class="table table-bordered table-sm">
                                                        <tr>
                                                            <td>Pengaturan tata letak kegiatan</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <input type="text" class="form-control" name="search_input" id="search_input" placeholder="Ketik Alamat..." style="border: 1px solid black;" value="<?php echo @$post['search_input']; ?>"/>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <input name="point1" id="latlong" type="hidden" value=""/>
                                                                        <div id="map" style="height: 500px; width: 100%;"></div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="tab-pane fade" id="tabfotomaps" role="tabpanel" aria-labelledby="tabfotomaps-tab">
                                        <br>
                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <td>Foto Lokasi Progres 0%</td>
                                                <td>:</td>
                                                <td><input type="file" name="image1" class="form-control" required></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table table-bordered table-sm">
                            <tr>
                                <td>
                                    Kategori: <br>
                                    <div class="row">
                                        <?php
                                            foreach ($list_category as $data) {
                                        ?>
                                            <div class="col-md-2" style="padding: 10px;">
                                                <input type="checkbox" onclick="add_category(this, '<?php echo $data['id']; ?>', '<?php echo $data['name']; ?>')"> <?php echo $data['name']; ?> 
                                            </div>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <table class="table table-bordered table-sm" id="table-input">
                            <tr>
                                <td width="1%">No</td>
                                <td>Uraian</td>
                                <td>Volume</td>
                                <td>Satuan</td>
                                <td>Harga Satuan</td>
                                <td>Total Harga</td>
                                <td>Input Pegawai ?</td>
                            </tr>

                            <tr>
                                <td colspan="5" style="text-align: right;">Grand Total</td>
                                <td style='text-align: right; padding-right: 18px;' class="grandtotal">Rp. 0</td>
                                <td style='text-align: right; padding-right: 18px;'></td>
                            </tr>
                            
                        </table>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-actions pull-right">
                                    <button type="submit" name="save" class="btn btn-success"> 
                                        <i class="fa fa-check"></i> SIMPAN
                                    </button>
                                    <input type="hidden" name="total_cost" value="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    if ( isset( $post['activity_id'] ) ) {
        if ( @explode("-", @$post['activity_id'])[1] == "maps_direction" ) {
?>
            <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly"
                async
            ></script>
            <script>
                function initMap() {
                    const map = new google.maps.Map(document.getElementById("map"), {
                        mapTypeControl: false,
                        center: { lat: -6.5950181, lng: 106.7218518 },
                        zoom: 13,
                    });
                    new AutocompleteDirectionsHandler(map);
                    }

                    class AutocompleteDirectionsHandler {
                    constructor(map) {
                        this.map = map;
                        this.originPlaceId = "";
                        this.destinationPlaceId = "";
                        this.travelMode = google.maps.TravelMode.WALKING;
                        this.directionsService = new google.maps.DirectionsService();
                        this.directionsRenderer = new google.maps.DirectionsRenderer({draggable: true});
                        this.directionsRenderer.setMap(map);
                        this.directionsRenderer.addListener("directions_changed", () => {
                            this.computeTotalDistance(this.directionsRenderer.getDirections());
                        })
                        const originInput = document.getElementById("origin-input");
                        const destinationInput = document.getElementById("destination-input");
                        const modeSelector = document.getElementById("mode-selector");
                        const originAutocomplete = new google.maps.places.Autocomplete(originInput);
                        // Specify just the place data fields that you need.
                        originAutocomplete.setFields(["place_id"]);
                        const destinationAutocomplete = new google.maps.places.Autocomplete(
                            destinationInput
                        );
                        // Specify just the place data fields that you need.
                        destinationAutocomplete.setFields(["place_id"]);
                        this.setupClickListener(
                            "changemode-walking",
                            google.maps.TravelMode.WALKING
                        );
                        this.setupClickListener(
                            "changemode-transit",
                            google.maps.TravelMode.TRANSIT
                        );
                        this.setupClickListener(
                            "changemode-driving",
                            google.maps.TravelMode.DRIVING
                        );
                        this.setupPlaceChangedListener(originAutocomplete, "ORIG");
                        this.setupPlaceChangedListener(destinationAutocomplete, "DEST");
                        this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
                        this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(
                            destinationInput
                        );
                        this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(modeSelector);
                    }
                    // Sets a listener on a radio button to change the filter type on Places
                    // Autocomplete.
                    setupClickListener(id, mode) {
                        const radioButton = document.getElementById(id);
                        radioButton.addEventListener("click", () => {
                            this.travelMode = mode;
                            this.route();
                        });
                    }
                    setupPlaceChangedListener(autocomplete, mode) {
                        autocomplete.bindTo("bounds", this.map);
                        autocomplete.addListener("place_changed", () => {
                            const place = autocomplete.getPlace();

                            if (!place.place_id) {
                                window.alert("Please select an option from the dropdown list.");
                                return;
                            }

                            if (mode === "ORIG") {
                                this.originPlaceId = place.place_id;
                            } else {
                                this.destinationPlaceId = place.place_id;
                            }
                            this.route();
                        });
                    }
                    route() {
                        if (!this.originPlaceId || !this.destinationPlaceId) {
                            return;
                        }
                        const me = this;
                        this.directionsService.route(
                            {
                                origin: { placeId: this.originPlaceId },
                                destination: { placeId: this.destinationPlaceId },
                                travelMode: this.travelMode,
                            },
                            (response, status) => {
                                if (status === "OK") {
                                    me.directionsRenderer.setDirections(response);
                                } else {
                                    window.alert("Directions request failed due to " + status);
                                }
                            }
                        );
                    }
                    computeTotalDistance(result) {
                        const myroute    = result.routes[0];

                        const point1_lat = myroute.legs[0].start_location.lat;
                        const point1_lng = myroute.legs[0].start_location.lng;

                        const point2_lat = myroute.legs[0].end_location.lat;
                        const point2_lng = myroute.legs[0].end_location.lng;

                        $("#point1_lat").val(point1_lat);
                        $("#point1_lng").val(point1_lng);

                        $("#point2_lat").val(point2_lat);
                        $("#point2_lng").val(point2_lng);
                    }
                }
            </script>
<?php
        } else if (@explode("-", @$post['activity_id'])[1] == "maps") {
?>
            <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly"
                async
            ></script>

            <script>
                function initMap() {
                    var autocomplete;
                    autocomplete = new google.maps.places.Autocomplete((document.getElementById('search_input')), {
                        types: ['geocode'],
                    });

                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        var near_place = autocomplete.getPlace();

                        document.getElementById('latlong').value = near_place.geometry.location.lat() +","+near_place.geometry.location.lng();

                        const map = new google.maps.Map(document.getElementById("map"), {
                            zoom: 15,
                            center: { lat: -6.5950181, lng: 106.7218518 },
                        });

                        var vMarker = new google.maps.Marker({
                            position: { lat: near_place.geometry.location.lat(), lng: near_place.geometry.location.lng() },
                            map,
                            draggable: true,
                        });
                        
                        google.maps.event.addListener(vMarker, 'dragend', function (evt) {
                            
                            $("#latlong").val(evt.latLng.lat().toFixed(6)+","+evt.latLng.lng().toFixed(6));
                            map.panTo(evt.latLng);
                        });

                        // centers the map on markers coords
                        map.setCenter(vMarker.position);

                        // adds the marker on the map
                        vMarker.setMap(map);
                    });
                }
            </script>
<?php
        }
    }
?>

<script type="text/javascript">
    $( document ).ready(function() {
        $("#search_input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

        $("#origin-input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

        $("#destination-input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    function add_category(that, id, name) {
        var html = "";

        html += "<tr class='category"+id+"'>" +
                    "<td colspan='7'>"+name+"</td>" +
                "</tr>" +
                "<tr class='category"+id+" category-subtotal"+id+"'>" +
                    "<td colspan='5' style='text-align: right;'>Sub Total</td>" +
                    "<td class='subtotal"+id+"' style='text-align: right; padding-right: 18px;'>Rp. 0</td>" +
                    "<td style='text-align: right; padding-right: 18px;'></td>" +
                "</tr>" +
                "<tr class='category"+id+"'>" +
                    "<td colspan='7'>" +
                        "<button type='button' class='btn btn-success' onclick='add(this, "+id+")' style='width: 100%;'>Tambah</button>" +
                    "</td>" +
                "</tr>";

        if($(that).is(":checked")){
            $("#table-input tr:last-child").before(html);

            var total = 0;
                for (i = 0; i < $(".category"+id+" .total_price").length; i++) {
                    input = $(".category"+id+" .total_price").eq(i).val()
                    total += parseFloat(input);
                }
                var grandtotal  = parseFloat($('.grandtotal').text().replace(/r|p|\.| /gi, "")) + parseFloat(total);
                $('.grandtotal').text(formatRupiah(String(grandtotal), 'Rp. '));
                $('input[name=total_cost]').val(grandtotal);
        }
        else if($(that).is(":not(:checked)")){
            var sub_total   = parseFloat($('.subtotal'+id).text().replace(/r|p|\.| /gi, ""));
            var grandtotal  = parseFloat($('.grandtotal').text().replace(/r|p|\.| /gi, "")) - parseFloat(sub_total);
            $('.grandtotal').text(formatRupiah(String(grandtotal), 'Rp. '));
            $('input[name=total_cost]').val(grandtotal);

            $(".category"+id+"").remove();
        }
    }

    function add( that, category_id ) {
        var html = "";

        html += "<tr class='category"+category_id+"'>" +
                    "<td><i class='fa fa-times' style='color: red;' onclick='remove(this, "+category_id+")'></i></td>" +
                    "<td><input type='text' name='name["+category_id+"][]' class='form-control'></td>" +
                    "<td><input type='text' onkeyup='sum(this); subsum(this, "+category_id+"); grandtotal(this);' name='volume["+category_id+"][]' class='form-control volume' value='0'></td>" +
                    "<td><input type='text' name='unit["+category_id+"][]' class='form-control'></td>" +
                    "<td><input type='text' onkeyup='sum(this); subsum(this, "+category_id+"); grandtotal(this);' name='unit_price["+category_id+"][]' class='form-control unit_price' value='0'></td>" +
                    "<td><input type='text' name='total_price["+category_id+"][]' class='form-control total_price total_price"+category_id+"' value='0' readonly style='text-align: right;'></td>" +
                    "<td>" +
                        "<select name='is_worker["+category_id+"][]' class='form-control'>" +
                            "<option value='false' selected>Tidak</option>" +
                            "<option value='true'>Ya</option>" +
                        "</select>" +
                    "</td>" +
                "</tr>";

        $(that).parent().parent().parent().find('.category-subtotal'+category_id).before(html);
    }

    function sum(that){
        var volume      = parseFloat($(that).parent().parent().find('.volume').val());
        var unit_price  = parseFloat($(that).parent().parent().find('.unit_price').val());
        var total_price = parseFloat(volume * unit_price);


        $(that).parent().parent().find('.total_price').val(total_price);
    }

    function subsum(that, category_id){
        var total = 0;
        for (i = 0; i < $(that).parent().parent().parent().find('.total_price'+category_id).length; i++) {
            input = $(that).parent().parent().parent().find('.total_price'+category_id).eq(i).val();
            total += parseFloat(input); 
        }

        $(that).parent().parent().parent().find('.subtotal'+category_id).text(formatRupiah(String(total), 'Rp. '));
    }

    function grandtotal(that){
        var total = 0;
        for (i = 0; i < $(that).parent().parent().parent().find('.total_price').length; i++) {
            input = $(that).parent().parent().parent().find('.total_price').eq(i).val();
            total += parseFloat(input); 
        }

        $(that).parent().parent().parent().find('.grandtotal').text(formatRupiah(String(total), 'Rp. '));
        $('input[name=total_cost]').val(total);
    }

    function remove(that, category_id) {
        // pengurangan subtotal
        var total_price = $(that).parent().parent().find('.total_price'+category_id).val();
        var sub_total   = $('.subtotal'+category_id).text();
        var total       = parseFloat(sub_total.replace(/r|p|\.| /gi, "")) - parseFloat(total_price);
        $('.subtotal'+category_id).text(formatRupiah(String(total), 'Rp. '));

        // pengurangan grandtotal
        var grandtotal  = parseFloat($('.grandtotal').text().replace(/r|p|\.| /gi, "")) - parseFloat(total_price);
        $('.grandtotal').text(formatRupiah(String(grandtotal), 'Rp. '));
        $('input[name=total_cost]').val(grandtotal);

        $(that).parent().parent().remove();
    }
</script>