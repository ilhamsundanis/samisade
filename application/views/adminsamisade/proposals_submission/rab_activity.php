<style>
* {
    margin: 0;
    padding: 0
}


#heading {
    text-transform: uppercase;
    color: #673AB7;
    font-weight: normal
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #673AB7;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #673AB7;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #673AB7;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #673AB7;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    /* margin-bottom: 30px; */
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #00a687
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar #proposal_submission:before {
    font-family: FontAwesome;
    content: "\f1ea"
}

#progressbar #rab:before {
    font-family: FontAwesome;
    content: "\f022"
}

#progressbar #realisasi:before {
    font-family: FontAwesome;
    content: "\f093"
}

#progressbar #finish:before {
    font-family: FontAwesome;
    content: "\f05d"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #00a687
}

#progressbar li.open:before,
#progressbar li.open:after {
    background: #e8db08
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #00a687
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.open {
    color: #e8db08;
}
</style>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <form id="msform">
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active" id="proposal_submission">
                <a href="<?php echo base_url(); ?>adminsamisade/proposals_submission/view/<?php echo $this->uri->segment(4);?>">
                    <strong>PENGAJUAN PROPOSAL</strong>
                </a>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active']; ?> open" id="rab">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['link']; ?>">
                        <strong class="open">INPUT RAB</strong>
                    </a>
                <?php } else { ?>
                    <strong>INPUT RAB</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active']; ?>" id="realisasi">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['link']; ?>">
                        <strong>REALISASI</strong>
                    </a>
                <?php } else { ?>
                    <strong>REALISASI</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active']; ?>" id="finish">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['link']; ?>">
                        <strong>SELESAI</strong>
                    </a>
                <?php } else { ?>
                    <strong>SELESAI</strong>
                <?php } ?>
            </li>
        </ul>
    </form>
</div>

<?php
    echo alert()
?>

<?php
    if ( $detail_all['rab']['status'] == "verification" ) {
?>
        <div class="alert alert-info alert-dismissable">
            <i data-icon="G" class="linea-icon linea-basic fa-fw"></i>
            PENGAJUAN RAB ANDA SEDANG DI VERIFIKASI, ANDA AKAN BISA MELNJUTKAN PROSES SELANJUTNYA SETELAH PROSES VERIFIKASI SELESAI
        </div>
<?php
    }
?>

<div class="card">
    <div class="card-header">
        <strong>INFORMASI</strong>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <td>Total Anggaran RAB Maksimal Pengajuan</td>
                <td>:</td>
                <td>Rp. 1.000.000.000</td>
            </tr>
            <tr>
                <td>Anggaram RAB yang telah anda ajukan</td>
                <td>:</td>
                <td>Rp. <?php echo number_format(check_rab($this->uri->segment(4))['all_total_cost'],0,',','.'); ?></td>
            </tr>
            <tr>
                <td>Sisa anggaran yang dapat di ajukan</td>
                <td>:</td>
                <td>Rp. <?php echo number_format(check_rab($this->uri->segment(4))['rest_total_cost'],0,',','.'); ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <strong>INPUT RAB (RENCANA ANGGARAN BANGUNGAN)</strong><br>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <?php
                $no = 0;
                foreach ( $list_activity_by_desa as $data ) {
                    $no++;
                    if ( $this->uri->segment(5) == $data['id'] ) {
                        $active = "active";
                    } else {
                        $active = "";
                    }
            ?>
                    <li class="nav-item"> 
                        <a class="nav-link <?php echo $active; ?> text-center" href="<?php echo base_url() . 'adminsamisade/proposals_submission/rab_activity/' . $this->uri->segment(4) . '/'  . $data['id'] . '/' . $data['activity_id']; ?>">
                            <?php echo "[No. "  .$no . "] " . strtoupper($data['name']); ?> <br>
                            <label class="label label-<?php echo parsing_status_class()[$data['status']]; ?>">
                                <?php echo parsing_status()[$data['status']]; ?>
                            </label>   
                        </a> 
                    </li>
            <?php
                }
            ?>
            <li class="nav-item"> 
                <a class="nav-link" href="<?php echo base_url() . 'adminsamisade/proposals_submission/rab/' . $this->uri->segment(4); ?>">
                    TAMBAH RAB <br>
                    <label class="label label-danger">
                        TAMBAH RAB
                    </label>  
                </a> 
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tambah-rab" role="tabpanel">
                <div class="card-body">
                    <?php
                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                    ?>
                        <a href="#" class="btn btn-danger deleteRAB" style="margin-bottom: 20px;"><i class="fa fa-times"></i> HAPUS RAB (RENCANA ANGGARAN BANGUNGAN)</a>
                    <?php
                        }
                    ?>
                    <form method="post" enctype="multipart/form-data">
                        <table class="table table-bordered table-sm">
                            <tr>
                                <td colspan="7" style="text-align: center; font-size: 16px;">
                                    <strong>RENCANA ANGGARAN BIAYA</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td>:</td>
                                <td>Jawa Barat</td>
                                <td width="40%"></td>
                                <td>Program</td>
                                <td>:</td>
                                <td>Samisade</td>
                            </tr>
                            <tr>
                                <td>Kabupaten</td>
                                <td>:</td>
                                <td>Bogor</td>
                                <td></td>
                                <td>Jenis Kegiatan</td>
                                <td>:</td>
                                <td>
                                    <?php echo $detail_all['rab']['activity_name']; ?>
                                    <input type="hidden" name="activity_id" value="<?php echo $detail_all['rab']['activity_id']; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>:</td>
                                <td><?php echo $detail_all['kec_name']; ?></td>
                                <td></td>
                                <td>Ukuran</td>
                                <td>:</td>
                                <td>
                                    <?php echo $detail_all['rab']['dimension']; ?>
                                    <input type="hidden" name="dimension" value="<?php echo $detail_all['rab']['dimension']; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Desa</td>
                                <td>:</td>
                                <td><?php echo $detail_all['desa_name']; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>

                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tabmaps-tab" data-toggle="tab" href="#tabmaps" role="tab" aria-controls="tabmaps" aria-selected="true">MAP</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tabfotomaps-tab" data-toggle="tab" href="#tabfotomaps" role="tab" aria-controls="tabfotomaps" aria-selected="false">FOTO LOKASI</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="tabmaps" role="tabpanel" aria-labelledby="tabmaps-tab">
                                        <br>
                                        <table class="table table-bordered table-sm">
                                            <tr>
                                                <td>Pengaturan tata letak kegiatan</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                        if ( $detail_all['rab']['activity_type'] == "maps_direction" ) {
                                                    ?>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div id="map" style="height: 500px; width: 100%;"></div>
                                                            </div>
                                                            <div class="col-md-4" style="padding: 15px;
                                                                width: 50%;
                                                                height: 500px;
                                                                overflow: scroll;
                                                                border: 1px solid #ccc;">
                                                                <div id="right-panel">
                                                                    <p>Total Jarak: <span id="total"></span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php
                                                        } elseif ( $detail_all['rab']['activity_type'] == "maps" ) {
                                                    ?>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div id="map" style="height: 500px; width: 100%;"></div>
                                                                </div>
                                                            </div>
                                                    <?php
                                                        }
                                                    ?>
                                                    
                                                </td>
                                            </tr>
                                        </table>

                                        <?php
                                            if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                        ?>
                                        <table class="table table-bordered table-sm">
                                            <tr>
                                                <td>
                                                    Kategori: <br>
                                                    <div class="row">
                                                        <?php
                                                            foreach ($list_category as $data) {
                                                                if (in_array($data['id'], $detail_all['category'])) {
                                                                    $checked = "checked";
                                                                } else {
                                                                    $checked = "";
                                                                }
                                                        ?>
                                                            <div class="col-md-2" style="padding: 10px;">
                                                                <input type="checkbox" onclick="add_category(this, '<?php echo $data['id']; ?>', '<?php echo $data['name']; ?>')" <?php echo $checked; ?>> <?php echo $data['name']; ?> 
                                                            </div>
                                                        <?php
                                                            }
                                                        ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                    <div class="tab-pane fade" id="tabfotomaps" role="tabpanel" aria-labelledby="tabfotomaps-tab">
                                        <br>

                                        <div class="card-group">
                                            <?php foreach ( $foto_location as $index => $val ) { ?>
                                            <div class="card">
                                                <div class="card-body">
                                                    <strong>Progres (<?php echo $val['progress']; ?>)</strong> <br>
                                                    <input type="file" name="image<?php echo ($index + 1); ?>" id="image<?php echo ($index + 1); ?>" class="form-control">
                                                    <hr>
                                                    <a href="<?php echo $val['foto']; ?>" id="image-a-<?php echo ($index + 1); ?>" data-fancybox>
                                                        <img src="<?php echo $val['foto']; ?>" id="image-img-<?php echo ($index + 1); ?>" width="100%">
                                                    </a>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <table class="table table-bordered table-sm" id="table-input">
                            <tr>
                                <td width="1%">No</td>
                                <td>Uraian</td>
                                <td>Volume</td>
                                <td>Satuan</td>
                                <td>Harga Satuan</td>
                                <td>Total Harga</td>
                                <td>Input Pegawai ?</td>
                            </tr>

                            <?php
                                $grandtotal = 0;
                                foreach ( $detail_all['rab_item'] as $value ) {
                            ?>
                                    <tr class='category<?php echo $value['category']; ?>'>
                                        <td colspan='7'><?php echo $value['category_name']; ?></td>
                                    </tr>

                                    <?php 
                                        $subtotal = 0;
                                        $no = 0;
                                        foreach ($value['item'] as $data) {
                                            $no++;
                                            $subtotal += $data['total_price'];
                                    ?>
                                            <tr class='category<?php echo $value['category']; ?>'>
                                                <td>
                                                    <?php
                                                        echo $no;
                                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                                    ?>
                                                    <i class='fa fa-times' style='color: red;' onclick='removeFromDB(this, <?php echo $data["id"]; ?>, <?php echo $value["category"]; ?>)'></i>
                                                    <?php
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                                    ?>
                                                        <input value="<?php echo $data['id']; ?>" type="hidden" name="idUpdate[<?php echo $value["category"]; ?>][]">
                                                        <input value="<?php echo $data['name']; ?>" type='text' name='nameUpdate[<?php echo $value["category"]; ?>][]' class='form-control'>
                                                    <?php 
                                                        } else { 
                                                            if ( $data['is_worker'] == true ) {
                                                                echo "<a data-fancybox data-type='iframe' data-src='".base_url()."adminsamisade/verification/rab/popup_employee/".$data['id']."' style='color: #00c292; cursor: pointer;' data-toggle='tooltip' title='Lihat daftar ". $data['name'] ."'>".$data['name']."</a>";
                                                            } else {
                                                                echo $data['name']; 
                                                            }
                                                        } 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                                    ?>
                                                    <input value="<?php echo $data['volume']; ?>" type='text' onkeyup='sum(this); subsum(this, <?php echo $value["category"]; ?>); grandtotal(this);' name='volumeUpdate[<?php echo $value["category"]; ?>][]' class='form-control volume' value='0'>
                                                    <?php 
                                                        } else { 
                                                            echo $data['volume'];
                                                        } 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                                    ?>
                                                    <input value="<?php echo $data['unit']; ?>" type='text' name='unitUpdate[<?php echo $value["category"]; ?>][]' class='form-control'>
                                                    <?php 
                                                        } else { 
                                                            echo $data['unit'];
                                                        } 
                                                    ?>
                                                </td>
                                                <td style='text-align: right;'>
                                                    <?php
                                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                                    ?>
                                                    <input value="<?php echo $data['unit_price']; ?>" type='text' onkeyup='sum(this); subsum(this, <?php echo $value["category"]; ?>); grandtotal(this);' name='unit_priceUpdate[<?php echo $value["category"]; ?>][]' class='form-control unit_price' value='0'>
                                                    <?php 
                                                        } else { 
                                                            echo "Rp. " . number_format($data['unit_price'],0,',','.');
                                                        } 
                                                    ?>
                                                </td>
                                                <td style='text-align: right;'>
                                                    <?php
                                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                                    ?>
                                                    <input value="<?php echo $data['total_price']; ?>" type='text' name='total_priceUpdate[<?php echo $value["category"]; ?>][]' class='form-control total_price total_price<?php echo $value["category"]; ?>' value='0' readonly style='text-align: right;'>
                                                    <?php 
                                                        } else { 
                                                            echo "Rp. " . number_format($data['total_price'],0,',','.');
                                                        } 
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                                    ?>
                                                    <?php 
                                                        $selectedTrue  = "";
                                                        $selectedFalse = "";
                                                        if ( $data['is_worker'] == true ) {
                                                            $selectedTrue = "selected";
                                                        } else if ( $data['is_worker'] == false ) {
                                                            $selectedFalse = "selected";
                                                        }
                                                    ?>
                                                    <select name='is_workerUpdate[<?php echo $value["category"]; ?>][]' class='form-control is_worker is_worker<?php echo $value["category"]; ?>'>
                                                        <option value="false" <?php echo $selectedFalse; ?>>Tidak</option>
                                                        <option value="true" <?php echo $selectedTrue; ?>>Ya</option>
                                                    </select>
                                                    <?php 
                                                        } else { 
                                                            echo ($data['is_worker'] == true) ? "Ya" : "Tidak";
                                                        } 
                                                    ?>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                        $grandtotal += $subtotal;
                                    ?>

                                    <tr class='category<?php echo $value["category"]; ?> category-subtotal<?php echo $value["category"]; ?>'>
                                        <td colspan='5' style='text-align: right;'>Sub Total</td>
                                        <td class='subtotal<?php echo $value["category"]; ?>' style='text-align: right;'>Rp. <?php echo number_format($subtotal,0,',','.'); ?></td>
                                        <td style='text-align: right;'></td>
                                    </tr>
                                    <?php
                                        if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                                    ?>
                                    <tr class='category<?php echo $value["category"]; ?>'>
                                        <td colspan='7'>
                                            <button type='button' class='btn btn-success' onclick='add(this, <?php echo $value["category"]; ?>)' style='width: 100%;'>Tambah</button>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                            <?php
                                }
                            ?>
                            
                            <tr>
                                <td colspan="5" style="text-align: right;">Grand Total</td>
                                <td style='text-align: right;' class="grandtotal">Rp. <?php echo number_format($grandtotal,0,',','.'); ?></td>
                                <td style='text-align: right;' ></td>
                            </tr>
                            <?php
                                if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                            ?>
                            <tr>
                                <td colspan="7" style="text-align: right;">
                                    <a data-fancybox data-type="iframe" data-src="<?php echo base_url(); ?>adminsamisade/employee/index/<?php echo $detail_all['rab']['id']; ?>" class="btn btn-info browse-receipt"  style="color: white;" data-toggle="tooltip" title="Tambah Pegawai">
                                        <i class="fa fa-plus"></i> Tambah Pegawai
                                    </a>
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </table>

                        <table>
                            <tr>
                                <td>Catatan:</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $detail_all['rab']['note']; ?>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <?php
                            if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
                        ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        Kirim Data Untuk Diverifikasi ? &nbsp; 
                                        <input type="radio" name="verification" value="no" checked> Tidak
                                        <input type="radio" name="verification" value="yes"> Ya &nbsp; 
                                    </div>
                                </div>
                        <?php
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-actions pull-right">
                                    <?php if ( $this->session->userdata('role') == 1) { ?>
                                    <button type="submit" name="reject" class="btn btn-danger"> 
                                        <i class="fa fa-times"></i> TOLAK PENGAJUAN
                                    </button>
                                    <?php } ?>
                                    <button type="submit" name="save" class="btn btn-success"> 
                                        <i class="fa fa-check"></i> SIMPAN
                                    </button>
                                    <input type="hidden" name="total_cost" value="<?php echo $grandtotal; ?>">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    if ( $detail_all['rab']['activity_type'] == "maps_direction" ) {
?>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly&language=id"
            async
        ></script>
        <script>
            function initMap() {
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 4,
                    center: { lat: -24.345, lng: 134.46 },
                });
                const directionsService = new google.maps.DirectionsService();
                const directionsRenderer = new google.maps.DirectionsRenderer({
                    map,
                    panel: document.getElementById("right-panel"),
                });
                directionsRenderer.addListener("directions_changed", () => {
                    computeTotalDistance(directionsRenderer.getDirections());
                });
                displayRoute(
                    "<?php echo $detail_all['rab_location']['point1']; ?>",
                    "<?php echo $detail_all['rab_location']['point2']; ?>",
                    directionsService,
                    directionsRenderer
                );
            }

            function displayRoute(origin, destination, service, display) {
                service.route(
                    {
                        origin: origin,
                        destination: destination,
                        travelMode: google.maps.TravelMode.WALKING,
                        avoidTolls: true,
                    },
                    (result, status) => {
                        if (status === "OK") {
                            display.setDirections(result);
                        } else {
                            alert("Could not display directions due to: " + status);
                        }
                    }
                );
            }

            function computeTotalDistance(result) {
                let total = 0;
                const myroute = result.routes[0];

                for (let i = 0; i < myroute.legs.length; i++) {
                    total += myroute.legs[i].distance.value;
                }
                total = total / 1000;
                document.getElementById("total").innerHTML = total + " km";
            }
        </script>
<?php
    } else if ($detail_all['rab']['activity_type'] == "maps") {
?>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly"
            async
        ></script>

        <script>
            function initMap() {
                const myLatLng = { lat: -25.363, lng: 131.044 };
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 15,
                    center: myLatLng,
                });
                var vMarker = new google.maps.Marker({
                    position: { lat: <?php echo explode(",", $detail_all['rab_location']['point1'])[0]; ?>, lng: <?php echo explode(",", $detail_all['rab_location']['point1'])[1]; ?> },
                    map,
                });

                // centers the map on markers coords
                map.setCenter(vMarker.position);

                // adds the marker on the map
                vMarker.setMap(map);
            }
        </script>
<?php
    }
?>

<script type="text/javascript">
    $(function(){
		$('[data-fancybox]').fancybox({
            toolbar  : false,
            smallBtn : true,
            iframe : {
                preload : true,
                css : {
                    height : '800px'
                }
            },
            fullScreen : {
                requestOnStart : true // Request fullscreen mode on opening
            },
        });
    });

    $( document ).ready(function() {
        $("#search_input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

        $("#origin-input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

        $("#destination-input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

        $('.deleteRAB').click(function(){
            swal({
                // title: alert1,
                text: "APAKAH ANDA AKAN MENGHAPUS RAB INI ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: '<?php echo base_url(); ?>adminsamisade/proposals_submission/delete_rab/' + <?php echo $detail_all['rab']['id']; ?>,
                        success:function(data) {
                            if (data == "success") {
                                window.location = "<?php echo base_url() . "adminsamisade/proposals_submission/rab/" . $this->uri->segment(4); ?>";
                            } else {
                                swal("GAGAL", {
                                    icon: "error",
                                    buttons: false,
                                    timer: 2000,
                                });
                            }
                        }
                    });
                }
            });
        });
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    function add_category(that, id, name) {
        var rab_id   = <?php echo $this->uri->segment(5); ?>;
        var html     = "";
        var subtotal = parseFloat(0);

        html += "<tr class='category"+id+"'>" +
                    "<td colspan='7'>"+name+"</td>" +
                "</tr>";

        html += "<tr class='category"+id+" category-subtotal"+id+"'>" +
            "<td colspan='5' style='text-align: right;'>Sub Total</td>" +
            "<td class='subtotal"+id+"' style='text-align: right; padding-right: 18px;'>"+formatRupiah(String(subtotal), 'Rp. ')+"</td>" +
            "<td style='text-align: right; padding-right: 18px;'></td>" +
        "</tr>";

        <?php
            if ( $detail_all['rab']['status'] == "pending" || $detail_all['rab']['status'] == "reject" ) {
        ?>
        html += "<tr class='category"+id+"'>" +
            "<td colspan='7'>" +
                "<button type='button' class='btn btn-success' onclick='add(this, "+id+")' style='width: 100%;'>Tambah</button>" +
            "</td>" +
        "</tr>";
        <?php
            }
        ?>

        if($(that).is(":checked")){
            $("#table-input tr:last-child").before(html);
        }
        else if($(that).is(":not(:checked)")){
            var sub_total   = parseFloat($('.subtotal'+id).text().replace(/r|p|\.| /gi, ""));
            var grandtotal  = parseFloat($('.grandtotal').text().replace(/r|p|\.| /gi, "")) - parseFloat(sub_total);
            $('.grandtotal').text(formatRupiah(String(grandtotal), 'Rp. '));
            $('input[name=total_cost]').val(grandtotal);

            $(".category"+id+"").remove();
        }

        $.ajax({
            dataType: "json",
            url: '<?php echo base_url("adminsamisade/proposals_submission/get_rab_item_by_category"); ?>/' + rab_id + '/' + id,
            success: function (data) {
                html     = "";
                subtotal = parseFloat(0);
                $(data).each(function(i) {
                    var selectedFalse = "";
                    var selectedTrue  = "";
                    if ( data[i].is_worker == true ) {
                        selectedTrue = "selected";
                    } else if ( data[i].is_worker == false ) {
                        selectedFalse = "selected";
                    }

                    subtotal += parseFloat(data[i].total_price);
                    html += "<tr class='category"+id+"'>" +
                        "<td><i class='fa fa-times' style='color: red;' onclick='removeFromDB(this, "+data[i].id+", "+id+")'></i></td>" +
                        "<td>" +
                            "<input value='"+data[i].id+"' type='hidden' name='idUpdate["+id+"][]' class='form-control'>" +
                            "<input value='"+data[i].name+"' type='text' name='nameUpdate["+id+"][]' class='form-control'>" +
                        "</td>" +
                        "<td><input value='"+data[i].volume+"' type='text' onkeyup='sum(this); subsum(this, "+id+"); grandtotal(this);' name='volumeUpdate["+id+"][]' class='form-control volume'></td>" +
                        "<td><input value='"+data[i].unit+"' type='text' name='unitUpdate["+id+"][]' class='form-control'></td>" +
                        "<td><input value='"+data[i].unit_price+"' type='text' onkeyup='sum(this); subsum(this, "+id+"); grandtotal(this);' name='unit_priceUpdate["+id+"][]' class='form-control unit_price'></td>" +
                        "<td><input value='"+data[i].total_price+"' type='text' name='total_priceUpdate["+id+"][]' class='form-control total_price total_price"+id+"' readonly style='text-align: right;'></td>" +
                        "<td>" +
                            "<select name='is_workerUpdate["+id+"][]' class='form-control is_worker is_worker"+id+"'>" +
                                "<option value='false' "+ selectedFalse +">Tidak</option>" +
                                "<option value='true' "+ selectedTrue +">Ya</option>" +
                            "</select>" +
                        "</td>" +
                    "</tr>";
                });
                $('.category-subtotal'+id).before(html);
                $('.subtotal'+id).text(formatRupiah(String(subtotal), 'Rp. '));

                var total = 0;
                for (i = 0; i < $(".category"+id+" .total_price").length; i++) {
                    input = $(".category"+id+" .total_price").eq(i).val()
                    total += parseFloat(input);
                }
                var grandtotal  = parseFloat($('.grandtotal').text().replace(/r|p|\.| /gi, "")) + parseFloat(total);
                $('.grandtotal').text(formatRupiah(String(grandtotal), 'Rp. '));
                $('input[name=total_cost]').val(grandtotal);
            }
        });
    }

    function add( that, category_id ) {
        var html = "";

        html += "<tr class='category"+category_id+"'>" +
                    "<td><i class='fa fa-times' style='color: red;' onclick='remove(this, "+category_id+")'></i></td>" +
                    "<td><input type='text' name='name["+category_id+"][]' class='form-control'></td>" +
                    "<td><input type='text' onkeyup='sum(this); subsum(this, "+category_id+"); grandtotal(this);' name='volume["+category_id+"][]' class='form-control volume' value='0'></td>" +
                    "<td><input type='text' name='unit["+category_id+"][]' class='form-control'></td>" +
                    "<td><input type='text' onkeyup='sum(this); subsum(this, "+category_id+"); grandtotal(this);' name='unit_price["+category_id+"][]' class='form-control unit_price' value='0'></td>" +
                    "<td><input type='text' name='total_price["+category_id+"][]' class='form-control total_price total_price"+category_id+"' value='0' readonly style='text-align: right;'></td>" +
                    "<td>" +
                        "<select name='is_worker["+category_id+"][]' class='form-control is_worker is_worker"+category_id+"'>" +
                            "<option value='false' selected>Tidak</option>" +
                            "<option value='true'>Ya</option>" +
                        "</select>" +
                    "</td>" +
                "</tr>";

        $(that).parent().parent().parent().find('.category-subtotal'+category_id).before(html);
    }

    function sum(that){
        var volume      = parseFloat($(that).parent().parent().find('.volume').val());
        var unit_price  = parseFloat($(that).parent().parent().find('.unit_price').val());
        var total_price = parseFloat(volume * unit_price);


        $(that).parent().parent().find('.total_price').val(total_price);
    }

    function subsum(that, category_id){
        var total = 0;
        for (i = 0; i < $(that).parent().parent().parent().find('.total_price'+category_id).length; i++) {
            input = $(that).parent().parent().parent().find('.total_price'+category_id).eq(i).val();
            total += parseFloat(input); 
        }

        $(that).parent().parent().parent().find('.subtotal'+category_id).text(formatRupiah(String(total), 'Rp. '));
    }

    function grandtotal(that){
        var total = 0;
        for (i = 0; i < $(that).parent().parent().parent().find('.total_price').length; i++) {
            input = $(that).parent().parent().parent().find('.total_price').eq(i).val();
            total += parseFloat(input); 
        }

        $(that).parent().parent().parent().find('.grandtotal').text(formatRupiah(String(total), 'Rp. '));
        $('input[name=total_cost]').val(total);
    }

    function remove(that, category_id) {
        // pengurangan subtotal
        var total_price = $(that).parent().parent().find('.total_price'+category_id).val();
        var sub_total   = $('.subtotal'+category_id).text();
        var total       = parseFloat(sub_total.replace(/r|p|\.| /gi, "")) - parseFloat(total_price);
        $('.subtotal'+category_id).text(formatRupiah(String(total), 'Rp. '));

        // pengurangan grandtotal
        var grandtotal  = parseFloat($('.grandtotal').text().replace(/r|p|\.| /gi, "")) - parseFloat(total_price);
        $('.grandtotal').text(formatRupiah(String(grandtotal), 'Rp. '));
        $('input[name=total_cost]').val(grandtotal);

        $(that).parent().parent().remove();

    }

    function removeFromDB(that, id, category_id) {
        // pengurangan subtotal
        var total_price = $(that).parent().parent().find('.total_price'+category_id).val();
        var sub_total   = $('.subtotal'+category_id).text();
        var total       = parseFloat(sub_total.replace(/r|p|\.| /gi, "")) - parseFloat(total_price);
        $('.subtotal'+category_id).text(formatRupiah(String(total), 'Rp. '));

        // pengurangan grandtotal
        var grandtotal  = parseFloat($('.grandtotal').text().replace(/r|p|\.| /gi, "")) - parseFloat(total_price);
        $('.grandtotal').text(formatRupiah(String(grandtotal), 'Rp. '));
        $('input[name=total_cost]').val(grandtotal);

        $.ajax({
            url: '<?php echo base_url("adminsamisade/proposals_submission/delete_rab_item"); ?>/' + id,
            success: function (data) {
                if ( data == "success" ) {
                    $(that).parent().parent().remove();
                }
            }
        });
    }

    function readURL(input, image_a, image_img) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + image_a).attr('href', e.target.result);
                $('#' + image_img).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    <?php foreach ( $foto_location as $index => $val ) { ?>
    $("#image<?php echo ($index + 1)?>").change(function(){
        readURL(this, "image-a-<?php echo ($index + 1)?>", "image-img-<?php echo ($index + 1)?>");
    });
    <?php } ?>
</script>