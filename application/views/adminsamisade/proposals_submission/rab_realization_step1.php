<style>
* {
    margin: 0;
    padding: 0
}


#heading {
    text-transform: uppercase;
    color: #673AB7;
    font-weight: normal
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #673AB7;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #673AB7;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #673AB7;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #673AB7;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    /* margin-bottom: 30px; */
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #00a687
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar #proposal_submission:before {
    font-family: FontAwesome;
    content: "\f1ea"
}

#progressbar #rab:before {
    font-family: FontAwesome;
    content: "\f022"
}

#progressbar #realisasi:before {
    font-family: FontAwesome;
    content: "\f093"
}

#progressbar #finish:before {
    font-family: FontAwesome;
    content: "\f05d"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #00a687
}

#progressbar li.open:before,
#progressbar li.open:after {
    background: #e8db08
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #00a687
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.open {
    color: #e8db08;
}
</style>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <form id="msform">
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active" id="proposal_submission">
                <a href="<?php echo base_url(); ?>adminsamisade/proposals_submission/view/<?php echo $this->uri->segment(4);?>">
                    <strong>PENGAJUAN PROPOSAL</strong>
                </a>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active']; ?>" id="rab">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['link']; ?>">
                        <strong>INPUT RAB</strong>
                    </a>
                <?php } else { ?>
                    <strong>INPUT RAB</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active']; ?> open" id="realisasi">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['link']; ?>">
                        <strong class="open">REALISASI</strong>
                    </a>
                <?php } else { ?>
                    <strong>REALISASI</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active']; ?>" id="finish">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['link']; ?>">
                        <strong>SELESAI</strong>
                    </a>
                <?php } else { ?>
                    <strong>SELESAI</strong>
                <?php } ?>
            </li>
        </ul>
    </form>
</div>

<?php
    echo alert()
?>

<div class="card">
    <div class="card-header">
        <strong>INPUT RAB (RENCANA ANGGARAN BANGUNGAN)</strong><br>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <?php
                $no = 0;
                foreach ( $list_activity_by_desa as $data ) {
                    $no++;
                    if ( $this->uri->segment(5) == $data['id'] ) {
                        $active = "active";
                    } else {
                        $active = "";
                    }
            ?>
                    <li class="nav-item"> 
                        <a class="nav-link <?php echo $active; ?>" href="<?php echo base_url() . 'adminsamisade/proposals_submission/rab_realization_step1/' . $this->uri->segment(4) . '/'  . $data['id'] . '/' . $data['activity_id']; ?>">
                            <?php echo "[No. "  .$no . "] " . strtoupper($data['name']); ?>
                        </a> 
                    </li>
            <?php
                }
            ?>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tambah-rab" role="tabpanel">
                <div class="card-body">

                    <div class="row">
                        <div class="col-2" style="border-right: 1px solid #dee2e6;">
                            <div class="nav flex-column nav-pills" aria-orientation="vertical">
                                <a class="nav-link active" href="javascript:void();">Realisasi Tahap 1 (40%)</a>
                                <?php if ( $detail_all['rab']['step1_percentage'] >= 75 ) { ?>
                                <a class="nav-link" href="<?php echo base_url() . 'adminsamisade/proposals_submission/rab_realization_step2/' . $this->uri->segment(4) . '/'  . $detail_all['rab']['id'] . '/' . $detail_all['rab']['activity_id']; ?>">Realisasi Tahap 2 (60%)</a>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                            if ( empty($detail_all['rab']['disbursement_date']['step1']) ) {
                        ?>
                                <div class="col-10">
                                    <div class="alert alert-warning">
                                        Mohon maaf anda belum bisa melakukan realisasi dikarenakan dana realisasi anda belum cair. Silahkan anda menunggu atau bisa menghubungi admin samisade Terima kasih.
                                    </div>
                                </div>
                        <?php
                            } else if ( date("Y-m-d") < $detail_all['rab']['disbursement_date']['step1'] ) {
                        ?>
                            <div class="col-10">
                                <div class="alert alert-warning">
                                    Mohon maaf anda baru bisa melakukan realisasi pada tanggal <strong><i><?php echo $detail_all['rab']['disbursement_date']['step1']; ?></i></strong>, Terima kasih.
                                </div>
                            </div>
                        <?php
                            } else {
                        ?>
                        <div class="col-10">
                            <div class="tab-content" id="v-pills-tabContent">
                                
                                <form method="post">
                                    <table class="table table-bordered table-sm">
                                        <tr>
                                            <td colspan="7" style="text-align: center; font-size: 16px;">
                                                <strong>REALISASI RENCANA ANGGARAN BIAYA TAHAP 1</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Provinsi</td>
                                            <td>:</td>
                                            <td>Jawa Barat</td>
                                            <td width="40%"></td>
                                            <td>Program</td>
                                            <td>:</td>
                                            <td>Samisade</td>
                                        </tr>
                                        <tr>
                                            <td>Kabupaten</td>
                                            <td>:</td>
                                            <td>Bogor</td>
                                            <td></td>
                                            <td>Jenis Kegiatan</td>
                                            <td>:</td>
                                            <td>
                                                <?php echo $detail_all['rab']['activity_name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kecamatan</td>
                                            <td>:</td>
                                            <td><?php echo $detail_all['kec_name']; ?></td>
                                            <td></td>
                                            <td>Ukuran</td>
                                            <td>:</td>
                                            <td>
                                                <?php echo $detail_all['rab']['dimension']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Desa</td>
                                            <td>:</td>
                                            <td><?php echo $detail_all['desa_name']; ?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered table-sm" id="table-input">
                                        <tr>
                                            <td width="1%">No</td>
                                            <td>Uraian</td>
                                            <td>Bukti Nota</td>
                                            <td>Bukti Kegiatan</td>
                                            <!-- <td>Total Pembayaran</td> -->
                                            <!-- <td>Status</td> -->
                                            <!-- <td>Aksi</td> -->
                                        </tr>

                                        <?php
                                            $grandtotal = 0;
                                            foreach ( $detail_all['rab_item'] as $value ) {
                                        ?>
                                                <tr class='category<?php echo $value['category']; ?>'>
                                                    <td colspan="7"><?php echo $value['category_name']; ?></td>
                                                </tr>

                                                <?php 
                                                    $subtotal = 0;
                                                    $no = 0;
                                                    foreach ($value['item'] as $data) {
                                                        $no++;
                                                        $subtotal += $data['total_price'];
                                                ?>
                                                        <tr class='category<?php echo $value['category']; ?>'>
                                                            <td style="vertical-align: middle;">
                                                                <?php
                                                                    echo $no;
                                                                ?>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                                <?php
                                                                    if ( $data['is_worker'] == true ) {
                                                                        echo "<a data-fancybox data-type='iframe' data-src='".base_url()."adminsamisade/verification/rab/popup_employee/".$data['id']."' style='color: #00c292; cursor: pointer;' data-toggle='tooltip' title='Lihat daftar ". $data['name'] ."'>".$data['name']."</a>";
                                                                    } else {
                                                                        echo $data['name']; 
                                                                    }
                                                                ?> (
                                                                <?php echo $data['volume']; ?> <?php echo $data['unit']; ?> * 
                                                                Rp. <?php echo number_format($data['unit_price'],0,',','.'); ?> = 
                                                                Rp. <?php echo number_format($data['total_price'],0,',','.'); ?>)</td>
                                                            <td style="text-align: center; vertical-align: middle;">
                                                                <a data-fancybox data-type="iframe" data-src="<?php echo base_url(); ?>adminsamisade/proposals_submission/popup_rab_gallery_receipt/<?php echo $detail_all['rab']['id']."/".$data['id']; ?>/1/<?php echo $data['total_price']; ?>" class="btn btn-info browse-receipt"  style="color: white;" data-toggle="tooltip" title="Lihat & Upload Bukti Nota">
                                                                    <i class="fa fa-folder-open"></i>
                                                                </a>
                                                            </td>

                                                            <td style="text-align: center; vertical-align: middle;">
                                                                <a data-fancybox data-type="iframe" data-src="<?php echo base_url(); ?>adminsamisade/proposals_submission/popup_rab_gallery_activities/<?php echo $detail_all['rab']['id']."/".$data['id']; ?>/1" class="btn btn-info browse-activities"  style="color: white;" data-toggle="tooltip" title="Browse">
                                                                    <i class="fa fa-folder-open"></i>
                                                                </a>
                                                                <a data-fancybox data-type="iframe" data-src="<?php echo base_url(); ?>adminsamisade/proposals_submission/popup_rab_gallery_realization_activities/<?php echo $detail_all['rab']['id']."/".$data['id']; ?>/1" class="btn btn-success" style="color: white;" data-toggle="tooltip" title="Lihat Bukti Kegiatan">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                <?php
                                                    }
                                                    $grandtotal += $subtotal;
                                                ?>
                                        <?php
                                            }
                                        ?>
                                        
                                        <tr>
                                            <td colspan="7" style="text-align: center; font-size: 20px;">
                                                TOTAL DANA CAIR TAHAP 1 (40%) <br>
                                                Rp. <?php echo number_format(($grandtotal*0.4),0,',','.'); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" style="text-align: center; font-size: 20px;">
                                                SISA SALDO TAHAP 1 <br>
                                                <span class="saldo">
                                                    Rp. <?php 
                                                            $grandtotal_pembayaran = str_replace(".", "", check_percentage($detail_all['rab']['id'], 1)['grandtotal_pembayaran']); 
                                                            echo number_format(($grandtotal*0.4) - ($grandtotal_pembayaran),0,',','.');
                                                        ?>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7">
                                                <div style="text-align: center; font-size: 20px; width: 100%;">
                                                    Total Penggunaan Dana Telah Mencapai: 
                                                    <span class="percentage"><?php echo check_percentage($detail_all['rab']['id'], 1)['percentage']; ?></span>% 
                                                    <span class="grandtotal_pembayaran">(Rp. <?php echo check_percentage($detail_all['rab']['id'], 1)['grandtotal_pembayaran']; ?>)</span>
                                                </div>
                                                <div>
                                                    <span class="label label-info">Informasi: </span> 
                                                    <i>Anda bisa melanjutkan pencairan dana tahap 2 setelah penggunaan dana tahap 1 telah mencapai 75% atau lebih</i>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="7">
                                                <div>
                                                    <span class="label label-info">Catatan:</span> 
                                                    <i style="color: red;"><?php echo $detail_all['rab']['step1_notes']; ?></i>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </form>

                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    // function send_data(that, rab_item_id, step) {
    //     total_pembayaran = $(that).parent().parent().find("input[type=number]").val();
    //     receipt    = $(that).parent().parent().find("input[type=hidden]").eq(0).val();
    //     activities = $(that).parent().parent().find("input[type=hidden]").eq(1).val();
    //     total      = 0;
    //     grandtotal = <?php //echo ($grandtotal*0.4); ?>;
    //     for (i = 0; i < $("input[type=number]").length; i++) {
    //         input = $("input[type=number]").eq(i).val();
    //         total += parseFloat(input); 
    //     }
        
    //     temp = "";
    //     tempBuktiNota = "";
    //     if ( receipt == "false" ) {
    //         tempBuktiNota = "Nota";
    //         temp          = tempBuktiNota;
    //     }

    //     tempBuktiKegiatan = "";
    //     if ( activities == "false" ) {
    //         tempBuktiKegiatan = "Kegiatan";
    //         temp              = tempBuktiKegiatan;
    //     }

    //     if ( tempBuktiNota != "" && tempBuktiKegiatan != "" ) {
    //         temp = tempBuktiNota + " dan " + tempBuktiKegiatan;
    //     }

    //     if ( receipt == "false" || activities == "false" ) {
    //         swal({
    //             text: "Silahkan lengkapi terlebih dahulu bukti " + temp,
    //             icon: "error",
    //             buttons: false,
    //             timer: 5000,
    //         });
    //     } else {
    //         if (total_pembayaran == 0) {
    //             swal("Total Pembayaran tidak boleh 0", {
    //                 icon: "error",
    //                 buttons: false,
    //                 timer: 2000,
    //             });
    //         } else {
    //             var saldo = grandtotal - total;
    //             if ( saldo < 0 ) {
    //                 swal("Sisa saldo tidak boleh minus", {
    //                     icon: "error",
    //                     buttons: false,
    //                     timer: 2000,
    //                 });
    //             } else {
    //                 swal({
    //                     text: "Apakah anda ingin mengirim data ini untuk di verifikasi?",
    //                     icon: "warning",
    //                     buttons: true,
    //                     dangerMode: true,
    //                 })
    //                 .then((willDelete) => {
    //                     if (willDelete) {
    //                         $.ajax({
    //                             url: '<?php //echo base_url(); ?>adminsamisade/proposals_submission/send_data_realization',
    //                             method:'POST',
    //                             data:{rab_item_id:rab_item_id, step:step, total_pembayaran:total_pembayaran},
    //                             success:function(data) {
    //                                 if (data == "success") {
    //                                     $(that).parent().parent().find(".status-realization").empty()
    //                                     html = "";
    //                                     html += "<span class='label label-info'>" +
    //                                                 "<strong>Sedang Diverifikasi</strong>" +
    //                                             "</span>";
    //                                     $(that).parent().parent().find(".status-realization").html(html);
    //                                     $(that).parent().parent().find(".browse-receipt").remove();
    //                                     $(that).parent().parent().find(".browse-activities").remove();
    //                                     $(that).parent().parent().find("input[type=number]").attr('disabled', true);
    //                                     $(that).remove();
    //                                 } else {
    //                                     swal("Coba beberapa saat lagi", {
    //                                         icon: "error",
    //                                         buttons: false,
    //                                         timer: 2000,
    //                                     });
    //                                 }
    //                             }
    //                         });
    //                     }
    //                 });
    //             }
    //         }
    //     }
    // }

    function sum() {
        var total = 0;
        var percentage = 0;
        var grandtotal = <?php echo ($grandtotal*0.4); ?>;
        for (i = 0; i < $("input[type=number]").length; i++) {
            input = $("input[type=number]").eq(i).val();
            total += parseFloat(input); 
        }

        percentage = ((total*100)/grandtotal).toFixed(2);
        if ( percentage > 100 ) {
            percentage = "100";
        }

        var saldo = grandtotal - total;
        if ( saldo < 0 ) {
            minus = "-";
            swal("Sisa saldo tidak boleh minus", {
                icon: "error",
                buttons: false,
                timer: 2000,
            });
        } else {
            minus = "";
        }
        $(".percentage").text(percentage);
        $(".grandtotal_pembayaran").text(formatRupiah(total.toString(), "Rp. "));
        $(".saldo").text(minus + formatRupiah(saldo.toString(), "Rp. "));
    }

    function callbackfancybox( rab_item_id, gallery_id, type ) {
        var html = ""
        bukti = "";
        if ( type == "receipt" ) {
            bukti = "Nota";
        } else {
            bukti = "Kegiatan";
        }

        html += "<a data-fancybox data-type='iframe' data-src='<?php echo base_url(); ?>adminsamisade/proposals_submission/popup_rab_gallery_realization/"+gallery_id+"' class='btn btn-success'  style='color: white;' data-toggle='tooltip' title='Lihat Bukti "+ bukti +"'>" +
                    "<i class='fa fa-eye'></i>" +
                "</a>" +
                "<input type='hidden' value='true'>";
        
        $(".viewimage-" + type + rab_item_id).empty();
        $(".viewimage-" + type + rab_item_id).html(html);
    }

    $(function(){
		$('[data-fancybox]').fancybox({
            toolbar  : false,
            smallBtn : true,
            iframe : {
                preload : true,
                css : {
                    height : '800px'
                }
            },
            fullScreen : {
                requestOnStart : true // Request fullscreen mode on opening
            },
        });
    });
</script>