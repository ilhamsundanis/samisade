<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

<style>
* {
    margin: 0;
    padding: 0
}


#heading {
    text-transform: uppercase;
    color: #673AB7;
    font-weight: normal
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #673AB7;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #673AB7;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #673AB7;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #673AB7;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    /* margin-bottom: 30px; */
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #00a687
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar #proposal_submission:before {
    font-family: FontAwesome;
    content: "\f1ea"
}

#progressbar #rab:before {
    font-family: FontAwesome;
    content: "\f022"
}

#progressbar #realisasi:before {
    font-family: FontAwesome;
    content: "\f093"
}

#progressbar #finish:before {
    font-family: FontAwesome;
    content: "\f05d"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #00a687
}

#progressbar li.open:before,
#progressbar li.open:after {
    background: #e8db08
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #00a687
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.open {
    color: #e8db08;
}
</style>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <form id="msform">
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active open" id="proposal_submission">
                <a href="<?php echo base_url(); ?>adminsamisade/proposals_submission/view/<?php echo $this->uri->segment(4);?>">
                    <strong class="open">PENGAJUAN PROPOSAL</strong>
                </a>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active']; ?>" id="rab">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['rab']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['rab']['link']; ?>">
                        <strong>INPUT RAB</strong>
                    </a>
                <?php } else { ?>
                    <strong>INPUT RAB</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active']; ?>" id="realisasi">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['realisasi']['link']; ?>">
                        <strong>REALISASI</strong>
                    </a>
                <?php } else { ?>
                    <strong>REALISASI</strong>
                <?php } ?>
            </li>
            <li class="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active']; ?>" id="finish">
                <?php if ( step_menu_proposals_submission( $this->uri->segment(4) )['finish']['active'] == "active" ) { ?>
                    <a href="<?php echo step_menu_proposals_submission( $this->uri->segment(4) )['finish']['link']; ?>">
                        <strong>SELESAI</strong>
                    </a>
                <?php } else { ?>
                    <strong>SELESAI</strong>
                <?php } ?>
            </li>
        </ul>
    </form>
</div>

<?php
    echo alert()
?>

<?php if ( $proposals_submission['status'] == "verification" ) { ?>
<div class="alert alert-info alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i data-icon="G" class="linea-icon linea-basic fa-fw"></i>
   PROPOSAL PENGAJUAN ANDA SEDANG DI VERIFIKASI, ANDA AKAN BISA MELNJUTKAN PROSES SELANJUTNYA SETELAH PROSES VERIFIKASI SELESAI
</div>
<?php } ?>

<form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <strong>PENGAJUAN PROPOSAL</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if ( $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2 ) { ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        KECAMATAN <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" value="<?php echo $proposals_submission['kec_name']; ?>" disabled>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        DESA <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" value="<?php echo $proposals_submission['desa_name']; ?>" disabled>
                                </div>
                            </div>
                            <input type="hidden" name="desa_id" value="<?php echo $proposals_submission['desa_id']; ?>">
                        <?php } else { ?>
                            <input type="hidden" name="desa_id" value="<?php echo $this->session->userdata('desa_id'); ?>">
                        <?php } ?>

                        <div class="col-md-12 form-radius">
                            <div class="form-group">
                                <label class="control-label">
                                    JUDUL <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control" name="title" value="<?php echo $proposals_submission['title']; ?>" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">
                                    DOKUMEN <span class="text-danger">*</span> <i>Max File 6Mb (.pdf)</i>
                                </label>
                                <input type="file" class="form-control" name="file">

                                <a href="<?php echo $proposals_submission['file']; ?>" target="_blank">Unduh Dokumen</a>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">
                                    Status: 
                                </label><br>
                                <label class="label label-<?php echo parsing_status_class()[$proposals_submission['status']]; ?>">
                                    <?php echo parsing_status()[$proposals_submission['status']]; ?>
                                </label>   
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">
                                    Catatan: 
                                </label><br>
                                <?php echo $proposals_submission['note']; ?>
                            </div>
                        </div>
                    </div>

                    <?php //if ( $proposals_submission['status'] == "pending" || $proposals_submission['status'] == "reject" ) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php //} ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <embed type="application/pdf" src="<?php echo $proposals_submission['file']; ?>" width="100%" height="96%"></embed>
        </div>
    </div>
</form>