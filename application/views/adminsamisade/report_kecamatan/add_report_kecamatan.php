<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/report_kecamatan">
                DAFTAR LAPORAN KECAMATAN
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                TAMBAH LAPORAN KECAMATAN
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DESA <span class="text-danger">*</span>
                                        </label>
                                        <select name="desa_id" id="desa_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_desa as $data) {
                                                    if (@$post['desa_id'] == $data['id']) {
                                                        $selected1 = "selected";
                                                    } else {
                                                        $selected1 = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                    <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            JUDUL <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="title" value="" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DESKRIPSI <span class="text-danger">*</span>
                                        </label>
                                        <textarea name="description" class="form-control" id="content" cols="30" rows="10"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DOKUMENTASI 1 <span class="text-danger">*</span>
                                        </label>
                                        <input type="file" name="image1" id="image1" class="form-control" value="">
                                        <img src="#" id="image1-banner" height="300px;" style="margin-top: 10px;">
                                    </div>
                                    
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DOKUMENTASI 2 <span class="text-danger">*</span>
                                        </label>
                                        <input type="file" name="image2" id="image2" class="form-control" value="">
                                        <img src="#" id="image2-banner" height="300px;" style="margin-top: 10px;">
                                    </div>
                                    
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DOKUMENTASI 3 <span class="text-danger">*</span>
                                        </label>
                                        <input type="file" name="image3" id="image3" class="form-control" value="">
                                        <img src="#" id="image3-banner" height="300px;" style="margin-top: 10px;">
                                    </div>
                                    
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DOKUMENTASI 4 <span class="text-danger">*</span>
                                        </label>
                                        <input type="file" name="image4" id="image4" class="form-control" value="">
                                        <img src="#" id="image4-banner" height="300px;" style="margin-top: 10px;">
                                    </div>
                                    
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DOKUMENTASI 5 <span class="text-danger">*</span>
                                        </label>
                                        <input type="file" name="image5" id="image5" class="form-control" value="">
                                        <img src="#" id="image5-banner" height="300px;" style="margin-top: 10px;">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var editor = CKEDITOR.replace( 'content');
    CKFinder.setupCKEditor( editor );

    function readURL(input, number) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image'+number+'-banner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image1").change(function(){
        readURL(this, 1);
    });

    $("#image2").change(function(){
        readURL(this, 2);
    });

    $("#image3").change(function(){
        readURL(this, 3);
    });

    $("#image4").change(function(){
        readURL(this, 4);
    });

    $("#image5").change(function(){
        readURL(this, 5);
    });

</script>