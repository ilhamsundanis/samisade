<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/report_person_in_charge">
                DAFTAR LAPORAN PENANGGUNG JAWAB
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                TAMBAH LAPORAN PENANGGUNG JAWAB
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            PROPOSAL <span class="text-danger">*</span>
                                        </label>
                                        <select name="proposal_id" id="proposal_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_proposal as $data) {
                                                    if (@$post['proposal_id'] == $data['id']) {
                                                        $selected1 = "selected";
                                                    } else {
                                                        $selected1 = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                    <?php echo $data['title']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            JUDUL <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="title" value="" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DOKUMEN <span class="text-danger">*</span>
                                        </label>
                                        <input type="file" class="form-control" name="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function readURL(input, number) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image'+number+'-banner').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image1").change(function(){
        readURL(this, 1);
    });

    var editor = CKEDITOR.replace( 'surat_permohonan');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'kata_pengantar');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'daftar_isi');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'pendahuluan');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'uraian_kegiatan');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'penutup');
    CKFinder.setupCKEditor( editor );

</script>