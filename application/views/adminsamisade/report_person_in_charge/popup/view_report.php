<br>
<div class="row">
    <div class="col-md-12">
        <center><h2>LAPORAN KECAMATAN <?php echo $report_kecamatan['kec_name']; ?></h2></center>
    </div>

    <table class="table table-bordered table-striped">
        <tr>
            <td>Judul Laporan</td>
            <td>:</td>
            <td><?php echo $report_kecamatan['title']; ?></td>
        </tr>
        <tr>
            <td>Tanggal Laporan</td>
            <td>:</td>
            <td><?php echo $report_kecamatan['created_at']; ?></td>
        </tr>
        <tr>
            <td>Pembuat Laporan</td>
            <td>:</td>
            <td><?php echo $report_kecamatan['name_user']; ?></td>
        </tr>
        <tr>
            <td>Desa</td>
            <td>:</td>
            <td><?php echo $report_kecamatan['desa_name']; ?></td>
        </tr>
        <tr>
            <td>Isi Laporan</td>
            <td>:</td>
            <td><?php echo $report_kecamatan['description']; ?></td>
        </tr>
        <tr>
            <td>Dokumentasi</td>
            <td>:</td>
            <td>
                <a href="<?php echo $report_kecamatan['image1']; ?>" data-fancybox >
                    <img src="<?php echo $report_kecamatan['image1']; ?>" height="100px;"/>
                </a> &nbsp; | &nbsp; 
                <a href="<?php echo $report_kecamatan['image2']; ?>" data-fancybox >
                    <img src="<?php echo $report_kecamatan['image2']; ?>" height="100px;"/>
                </a> &nbsp; | &nbsp; 
                <a href="<?php echo $report_kecamatan['image3']; ?>" data-fancybox >
                    <img src="<?php echo $report_kecamatan['image3']; ?>" height="100px;"/>
                </a> &nbsp; | &nbsp; 
                <a href="<?php echo $report_kecamatan['image4']; ?>" data-fancybox >
                    <img src="<?php echo $report_kecamatan['image4']; ?>" height="100px;"/>
                </a> &nbsp; | &nbsp; 
                <a href="<?php echo $report_kecamatan['image5']; ?>" data-fancybox >
                    <img src="<?php echo $report_kecamatan['image5']; ?>" height="100px;"/>
                </a>
            </td>
        </tr>
    </table>
</div>

<script>
    function view_image(that) {
        $(that).fancybox({
            helpers: {
                title : {
                    ype : 'float'
                }
            }
        });
    }
</script>