            <!-- </div>
        </div>
    </div> -->
    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/custom.min.js"></script>
    <!--Custom select -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/select2.min.js"></script>
    <!--Data Tables -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/jquery.dataTables.min.js"></script>
    <!--Fancy Box -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/fancybox/jquery.fancybox.min.js"></script>
    <!--Fancy Box -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/toast/jquery.toast.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- <script src="<?php //echo base_url() ?>js/adminsamisade/rentist.js"></script> -->
    <script src="<?php echo base_url() ?>js/adminsamisade/global.js"></script>
    <script src="<?php echo base_url() ?>js/adminsamisade/phone/intlTelInput.js"></script>
    <!-- Add js on controller -->
    <?php
        if( ! empty( $js ) ) { 
            foreach ($js as $linkjs) echo '<script src="' . base_url() . $linkjs . '.js" /></script>', "\n"; 
        }
    ?>
    
    <script src="<?php echo base_url() ?>js/adminsamisade/tool.js"></script>

    <script src="<?php echo base_url() ?>js/adminsamisade/datepicker/moment.js"></script>
</body>

</html>