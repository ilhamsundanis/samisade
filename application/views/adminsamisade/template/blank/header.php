
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>images/favicon.png">

    <title>Admin</title>
    
    <!-- page css -->
    <link href="<?php echo base_url(); ?>css/adminsamisade/blank/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>css/adminsamisade/blank/style.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>css/adminsamisade/fancybox/jquery.fancybox.min.css" rel="stylesheet" />
    <!-- <link href="<?php //echo base_url(); ?>css/adminsamisade/toast/jquery.toast.css" rel="stylesheet" /> -->

    <?php
        if( ! empty( $css ) ) { 
            foreach ($css as $style) echo '<link rel="stylesheet" type="text/css" href="' . base_url() . $style . '.css" />', "\n"; 
        }
    ?>

    <script src="<?php echo base_url(); ?>js/adminsamisade/home/jquery-3.2.1.min.js"></script>
</head>

<body style="padding: 20px;">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <!-- <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Pilkades Serentak 2020</p>
        </div>
    </div> -->
    <!-- <div id="main-wrapper">
        <div class="page-wrapper">
            <div class="container-fluid"> -->