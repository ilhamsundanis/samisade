        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>adminsamisade/home">
                        <b>
                            S
                        </b>
                        <span style="align:center">
                            | SAMISADE APPS
                        </span>
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> 
                            <a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark" href="javascript:void(0)">
                                <i class="ti-menu"></i>
                            </a>
                        </li>
                        <li class="nav-item"> 
                            <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)">
                                <i class="icon-menu"></i>
                            </a> 
                        </li>
                    </ul>

                    <ul class="navbar-nav my-lg-0">

                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="hidden-md-down" style="color:black;">
                                    <?php echo $this->session->userdata('name'); ?> 
                                    <strong> > </strong> 
                                    <?php 
                                        if ( $this->session->userdata('role') == 1 ) {
                                            echo "<i style='font-weight:bold'>Administrator</i>";
                                        } else if ( $this->session->userdata('role') == 2 ) {
                                            echo "<i style='font-weight:bold'>" . $this->session->userdata('name_kecamatan') . "</i>";
                                        } else if ( $this->session->userdata('role') == 3) {
                                            echo "<i style='font-weight:bold'>" . $this->session->userdata('name_desa') . "</i>";
                                        }
                                    ?>
                                    <i class="icon-options-vertical"></i>
                                </span>
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <!-- <div class="dropdown-divider"></div> -->
                                <a href="<?php echo base_url(); ?>adminsamisade/auth/change_password" class="dropdown-item"><i class="fa fa-asterisk"></i>
                                    UBAH KATA SANDI
                                </a>
                                <a href="<?php echo base_url(); ?>adminsamisade/auth/logout" class="dropdown-item"><i class="fa fa-power-off"></i>
                                    KELUAR
                                </a>
                            </div>
                        </li>
                    </ul>

                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li>
                            <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/home" aria-expanded="false">
                                <i class="fa fa-home"></i>
                                <span class="hide-menu">HOME</span>
                            </a>
                        </li>

                        <?php
                            if ( $this->session->userdata('role') == 1 || $this->session->userdata('role') == 2 ) {
                        ?>

                                <li>
                                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                        <i class="fa fa-bar-chart"></i>
                                        <span class="hide-menu">GRAFIK</span>
                                    </a>
                                    <ul aria-expanded="false" class="collapse" style="margin-top: 5px;">
                    
                                        <li>
                                            <a href="<?php echo base_url(); ?>adminsamisade/chart/index/total-anggaran">
                                                TOTAL ANGGARAN
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>adminsamisade/chart/index/jumlah-dana">
                                                JUMLAH DANA TEREALISASI
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>adminsamisade/chart/index/persentase-pembangunan">
                                                PERSENTASE PEMBANGUNAN
                                            </a>
                                        </li>

                                        <?php if ( $this->session->userdata('role') == 1 ) { ?>
                                            <li>
                                                <a href="<?php echo base_url(); ?>adminsamisade/chart/access">
                                                    AKSES PENGGUNA
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/proposals_submission" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">PENGAJUAN PROPOSAL</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                        <i class="ti-archive"></i>
                                        <span class="hide-menu">VERIFIKASI (<?php echo count_verification()['total']; ?>)</span>
                                    </a>
                                    <ul aria-expanded="false" class="collapse" style="margin-top: 5px;">
                    
                                        <li>
                                            <a href="<?php echo base_url(); ?>adminsamisade/verification/proposal">
                                                PROPOSAL (<?php echo count_verification()['proposal']; ?>)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>adminsamisade/verification/rab">
                                                RAB (<?php echo count_verification()['rab']; ?>)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>adminsamisade/verification/realization">
                                                REALISASI (<?php echo count_verification()['rab_item_realization']; ?>)
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/category" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">KATEGORI</span>
                                    </a>
                                </li>

                                <?php if ( $this->session->userdata('role') == 1 ) { ?>

                                    <li>
                                        <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/disbursement_date" aria-expanded="false">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="hide-menu">TANGGAL PENCAIRAN DANA</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/kecamatan" aria-expanded="false">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="hide-menu">KECAMATAN</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/desa" aria-expanded="false">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="hide-menu">DESA</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/news" aria-expanded="false">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="hide-menu">BERITA</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/banner" aria-expanded="false">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="hide-menu">BANNER</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/company" aria-expanded="false">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="hide-menu">PENGATURAN INTANSI</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/complaint" aria-expanded="false">
                                            <i class="fa fa-plus-square-o"></i>
                                            <span class="hide-menu">ADUAN MASYARAKAT (<?php echo count_complaint()?>)</span>
                                        </a>
                                    </li>

                                <?php } ?>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/report_kecamatan" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">LAPORAN KECAMATAN</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/report_person_in_charge" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">LAPORAN PENANGGUNG JAWAB</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/user" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">PENGGUNA</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/auth/change_password" aria-expanded="false">
                                        <i class="fa fa-asterisk"></i>
                                        <span class="hide-menu">UBAH KATA SANDI</span>
                                    </a>
                                </li>
                        <?php
                            } else {
                        ?>
                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/category" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">KATEGORI</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/proposals_submission" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">PENGAJUAN PROPOSAL</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/report_person_in_charge" aria-expanded="false">
                                        <i class="fa fa-plus-square-o"></i>
                                        <span class="hide-menu">LAPORAN PENANGGUNG JAWAB</span>
                                    </a>
                                </li>

                                <li>
                                    <a class="waves-effect waves-dark" href="<?php echo base_url(); ?>adminsamisade/auth/change_password" aria-expanded="false">
                                        <i class="fa fa-asterisk"></i>
                                        <span class="hide-menu">UBAH KATA SANDI</span>
                                    </a>
                                </li>
                        <?php
                            }
                        ?>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

        <div class="page-wrapper">
            <div class="container-fluid">
                