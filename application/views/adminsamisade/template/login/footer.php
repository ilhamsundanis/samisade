    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/home/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>js/adminsamisade/login/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>js/adminsamisade/bootstrap.min.js"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- <script src="<?php //echo base_url(); ?>js/adminsamisade/rentist.js"></script> -->
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ============================================================== 
        // Login and Recover Password 
        // ============================================================== 
        $('#to-recover').on("click", function() {
            $("#rent-form").slideUp();
            $("#msform").fadeIn();
        });

        $('#close').on("click", function() {
            $("#msform").fadeOut();
            $("#rent-form").slideDown();
        });
    </script>
    
</body>

</html>