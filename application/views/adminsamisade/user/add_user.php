<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/user">
                DAFTAR PENGGUNA
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                TAMBAH PENGGUNA
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            NAMA <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="name" value="<?php echo @$post['name']; ?>" required>
                                    </div>
                                </div>

                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            EMAIL <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" class="form-control" name="email" value="<?php echo @$post['email']; ?>" required>
                                    </div>
                                </div>

                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            KATA SANDI <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control" name="password" value="<?php echo @$post['password']; ?>" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            LEVEL <span class="text-danger">*</span>
                                        </label>
                                        <select name="role" id="role" class="select2 form-control" required onchange="submit()">
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ( user_role() as $index => $val) {
                                                    if ( @$post['role'] == $index ) {
                                                        $selected = "selected";
                                                    } else {
                                                        $selected = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $index; ?>" <?php echo $selected; ?>>
                                                    <?php echo $val; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <?php if ( @$post['role'] == 2 ) {?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            KECAMATAN <span class="text-danger">*</span>
                                        </label>
                                        <select name="kec_id" id="kec_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_kecamatan as $data) {
                                                    if ($post['kec_id'] == $data['id']) {
                                                        $selected1 = "selected";
                                                    } else {
                                                        $selected1 = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                    <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if ( @$post['role'] == 3 ) {?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DESA <span class="text-danger">*</span>
                                        </label>
                                        <select name="desa_id" id="desa_id" class="select2 form-control" required>
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_desa as $data) {
                                                    if ($post['desa_id'] == $data['id']) {
                                                        $selected1 = "selected";
                                                    } else {
                                                        $selected1 = "";
                                                    }
                                            ?>
                                                <option value="<?php echo $data['id']; ?>" <?php echo $selected1; ?>>
                                                    [Kec: <?php echo $data['name_kecamatan']; ?>] - Desa: <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>