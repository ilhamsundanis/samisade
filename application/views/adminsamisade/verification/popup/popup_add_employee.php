<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link" href="<?php echo base_url(); ?>adminsamisade/verification/rab/popup_employee/<?php echo $this->uri->segment(5); ?>">
                DAFTAR PEGAWAI
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link active" href="javascript:void(0);">
                TAMBAH PEGAWAI
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <?php
                    echo alert()
                ?>
                <form method="post" role="form" id="rent-form" autocomplete="off">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Tipe <span class="text-danger">*</span>
                                    </label>
                                    <select name="rab_item_id" id="rab_item_id" class="form-control" required>
                                        <option value=""> --- Pilih --- </option>
                                        <?php
                                            foreach ($list_rab_item as $data) {
                                        ?>
                                            <option value="<?php echo $data['id']; ?>">
                                                <?php echo $data['name']; ?>
                                            </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        NIK <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="nik" value="" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Nama <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" class="form-control" name="name" value="" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Jenis Kelamin <span class="text-danger">*</span>
                                    </label>
                                    <select name="gender" class="form-control">
                                        <option value="l">Laki - Laki</option>
                                        <option value="p">Perempuan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Tanggal Lahir <span class="text-danger">*</span>
                                    </label>
                                    <input type="date" class="form-control" name="birthdate" value="" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Alamat <span class="text-danger">*</span>
                                    </label>
                                    <textarea name="address" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> SIMPAN
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>