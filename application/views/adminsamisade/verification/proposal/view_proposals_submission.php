<script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<?php
    echo alert()
?>

<form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <strong>PENGAJUAN PROPOSAL</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">

                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            KECAMATAN
                                        </label>
                                        <input type="text" class="form-control" value="<?php echo $proposals_submission['kec_name']; ?>" disabled>
                                    </div>
                                </div>

                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            DESA
                                        </label>
                                        <input type="text" class="form-control" value="<?php echo $proposals_submission['desa_name']; ?>" disabled>
                                    </div>
                                </div>

                                <div class="col-md-12 form-radius">
                                    <div class="form-group">
                                        <label class="control-label">
                                            JUDUL
                                        </label>
                                        <input type="text" class="form-control" value="<?php echo $proposals_submission['title']; ?>" disabled>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Dokumen<span class="text-danger">*</span>
                                        </label><br>
                                        <a href="<?php echo !empty($proposals_submission['file']) ? $proposals_submission['file'] : "javascript:void(0);"; ?>" target="_blank">Unduh Dokumen Proposal</a>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Verifiaksi Data<span class="text-danger">*</span>
                                        </label>
                                        <select name="status" class="form-control">
                                            <option value="">--- Pilih ---</option>
                                            <option value="reject">Tolak Pengajuan</option>
                                            <option value="verified">Setujui Pengajuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Catatan <span class="text-danger">*</span>
                                        </label>
                                        <textarea name="note" class="form-control" style="height: 200px;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-actions pull-right">
                                <button type="submit" name="save" class="btn btn-success"> 
                                    <i class="fa fa-check"></i> Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <embed type="application/pdf" src="<?php echo $proposals_submission['file']; ?>" width="100%" height="98%"></embed>
        </div>
    </div>
</form>

<script type="text/javascript">
    var editor = CKEDITOR.replace( 'surat_permohonan');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'kata_pengantar');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'daftar_isi');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'latar_belakang');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'dasar_hukum');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'maksud_tujuan');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'gambaran_umum_desa');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'potensi_desa');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'program_kegiatan');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'kesimpulan');
    CKFinder.setupCKEditor( editor );

    var editor = CKEDITOR.replace( 'saran');
    CKFinder.setupCKEditor( editor );
</script>