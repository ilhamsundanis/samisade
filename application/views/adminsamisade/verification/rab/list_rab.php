<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<div class="card">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> 
        	<a class="nav-link active" href="javascript:void(0);">
                DAFTAR RAB (Rencana Anggaran Biaya)
            </a> 
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
            <div class="card-body">
                <div class="row">
                	<div class="col-md-12">
						<div class="table-responsive">

                            <?php
                                echo alert()
                            ?>

				            <table id="table-rentist" class="table table-bordered table-striped">
				                <thead>
				                    <tr>
				                        <th>#</th>
					        			<th>KECAMATAN</th>
					        			<th>DESA</th>
					        			<th>PROPOSAL</th>
					        			<th>JENIS KEGIATAN</th>
					        			<th>TOTAL ANGGARAN</th>
					        			<th>#</th>
				                    </tr>
				                </thead>
				                <tbody>
				                	<?php
				                		$no = 0;
				                		foreach ($list_proposals_submission as $data) {
				                			$no++;
				                	?>
					                    <tr>
				                            <td><?php echo $no; ?></td>
						        			<td>
                                                <?php echo $data['kec_name']; ?>
                                            </td>
											<td>
                                                <?php echo $data['desa_name']; ?>
                                            </td>
											<td>
                                                <?php echo $data['title']; ?>
                                            </td>
											<td>
                                                <?php echo $data['activity_name']; ?>
                                            </td>
											<td>
												Rp. <?php echo number_format($data['total_cost'],0,',','.'); ?>
                                            </td>
						        			<td>
						        				<a href="<?php echo base_url(); ?>adminsamisade/verification/rab/view/<?php echo $data['proposal_id']; ?>/<?php echo $data['id']; ?>/<?php echo $data['activity_id']; ?>" class="btn btn-info" style="margin-top: -7px;">
				                                    <i class="fa fa-edit"></i>
				                                </a>
						        			</td>
					                    </tr>
				                    <?php
				                		}
				                	?>
				                </tbody>
				            </table>
				        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>