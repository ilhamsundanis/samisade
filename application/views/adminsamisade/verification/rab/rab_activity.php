<?php
    echo alert()
?>

<div class="card">
    <div class="card-header">
        <strong>PENGAJUAN RAB (RENCANA ANGGARAN BANGUNGAN)</strong><br>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <?php
                foreach ( $list_activity_by_desa as $data ) {
                    if ( ($this->uri->segment(6) == $data['id']) && ($this->uri->segment(7) == $data['activity_id']) ) {
            ?>
                    <li class="nav-item"> 
                        <a class="nav-link active" href="<?php echo base_url() . 'adminsamisade/verification/rab/view/' . $this->uri->segment(5) . '/'  . $data['id'] . '/' . $data['activity_id']; ?>">
                            <?php echo strtoupper($data['name']); ?>
                        </a> 
                    </li>
            <?php
                    }
                }
            ?>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tambah-rab" role="tabpanel">
                <div class="card-body">
                    <?php
                        if ( $detail_all['rab']['status'] == "pending" ) {
                    ?>
                        <a href="#" class="btn btn-danger deleteRAB" style="margin-bottom: 20px;"><i class="fa fa-times"></i> HAPUS RAB (RENCANA ANGGARAN BANGUNGAN)</a>
                    <?php
                        }
                    ?>
                    <form method="post">
                        <table class="table table-bordered table-sm">
                            <tr>
                                <td colspan="7" style="text-align: center; font-size: 16px;">
                                    <strong>RENCANA ANGGARAN BIAYA</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td>:</td>
                                <td>Jawa Barat</td>
                                <td width="40%"></td>
                                <td>Program</td>
                                <td>:</td>
                                <td>Samisade</td>
                            </tr>
                            <tr>
                                <td>Kabupaten</td>
                                <td>:</td>
                                <td>Bogor</td>
                                <td></td>
                                <td>Jenis Kegiatan</td>
                                <td>:</td>
                                <td>
                                    <?php echo $detail_all['rab']['activity_name']; ?>
                                    <input type="hidden" name="activity_id" value="<?php echo $detail_all['rab']['activity_id']; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>:</td>
                                <td><?php echo $detail_all['kec_name']; ?></td>
                                <td></td>
                                <td>Ukuran</td>
                                <td>:</td>
                                <td>
                                    <?php echo $detail_all['rab']['dimension']; ?>
                                    <input type="hidden" name="dimension" value="<?php echo $detail_all['rab']['dimension']; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Desa</td>
                                <td>:</td>
                                <td><?php echo $detail_all['desa_name']; ?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>

                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tabmaps-tab" data-toggle="tab" href="#tabmaps" role="tab" aria-controls="tabmaps" aria-selected="true">MAP</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tabfotomaps-tab" data-toggle="tab" href="#tabfotomaps" role="tab" aria-controls="tabfotomaps" aria-selected="false">FOTO LOKASI</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="tabmaps" role="tabpanel" aria-labelledby="tabmaps-tab">
                                        <br>
                                        <table class="table table-bordered table-sm">
                                            <tr>
                                                <td>Pengaturan tata letak kegiatan</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                        if ( $detail_all['rab']['activity_type'] == "maps_direction" ) {
                                                    ?>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div id="map" style="height: 500px; width: 100%;"></div>
                                                            </div>
                                                            <div class="col-md-4" style="padding: 15px;
                                                                width: 50%;
                                                                height: 500px;
                                                                overflow: scroll;
                                                                border: 1px solid #ccc;">
                                                                <div id="right-panel">
                                                                    <p>Total Jarak: <span id="total"></span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php
                                                        } elseif ( $detail_all['rab']['activity_type'] == "maps" ) {
                                                    ?>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div id="map" style="height: 500px; width: 100%;"></div>
                                                                </div>
                                                            </div>
                                                    <?php
                                                        }
                                                    ?>
                                                    
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="tabfotomaps" role="tabpanel" aria-labelledby="tabfotomaps-tab">
                                        <br>
                                        <div class="card-group">
                                            <?php foreach ( $foto_location as $index => $val ) { ?>
                                            <div class="card">
                                                <div class="card-body">
                                                    <strong>Progres (<?php echo $val['progress']; ?>)</strong> <br>
                                                    <hr>
                                                    <a href="<?php echo $val['foto']; ?>" id="image-a-<?php echo ($index + 1); ?>" data-fancybox>
                                                        <img src="<?php echo $val['foto']; ?>" id="image-img-<?php echo ($index + 1); ?>" width="100%">
                                                    </a>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                            if ( $detail_all['rab']['status'] == "pending" ) {
                        ?>
                        <table class="table table-bordered table-sm">
                            <tr>
                                <td>
                                    Kategori: <br>
                                    <div class="row">
                                        <?php
                                            foreach ($list_category as $data) {
                                                if (in_array($data['id'], $detail_all['category'])) {
                                                    $checked = "checked";
                                                } else {
                                                    $checked = "";
                                                }
                                        ?>
                                            <div class="col-md-2" style="padding: 10px;">
                                                <input type="checkbox" onclick="add_category(this, '<?php echo $data['id']; ?>', '<?php echo $data['name']; ?>')" <?php echo $checked; ?>> <?php echo $data['name']; ?> 
                                            </div>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <?php
                            }
                        ?>

                        <table class="table table-bordered table-sm" id="table-input">
                            <tr>
                                <td width="1%">No</td>
                                <td>Uraian</td>
                                <td>Volume</td>
                                <td>Satuan</td>
                                <td>Harga Satuan</td>
                                <td>Total Harga</td>
                            </tr>

                            <?php
                                $grandtotal = 0;
                                foreach ( $detail_all['rab_item'] as $value ) {
                            ?>
                                    <tr class='category<?php echo $value['category']; ?>'>
                                        <td colspan='6'><?php echo $value['category_name']; ?></td>
                                    </tr>

                                    <?php 
                                        $subtotal = 0;
                                        $no = 0;
                                        foreach ($value['item'] as $data) {
                                            $no++;
                                            $subtotal += $data['total_price'];
                                    ?>
                                            <tr class='category<?php echo $value['category']; ?>'>
                                                <td>
                                                    <?php
                                                        echo $no;
                                                        if ( $detail_all['rab']['status'] == "pending" ) {
                                                    ?>
                                                    <i class='fa fa-times' style='color: red;' onclick='removeFromDB(this, <?php echo $data["id"]; ?>, <?php echo $value["category"]; ?>)'></i>
                                                    <?php
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if ( $data['is_worker'] == true ) {
                                                            echo "<a data-fancybox data-type='iframe' data-src='".base_url()."adminsamisade/verification/rab/popup_employee/".$data['id']."' style='color: #00c292; cursor: pointer;' data-toggle='tooltip' title='Lihat daftar ". $data['name'] ."'>".$data['name']."</a>";
                                                        } else {
                                                            echo $data['name']; 
                                                        }
                                                    ?>    
                                                </td>
                                                <td><?php echo $data['volume']; ?></td>
                                                <td><?php echo $data['unit']; ?></td>
                                                <td style="text-align: right;">Rp. <?php echo number_format($data['unit_price'],0,',','.'); ?></td>
                                                <td style="text-align: right;">Rp. <?php echo number_format($data['total_price'],0,',','.'); ?></td>
                                            </tr>
                                    <?php
                                        }
                                        $grandtotal += $subtotal;
                                    ?>

                                    <tr class='category<?php echo $value["category"]; ?> category-subtotal<?php echo $value["category"]; ?>'>
                                        <td colspan='5' style='text-align: right;'>Sub Total</td>
                                        <td class='subtotal<?php echo $value["category"]; ?>' style='text-align: right;'>Rp. <?php echo number_format($subtotal,0,',','.'); ?></td>
                                    </tr>
                            <?php
                                }
                            ?>
                            
                            <tr>
                                <td colspan="5" style="text-align: right;">Grand Total</td>
                                <td style='text-align: right;' class="grandtotal">Rp. <?php echo number_format($grandtotal,0,',','.'); ?></td>
                            </tr>
                            
                        </table>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Verifiaksi Data<span class="text-danger">*</span>
                                    </label>
                                    <select name="status" class="form-control">
                                        <option value="">--- Pilih ---</option>
                                        <option value="reject">Tolak RAB </option>
                                        <option value="verified">Setujui RAB</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Catatan <span class="text-danger">*</span>
                                    </label>
                                    <textarea name="note" class="form-control" style="height: 200px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-actions pull-right">
                                    <button type="submit" name="save" class="btn btn-success"> 
                                        <i class="fa fa-check"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    if ( $detail_all['rab']['activity_type'] == "maps_direction" ) {
?>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly&language=id"
            async
        ></script>
        <script>
            function initMap() {
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 4,
                    center: { lat: -24.345, lng: 134.46 },
                });
                const directionsService = new google.maps.DirectionsService();
                const directionsRenderer = new google.maps.DirectionsRenderer({
                    map,
                    panel: document.getElementById("right-panel"),
                });
                directionsRenderer.addListener("directions_changed", () => {
                    computeTotalDistance(directionsRenderer.getDirections());
                });
                displayRoute(
                    "<?php echo $detail_all['rab_location']['point1']; ?>",
                    "<?php echo $detail_all['rab_location']['point2']; ?>",
                    directionsService,
                    directionsRenderer
                );
            }

            function displayRoute(origin, destination, service, display) {
                service.route(
                    {
                        origin: origin,
                        destination: destination,
                        travelMode: google.maps.TravelMode.WALKING,
                        avoidTolls: true,
                    },
                    (result, status) => {
                        if (status === "OK") {
                            display.setDirections(result);
                        } else {
                            alert("Could not display directions due to: " + status);
                        }
                    }
                );
            }

            function computeTotalDistance(result) {
                let total = 0;
                const myroute = result.routes[0];

                for (let i = 0; i < myroute.legs.length; i++) {
                    total += myroute.legs[i].distance.value;
                }
                total = total / 1000;
                document.getElementById("total").innerHTML = total + " km";
            }
        </script>
<?php
    } else if ($detail_all['rab']['activity_type'] == "maps") {
?>
        <script
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDR5qarJ7U46r2roVyqF5VENWNGwtpvKQ&callback=initMap&libraries=places&v=weekly"
            async
        ></script>

        <script>
            function initMap() {
                const myLatLng = { lat: -25.363, lng: 131.044 };
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 15,
                    center: myLatLng,
                });
                var vMarker = new google.maps.Marker({
                    position: { lat: <?php echo explode(",", $detail_all['rab_location']['point1'])[0]; ?>, lng: <?php echo explode(",", $detail_all['rab_location']['point1'])[1]; ?> },
                    map,
                });

                // centers the map on markers coords
                map.setCenter(vMarker.position);

                // adds the marker on the map
                vMarker.setMap(map);
            }
        </script>
<?php
    }
?>

<script type="text/javascript">
    $(function(){
		$('[data-fancybox]').fancybox({
            toolbar  : false,
            smallBtn : true,
            iframe : {
                preload : true,
                css : {
                    height : '800px'
                }
            },
            fullScreen : {
                requestOnStart : true // Request fullscreen mode on opening
            },
        });
    });
    
    $( document ).ready(function() {
        $("#search_input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

        $("#origin-input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

        $("#destination-input").keypress(function(e) {
            //Enter key
            if (e.which == 13) {
                return false;
            }
        });

    });
</script>