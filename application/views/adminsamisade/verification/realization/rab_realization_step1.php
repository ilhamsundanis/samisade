<style>
* {
    margin: 0;
    padding: 0
}


#heading {
    text-transform: uppercase;
    color: #673AB7;
    font-weight: normal
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #673AB7;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #673AB7;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #673AB7;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #673AB7;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    /* margin-bottom: 30px; */
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #00a687
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar #proposal_submission:before {
    font-family: FontAwesome;
    content: "\f1ea"
}

#progressbar #rab:before {
    font-family: FontAwesome;
    content: "\f022"
}

#progressbar #realisasi:before {
    font-family: FontAwesome;
    content: "\f093"
}

#progressbar #finish:before {
    font-family: FontAwesome;
    content: "\f05d"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #00a687
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #00a687
}

.fit-image {
    width: 100%;
    object-fit: cover
}
</style>

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor"><?php echo $title_page; ?></h4>
    </div>
</div>

<?php
    echo alert()
?>

<div class="card">
    <div class="card-header">
        <strong>INPUT RAB (RENCANA ANGGARAN BANGUNGAN)</strong><br>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <?php
                foreach ( $list_activity_by_desa as $data ) {
                    if ( ($this->uri->segment(6) == $data['id']) && ($this->uri->segment(7) == $data['activity_id']) ) {
            ?>
                    <li class="nav-item"> 
                        <a class="nav-link active" href="javascript:void();">
                            <?php echo strtoupper($data['name']); ?>
                        </a> 
                    </li>
            <?php
                    }
                }
            ?>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tambah-rab" role="tabpanel">
                <div class="card-body">

                    <div class="row">
                        <div class="col-2" style="border-right: 1px solid #dee2e6;">
                            <div class="nav flex-column nav-pills" aria-orientation="vertical">
                                <a class="nav-link active" href="javascript:void();">Realisasi Tahap 1 (40%)</a>
                            </div>
                        </div>
                        <div class="col-10">
                            <div class="tab-content" id="v-pills-tabContent">
                                
                                <form method="post">
                                    <table class="table table-bordered table-sm">
                                        <tr>
                                            <td colspan="7" style="text-align: center; font-size: 16px;">
                                                <strong>REALISASI RENCANA ANGGARAN BIAYA TAHAP 1</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Provinsi</td>
                                            <td>:</td>
                                            <td>Jawa Barat</td>
                                            <td width="40%"></td>
                                            <td>Program</td>
                                            <td>:</td>
                                            <td>Samisade</td>
                                        </tr>
                                        <tr>
                                            <td>Kabupaten</td>
                                            <td>:</td>
                                            <td>Bogor</td>
                                            <td></td>
                                            <td>Jenis Kegiatan</td>
                                            <td>:</td>
                                            <td>
                                                <?php echo $detail_all['rab']['activity_name']; ?>
                                                <input type="hidden" name="activity_id" value="<?php echo $detail_all['rab']['activity_id']; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Kecamatan</td>
                                            <td>:</td>
                                            <td><?php echo $detail_all['kec_name']; ?></td>
                                            <td></td>
                                            <td>Ukuran</td>
                                            <td>:</td>
                                            <td>
                                                <?php echo $detail_all['rab']['dimension']; ?>
                                                <input type="hidden" name="dimension" value="<?php echo $detail_all['rab']['dimension']; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Desa</td>
                                            <td>:</td>
                                            <td><?php echo $detail_all['desa_name']; ?></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>

                                    <table class="table table-bordered table-sm" id="table-input">
                                        <tr>
                                            <td width="1%">No</td>
                                            <td>Uraian</td>
                                            <td>Volume * Harga Satuan</td>
                                            <td>Total Harga</td>
                                            <td>Bukti Nota</td>
                                            <td>Bukti Kegiatan</td>
                                        </tr>

                                        <?php
                                            $grandtotal = 0;
                                            foreach ( $detail_all['rab_item'] as $value ) {
                                        ?>
                                                <tr class='category<?php echo $value['category']; ?>'>
                                                    <td colspan='8'><?php echo $value['category_name']; ?></td>
                                                </tr>

                                                <?php 
                                                    $subtotal = 0;
                                                    $no = 0;
                                                    foreach ($value['item'] as $data) {
                                                        $no++;
                                                        $subtotal += $data['total_price'];
                                                ?>
                                                        <tr class='category<?php echo $value['category']; ?>'>
                                                            <td style="vertical-align: middle;">
                                                                <?php
                                                                    echo $no;
                                                                ?>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                                <?php
                                                                    if ( $data['is_worker'] == true ) {
                                                                        echo "<a data-fancybox data-type='iframe' data-src='".base_url()."adminsamisade/verification/rab/popup_employee/".$data['id']."' style='color: #00c292; cursor: pointer;' data-toggle='tooltip' title='Lihat daftar ". $data['name'] ."'>".$data['name']."</a>";
                                                                    } else {
                                                                        echo $data['name']; 
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td style="text-align: right; vertical-align: middle;">
                                                                <?php echo $data['volume']; ?> <?php echo $data['unit']; ?> * 
                                                                Rp. <?php echo number_format($data['unit_price'],0,',','.'); ?></td>
                                                            <td style="text-align: right; vertical-align: middle;">Rp. <?php echo number_format($data['total_price'],0,',','.'); ?></td>
                                                            <td style="text-align: center; vertical-align: middle;">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td>No</td>
                                                                        <td>Bukti Nota</td>
                                                                        <td>Pembelian</td>
                                                                        <td>Status</td>
                                                                        <td>Aksi</td>
                                                                    </tr>
                                                                    <?php 
                                                                        $no = 0;
                                                                        $total = 0;
                                                                        foreach( $data['realization']['step1']['receipt'] as $val) { 
                                                                            $no++;
                                                                            $total += $val['total_price'];
                                                                    ?>
                                                                            <tr>
                                                                                <td><?php echo $no; ?></td>
                                                                                <td>
                                                                                    <a href="<?php echo $val['file']; ?>" data-fancybox class="btn btn-success" data-toggle="tooltip" title="Lihat Bukti Nota">
                                                                                        <i class="fa fa-eye"></i>
                                                                                    </a>
                                                                                </td>
                                                                                <td>Rp. <?php echo number_format($val['total_price'],0,',','.'); ?></td>
                                                                                <td class="form-status<?php echo $val['id']; ?>">
                                                                                    <span class="label label-<?php echo @parsing_status_class()[$val['status']]; ?>">
                                                                                        <strong><?php echo @parsing_status()[$val['status']]; ?></strong>
                                                                                    </span>
                                                                                </td>
                                                                                <td class="form-aksi<?php echo $val['id']; ?>">
                                                                                    <?php if ( $val['status'] == "verification" ) { ?>
                                                                                    <span class="label label-danger" style="cursor: pointer;" onclick="verified_realization(this, '<?php echo $val['id']; ?>', '<?php echo $detail_all['rab']['id']; ?>', 'reject', '1');">
                                                                                        <strong>Tolak</strong>
                                                                                    </span>
                                                                                    &nbsp; | &nbsp;
                                                                                    <span class="label label-success" style="cursor: pointer;" onclick="verified_realization(this, '<?php echo $val['id']; ?>', '<?php echo $detail_all['rab']['id']; ?>', 'verified', '1');">
                                                                                        <strong>Setujui</strong>
                                                                                    </span>
                                                                                    <?php } ?>
                                                                                </td>
                                                                            </tr>
                                                                    <?php } ?>
                                                                    <tr>
                                                                        <td colspan="2">Total Pembelian</td>
                                                                        <td>Rp. <?php echo number_format($total,0,',','.'); ?></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="text-align: center; vertical-align: middle;">
                                                                <a data-fancybox data-type="iframe" data-src="<?php echo base_url(); ?>adminsamisade/proposals_submission/popup_rab_gallery_realization_activities/<?php echo $detail_all['rab']['id']."/".$data['id']; ?>/1" class="btn btn-success"  style="color: white;" data-toggle="tooltip" title="Lihat Bukti Kegiatan">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                                <br>
                                                                Total Bukti Kegiatan: <br>
                                                                <span class="label label-info">
                                                                    <strong><?php echo count($data['realization']['step1']['activities']); ?></strong>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                <?php
                                                    }
                                                    $grandtotal += $subtotal;
                                                ?>
                                        <?php
                                            }
                                        ?>
                                        
                                        <tr>
                                            <td colspan="8" style="text-align: center; font-size: 20px;">
                                                TOTAL DANA CAIR TAHAP 1 (40%) <br>
                                                Rp. <?php echo number_format(($grandtotal*0.4),0,',','.'); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <div style="text-align: center; font-size: 20px; width: 100%;">
                                                    Total Penggunaan Dana Telah Mencapai: 
                                                    <span class="percentage"><?php echo check_percentage($detail_all['rab']['id'], 1)['percentage']; ?></span>% 
                                                    <span class="grandtotal_pembayaran">(Rp. <?php echo check_percentage($detail_all['rab']['id'], 1)['grandtotal_pembayaran']; ?>)</span>
                                                </div>
                                                <div>
                                                    <span class="label label-info">Informasi: </span> 
                                                    <i>Anda bisa melanjutkan pencairan dana tahap 2 setelah penggunaan dana tahap 1 telah mencapai 75% atau lebih</i>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Catatan <span class="text-danger">*</span>
                                                        </label>
                                                        <textarea name="note" class="form-control" style="height: 200px;"><?php echo $detail_all['rab']['step1_notes']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-actions pull-right">
                                                        <button type="submit" name="save" class="btn btn-success"> 
                                                            <i class="fa fa-check"></i> Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    function callbackfancybox( id, url, percentage, total_pembayaran, grandtotal_pembayaran ) {
        var html = ""

        html += "&nbsp;|&nbsp;" +
                "<a class='btn btn-success link"+id+"' href='"+url+"' data-fancybox>" +
                    "<i class='fa fa-eye'></i> " +
                    "Lihat Gambar" +
                "</a>";
        
        $(".viewimage"+id).empty();
        $(".viewimage"+id).html(html);
        $(".percentage").text(percentage);
        $(".grandtotal_pembayaran").text(grandtotal_pembayaran);
        $(".total_pembayaran"+id).text(formatRupiah(total_pembayaran, "Rp. "));

        if ( percentage >= 75 ) {
            $(".send").attr("disabled", false);
        } else {
            $(".send").attr("disabled", true);
        }
    }

    function verified_realization(that, rab_item_realization_id, rab_id, status, step) {
        tempStatus = "";
        if (status == "reject") {
            tempStatus = "Tolak";
        } else {
            tempStatus = "Menyetujui";
        }
        
        swal({
            text: "Apakah anda ingin " + tempStatus + " Realisasi ini?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: '<?php echo base_url("adminsamisade/verification/realization/verified_realization"); ?>',
                    data: { rab_item_realization_id: rab_item_realization_id, rab_id: rab_id, status: status, step: step },
                    success: function (data) {
                        if ( data.status == "success" ) {
                            html  = "";
                            if ( status == "verified" ) {
                                html += "<span class='label label-success'>" +
                                            "<strong>"+data.data.status+"</strong>" +
                                        "</span>";
                            } else {
                                html += "<span class='label label-danger'>" +
                                            "<strong>"+data.data.status+"</strong>" +
                                        "</span>";
                            }
                            $(".form-status"+rab_item_realization_id).empty();
                            $(".form-status"+rab_item_realization_id).html(html);
                            $(".form-aksi"+rab_item_realization_id).empty();

                            $(".percentage").empty();
                            $(".percentage").text(data.data.percentage);
                            
                            $(".grandtotal_pembayaran").empty();
                            $(".grandtotal_pembayaran").text(data.data.grandtotal_pembayaran);
                        }
                    }
                });     
            }
        });
    }

    $(function(){
		$('[data-fancybox]').fancybox({
            toolbar  : false,
            smallBtn : true,
            iframe : {
                preload : true,
                css : {
                    height : '800px'
                }
            },
            fullScreen : {
                requestOnStart : true // Request fullscreen mode on opening
            },
        });
    });
</script>