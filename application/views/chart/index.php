<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<section class="page-title-area" style="background-image: url(<?php echo base_url(); ?>assets/images/page-title-bg1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="page-title text-center">
                    <h1><?php echo $title_page; ?></h1>
                    <div class="breadcrumb">
                        <ul class="breadcrumb-list">
                            <li><a href="index-2.html">HOME</a></li>
                            <li><a class="active" href="#"><?php echo $title_page; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="theme-bg pt-40 pb-30 pl-60 pr-60">
                <div class="row no-gutters align-items-center">
                    <div class="col-md-12">
                        <div class="section-title white-title text-left">
                            <h6>FILTER DATA</h6>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label text-white">
                                            Kecamatan
                                        </label>
                                        <select name="kec_id" id="kec_id" class="select2 form-control">
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_kecamatan as $data) {
                                            ?>
                                                <option value="<?php echo $data['id']; ?>-<?php echo $data['name']; ?>">
                                                    <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label text-white">
                                            Desa
                                        </label>
                                        <select id="desa_id" name="desa_id" class="select2 form-control">
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-actions pull-right">
                                        <button type="submit" class="btn btn-info"> 
                                            <i class="fa fa-check"></i> Filter
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
            <?php echo $result_search; ?>
        </div>
    </div>
</div>

<hr>

<div class="container mb-100">
    <div class="row">
        <div class="col-md-12">
            <div class="portfolio-wrapper">
                <ul class="nav nav-tabs mb-20" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="grafik-tab" data-toggle="tab" href="#grafik" role="tab" aria-controls="grafik" aria-selected="true">
                            <span class="box-btn"></span> 
                            GRAFIK
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tabel-tab" data-toggle="tab" href="#tabel" role="tab" aria-controls="tabel" aria-selected="false">
                            <span class="box-btn"></span>
                            TABEL
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="grafik" role="tabpanel" aria-labelledby="grafik-tab">
                        <canvas id="chart" height="1500"></canvas>
                    </div>
                    <div class="tab-pane fade" id="tabel" role="tabpanel" aria-labelledby="tabel-tab">
                        <?php if ( $category == "total-anggaran" ) { ?>
                            <table class="table-custom">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kecamatan</th>
                                        <th>Desa</th>
                                        <th>Total Anggaran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0;
                                        $total = 0;
                                        foreach ($get_table as $value) {
                                            $no++;
                                            $total += $value['total_cost'];
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value['kec_name']; ?></td>
                                        <td><?php echo $value['desa_name']; ?></td>
                                        <td style="text-align: right;">Rp. <?php echo number_format($value['total_cost'],0,',','.'); ?></td>
                                    </tr>
                                </tbody>
                                <?php } ?>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="text-align: right;">Total Keseluruhan Anggaran</td>
                                        <td style="text-align: right;">Rp. <?php echo number_format($total,0,',','.'); ?></td>
                                    </tr>
                                </tfoot>
                            </table>

                        <?php } else if ( $category == "jumlah-dana" ) { ?>

                            <table class="table-custom">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Kecamatan</th>
                                        <th rowspan="2">Desa</th>
                                        <th colspan="2" style="text-align: center;">Jumlah Dana Terealisasi</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align: center;">Tahap 1</th>
                                        <th style="text-align: center;">Tahap 2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0;
                                        $total_step1 = 0;
                                        $total_step2 = 0;
                                        foreach ($get_table as $value) {
                                            $no++;
                                            $total_step1 += $value['step1'];
                                            $total_step2 += $value['step2'];
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value['kec_name']; ?></td>
                                        <td><?php echo $value['desa_name']; ?></td>
                                        <td style="text-align: right;">Rp. <?php echo number_format($value['step1'],0,',','.'); ?></td>
                                        <td style="text-align: right;">Rp. <?php echo number_format($value['step2'],0,',','.'); ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="text-align: right;">Total Keseluruhan Anggaran</td>
                                        <td style="text-align: right;">Rp. <?php echo number_format($total_step1,0,',','.'); ?></td>
                                        <td style="text-align: right;">Rp. <?php echo number_format($total_step2,0,',','.'); ?></td>
                                    </tr>
                                </tfoot>
                            </table>

                        <?php } else if ( $category == "persentase-pembangunan" ) { ?>

                            <table class="table-custom">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Kecamatan</th>
                                        <th rowspan="2">Desa</th>
                                        <th colspan="2" style="text-align: center;">Perentase Pembangunan</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align: center;">Tahap 1</th>
                                        <th style="text-align: center;">Tahap 2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 0;
                                        $total_step1 = 0;
                                        $total_step2 = 0;
                                        foreach ($get_table as $value) {
                                            $no++;
                                            $total_step1 += $value['step1'];
                                            $total_step2 += $value['step2'];
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $value['kec_name']; ?></td>
                                        <td><?php echo $value['desa_name']; ?></td>
                                        <td style="text-align: center;"><?php echo $value['step1'] ?>%</td>
                                        <td style="text-align: center;"><?php echo $value['step2'] ?>%</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/chart/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "-" + data[i].name + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    var ctx = document.getElementById('chart').getContext('2d');
    var chart = new Chart(ctx, {
        type: 'horizontalBar',
        data: <?php echo $get_chart; ?>,
        options: {
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        <?php if ( $category == "persentase-pembangunan" ) { ?>
                            return parseFloat(tooltipItem.value).toFixed(2) + "%";
                        <?php } else { ?>
                            return formatRupiah(tooltipItem.value, "Rp. ");
                        <?php } ?>
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes:[
                    {
                        ticks:{
                            beginAtZero:true,
                            callback: function(value, index, values) {
                                <?php if ( $category == "persentase-pembangunan" ) { ?>
                                    return parseFloat(value).toFixed(2) + "%";
                                <?php } else { ?>
                                    return formatRupiah(value.toString(), "Rp. ");
                                <?php } ?>
                            }
                    }
                }]
            },
        }
    });
</script>