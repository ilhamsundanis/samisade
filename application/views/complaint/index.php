<section class="page-title-area" style="background-image: url(<?php echo base_url(); ?>assets/images/page-title-bg1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="page-title text-center">
                    <h1>ADUAN MASYARAKAT</h1>
                    <div class="breadcrumb">
                        <ul class="breadcrumb-list">
                            <li><a href="index-2.html">HOME</a></li>
                            <li><a class="active" href="#">ADUAN MASYARAKAT</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="subscribe-details-area pt-100 pb-100">
    <div class="container">
        <div class="section-title text-left mb-20">
            <h6 class="left_line pl-55">FORM INPUT ADUAN MASYARAKAT</h6>
            <!-- <h2>FORM INPUT ADUAN MASYARAKAT</h2> -->
        </div>
        <div class="sub-bg-04 pos-rel white-bg pb-50">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 col-lg-11 offset-lg-2 col-12">
                    <div class="registration-form">
                        <div class="row pt-50">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">
                                        Nama Pelapor <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">
                                        Email Pelapor <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" name="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">
                                        No Telepon <span class="text-danger">*</span>
                                    </label>
                                    <input type="text" name="phone" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Kecamatan <span class="text-danger">*</span>
                                    </label>
                                    <select id="kec_id" class="select2 form-control" required>
                                        <option value=""> --- Pilih --- </option>
                                        <?php
                                            foreach ($list_kecamatan as $data) {
                                        ?>
                                            <option value="<?php echo $data['id']; ?>">
                                                <?php echo $data['name']; ?>
                                            </option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Desa <span class="text-danger">*</span>
                                    </label>
                                    <select id="desa_id" name="desa_id" class="select2 form-control" required>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Alamat <span class="text-danger">*</span>
                                    </label>
                                    <textarea name="address" cols="30" rows="2" class="form-control" required></textarea>
                                </div>
                            </div>

                            <div class="col-md-12 form-radius">
                                <div class="form-group">
                                    <label class="control-label">
                                        Aduan <span class="text-danger">*</span>
                                    </label>
                                    <textarea name="description" cols="30" rows="10" class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12">
                                <div class="subsribe-btn text-center">
                                    <button class="sub-btn2" type="submit" name="save">KIRIM <i class="fal fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/proposals_submission/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });
</script>