<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<section class="page-title-area" style="background-image: url(<?php echo base_url(); ?>assets/images/page-title-bg1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="page-title text-center">
                    <h1>TANGGAL PENCAIRAN DANA</h1>
                    <div class="breadcrumb">
                        <ul class="breadcrumb-list">
                            <li><a href="index-2.html">HOME</a></li>
                            <li><a class="active" href="#">TANGGAL PENCAIRAN DANA</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="events-area pt-100 pb-40">
    <div class="container">
        <div class="portfolio-wrapper pl-40">
            <div class="section-title mb-10">
                <h6 class="left_line pl-55">-</h6>
                <h2>Gelombang</h2>
            </div>
            <ul class="nav nav-tabs mb-20" id="myTab" role="tablist">
                <?php 
                    foreach ( $list_disbursement_date as $index => $data ) { 
                        if ( $index == 0 ) {
                            $active = "active show";
                        } else {
                            $active = "";
                        }
                ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo $active; ?>" id="wave<?php echo $data['wave']; ?>-tab" data-toggle="tab" href="#wave<?php echo $data['wave']; ?>" role="tab" aria-controls="wave<?php echo $data['wave']; ?>" aria-selected="true">
                        <?php echo $data['wave']; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
            <div class="tab-content" id="myTabContent">
                <hr>
                <?php 
                    foreach ( $list_disbursement_date as $index => $data ) { 
                        if ( $index == 0 ) {
                            $active = "active show";
                        } else {
                            $active = "";
                        }
                ?>
                <div class="tab-pane fade <?php echo $active; ?>" id="wave<?php echo $data['wave']; ?>" role="tabpanel" aria-labelledby="wave<?php echo $data['wave']; ?>-tab">
                    <div id="accordion">
                        <!-- step1 -->
                        <div class="card mb-15">
                            <div class="card-header" id="heading-wave<?php echo $data['wave']; ?>-step1">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#wave<?php echo $data['wave']; ?>-step1" aria-expanded="false" aria-controls="wave<?php echo $data['wave']; ?>-step1">
                                        TAHAP 1 (40%)
                                    </button>
                                </h5>
                            </div>

                            <div id="wave<?php echo $data['wave']; ?>-step1" class="collapse" aria-labelledby="heading-wave<?php echo $data['wave']; ?>-step1" data-parent="#accordion" style="">
                                <div class="card-body">
                                    <div class="faq-content">
                                        <table class="table-custom">
                                            <!-- <thead> -->
                                                <tr>
                                                    <th>NO</th>
                                                    <th>KECAMATAN</th>
                                                    <th>DESA</th>
                                                    <th>DANA</th>
                                                    <th>TANGGAL PENCAIRAN</th>
                                                </tr>
                                            <!-- </thead> -->
                                            <?php
                                                $no = 0;
                                                $total = 0;
                                                foreach ( $data['data']['step1'] as $val ) {
                                                    $no++;
                                                    $total += $val['liquid_funds'];
                                            ?>
                                            <!-- <tbody> -->
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $val['kec_name']; ?></td>
                                                    <td><?php echo $val['desa_name']; ?></td>
                                                    <td class="text-right">Rp. <?php echo number_format($val['liquid_funds'],0,',','.'); ?></td>
                                                    <td><?php echo $val['disbursement_date']; ?></td>
                                                </tr>
                                            <!-- </tbody> -->
                                            <?php
                                                }
                                            ?>
                                            <!-- <tfoot> -->
                                                <tr>
                                                    <td colspan="3" class="text-right">Total Keseluruhan</td>
                                                    <td class="text-right">Rp. <?php echo number_format($total,0,',','.'); ?></td>
                                                    <td></td>
                                                </tr>
                                            <!-- </tfoot> -->
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- step2 -->
                        <div class="card mb-15">
                            <div class="card-header" id="heading-wave<?php echo $data['wave']; ?>-step2">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#wave<?php echo $data['wave']; ?>-step2" aria-expanded="false" aria-controls="wave<?php echo $data['wave']; ?>-step2">
                                        TAHAP 2 (60%)
                                    </button>
                                </h5>
                            </div>

                            <div id="wave<?php echo $data['wave']; ?>-step2" class="collapse" aria-labelledby="heading-wave<?php echo $data['wave']; ?>-step2" data-parent="#accordion" style="">
                                <div class="card-body">
                                    <div class="faq-content">
                                        <table class="table-custom">
                                            <!-- <thead> -->
                                                <tr>
                                                    <th>NO</th>
                                                    <th>KECAMATAN</th>
                                                    <th>DESA</th>
                                                    <th>DANA</th>
                                                    <th>TANGGAL PENCAIRAN</th>
                                                </tr>
                                            <!-- </thead> -->
                                            <?php
                                                $no = 0;
                                                $total = 0;
                                                foreach ( $data['data']['step2'] as $val ) {
                                                    $no++;
                                                    $total += $val['liquid_funds'];
                                            ?>
                                            <!-- <tbody> -->
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $val['kec_name']; ?></td>
                                                    <td><?php echo $val['desa_name']; ?></td>
                                                    <td class="text-right">Rp. <?php echo number_format($val['liquid_funds'],0,',','.'); ?></td>
                                                    <td><?php echo $val['disbursement_date']; ?></td>
                                                </tr>
                                            <!-- </tbody> -->
                                            <?php
                                                }
                                            ?>
                                            <!-- <tfoot> -->
                                                <tr>
                                                    <td colspan="3" class="text-right">Total Keseluruhan</td>
                                                    <td class="text-right">Rp. <?php echo number_format($total,0,',','.'); ?></td>
                                                    <td></td>
                                                </tr>
                                            <!-- </tfoot> -->
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>