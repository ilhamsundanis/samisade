<!--slider-area start-->
<div class="slider-area slider-area-02 heding-bg pos-rel">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
                foreach ( $data['banner'] as $index => $val ) {
                    if ( $index == 0 ) {
                        $active = "active";
                    } else {
                        $active = "";
                    }
            ?>
            <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $index; ?>" class="<?php echo $active; ?>"></li>
            <?php } ?>
        </ol>
        <div class="carousel-inner">
            <?php
                foreach ( $data['banner'] as $idx => $val ) {
                    if ( $idx == 0 ) {
                        $active = "active";
                    } else {
                        $active = "";
                    }
            ?>
                <div class="carousel-item <?php echo $active; ?>">
                    <img class="d-block w-100" src="<?php echo $val['image']; ?>">
                </div>
            <?php
                }
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<!--slider-area end-->
<!--portfolio-area start-->
<section class="portfolio-area pt-100 pb-100">
    <div class="container">
        <div class="row ">
            <div class="col-md-6 text-center">
                <img class="img-fluid" src="<?php echo base_url(); ?>images/logo/logo.png" width="50%" alt="">
            </div>
            <div class="col-md-6">
                <div class="portfolio-wrapper-02">
                    <div class="portfolio">
                        <div class="section-title mb-15">
                            <h6 class="left_line pl-55">SAMISADE</h6>
                            <h2>Satu Milyar Satu Desa</h2>
                        </div>
                        <div class="portfolio__content">
                            <p class="mb-35"><?php echo load_company()['description']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--portfolio-area end-->

<!--events-area end-->
<section class="events-area grey-bg-03 pt-100 pb-100">
    <div class="container">
        <div class="row align-items-center mb-60">
            <div class="col-xl-6 col-lg-5">
                <div class="section-title text-left">
                    <h6 class="left_line pl-55">Berita Terkini</h6>
                    <h2>Berita Terkini</h2>
                </div>
            </div>
        </div>
        <div class="tab-content pr-20" id="myTabContent">
            <div class="tab-pane fade show active">
                <div class="row">
                <?php 
                    foreach ( $data['news'] as $index => $value ) {
                ?>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="events pos-rel mb-60">
                            <div class="events__img">
                                <div class="events__img--thumb">
                                    <img src="<?php echo $value['image']; ?>" height="365" alt="">
                                </div>
                                <span><b><?php echo date_format(date_create($value['created_at']), "d")?> </b>
                                <?php echo date_format(date_create($value['created_at']), "M")?> </span>
                            </div>
                            <div class="events__content">
                                <h5 class="semi-title mb-15"><a href="<?php echo base_url(); ?>news/detail/<?php echo $value['id']; ?>"><?php echo $value['title']; ?></a></h5>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--events-area end-->

<!--events-area end-->
<section class="events-area pt-100 pb-100">
    <div class="container">
        <div class="row align-items-center mb-60">
            <div class="col-xl-6 col-lg-5">
                <div class="section-title text-left">
                    <h6 class="left_line pl-55">Laporan Kegiatan</h6>
                    <h2>Laporan Kegiatan</h2>
                </div>
            </div>
        </div>
        <div class="tab-content pr-20" id="myTabContent">
            <div class="tab-pane fade show active">
                <div class="row">
                <?php 
                    foreach ( $data['report_kecamatan'] as $index => $value ) {
                ?>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="events pos-rel mb-60">
                            <div class="events__img">
                                <div class="events__img--thumb">
                                    <img src="<?php echo $value['image1']; ?>" height="365" alt="">
                                </div>
                                <span><b><?php echo date_format(date_create($value['created_at']), "d")?> </b>
                                <?php echo date_format(date_create($value['created_at']), "M")?> </span>
                            </div>
                            <div class="events__content">
                                <h5 class="semi-title mb-15"><a href="<?php echo base_url(); ?>report/detail/<?php echo $value['id']; ?>"><?php echo $value['title']; ?></a></h5>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--events-area end-->