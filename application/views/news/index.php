<section class="page-title-area" style="background-image: url(<?php echo base_url(); ?>assets/images/page-title-bg1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="page-title text-center">
                    <h1>BERITA TERKINI</h1>
                    <div class="breadcrumb">
                        <ul class="breadcrumb-list">
                            <li><a href="index-2.html">HOME</a></li>
                            <li><a class="active" href="#">BERITA TERKINI</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="news-area-02 pt-40 pb-100">
    <div class="container">
        <div class="row">
        <?php
            foreach ($data as $value) {
        ?>
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="blog">
                    <div class="blog__img mb-50">
                        <img src="<?php echo $value['image']; ?>" alt="">
                    </div>
                    <div class="blog__content">
                        <h3 class="blog-title mb-15">
                            <a href="<?php echo base_url(); ?>news/detail/<?php echo $value['id']; ?>">
                                <?php echo $value['title']; ?>
                            </a>
                        </h3>
                        <div class="blog__content--meta mb-20 text-right;">
                            <span><i class="far fa-calendar-alt"></i> <?php echo $value['created_at']; ?></span>
                        </div>
                        <p class="mb-35" style="text-align: "><?php echo substr($value['partial_description'], 0, 300); ?></p>
                        <div class="blog__content--list d-flex align-items-center justify-content-between">
                            <a class="blog-btn" href="<?php echo base_url(); ?>news/detail/<?php echo $value['id']; ?>"><img src="assets/img/news/bloger1.png" alt=""> by
                            <?php echo $value['user_name']; ?></a>
                            <a class="more_btn4" href="<?php echo base_url(); ?>news/detail/<?php echo $value['id']; ?>"><i class="far fa-arrow-right"></i> Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
            <?php 
                echo $this->pagination->create_links();
            ?>
    </div>
</section>