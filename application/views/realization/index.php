<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<section class="page-title-area" style="background-image: url(<?php echo base_url(); ?>assets/images/page-title-bg1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="page-title text-center">
                    <h1>REALISASI PEMBANGUNAN</h1>
                    <div class="breadcrumb">
                        <ul class="breadcrumb-list">
                            <li><a href="index-2.html">HOME</a></li>
                            <li><a class="active" href="#">REALISASI PEMBANGUNAN</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="theme-bg pt-40 pb-30 pl-60 pr-60">
                <div class="row no-gutters align-items-center">
                    <div class="col-md-12">
                        <div class="section-title white-title text-left">
                            <h6>FILTER DATA</h6>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <label class="control-label text-white">
                                            Kecamatan
                                        </label>
                                        <select name="kec_id" id="kec_id" class="select2 form-control">
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_kecamatan as $data) {
                                            ?>
                                                <option value="<?php echo $data['id']; ?>-<?php echo $data['name']; ?>">
                                                    <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div>
                                        <label class="control-label text-white">
                                            Desa
                                        </label>
                                        <select id="desa_id" name="desa_id" class="select2 form-control">
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div>
                                        <label class="control-label text-white">
                                            Jenis Pembangunan
                                        </label>
                                        <select name="activity_id" id="activity_id" class="select2 form-control">
                                            <option value=""> --- Pilih --- </option>
                                            <?php
                                                foreach ($list_activity as $data) {
                                            ?>
                                                <option value="<?php echo $data['id']; ?>-<?php echo $data['name']; ?>">
                                                    <?php echo $data['name']; ?>
                                                </option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div>
                                        <label class="control-label text-white">
                                            Anggaran
                                        </label>
                                        <select name="anggaran" id="anggaran" class="select2 form-control">
                                            <option value=""> --- Pilih --- </option>
                                                <option value="asc">
                                                    Terendah
                                                </option>
                                                <option value="desc">
                                                    Terbesar
                                                </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12 pt-10">
                                    <div class="form-actions pull-right">
                                        <button type="submit" class="btn btn-info"> 
                                            <i class="fa fa-check"></i> Filter
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="pt-40 pb-30 pl-60 pr-60">
                <div class="row no-gutters align-items-center">
                    <div class="col-md-12">
                        <div class="section-title white-title text-left">
                            <h6 style="color:black;">GRAFIK TOTAL PEMBANGUNAN</h6>
                        </div>
                    </div>
                    <hr>
                    <div class="col-md-12">
                        <canvas id="chart1" height="150"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px;">
            <?php echo $result_search; ?>
        </div>
    </div>
</div>

<hr>

<div class="container">
    <div class="row">
    <?php foreach( $list_all_data as $data ) { ?>
        <div class="col-md-12 mb-5">
            <div class="section-title text-left">
                <h6 class="left_line pl-55">KECAMATAN: <?php echo $data['proposal']['kec_name']; ?></h6>
                <h3>DESA: <?php echo $data['proposal']['desa_name']; ?></h3>
            </div>
            <div class="row">
                <?php foreach( $data['rab'] as $value ) { ?>
                    <div class="col-md-6">
                        <!-- <div class="col-md-12"> -->
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <p class="text-muted">
                                        Pembangunan: <?php echo $value['activity_name']; ?> <br>
                                        <label class="label label-<?php echo parsing_status_class()[$value['status']]; ?>">
                                            <?php echo parsing_status()[$value['status']]; ?>
                                        </label>
                                    </p>
                                </div>
                                <div class="ml-auto">
                                    <h4 class="text-success">
                                        Rp. <?php echo number_format($value['total_cost'],0,',','.'); ?>
                                    </h4>
                                </div>
                            </div>
                            <div>
                                <table class="table table-bordered table-striped table-sm">
                                    <tbody>
                                        <tr>
                                            <td width="50%" style="text-align: center;">
                                                TAHAP 1 <br>
                                                <h4 class="text-success">
                                                    Rp. <?php echo number_format(($value['total_cost'] * 0.4),0,',','.'); ?>
                                                </h4>
                                            </td>
                                            <td width="50%" style="text-align: center;">
                                                TAHAP 2 <br>
                                                <h4 class="text-success">
                                                    Rp. <?php echo number_format(($value['total_cost'] * 0.6),0,',','.'); ?>
                                                </h4>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>Persen</td>
                                                        <td>:</td>
                                                        <td><?php echo $value['step1_percentage']; ?>%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status</td>
                                                        <td>:</td>
                                                        <td>
                                                            <label class="label label-<?php echo parsing_status_class()[$value['step1_status']]; ?>">
                                                                <?php echo parsing_status()[$value['step1_status']]; ?>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td>Persen</td>
                                                        <td>:</td>
                                                        <td><?php echo $value['step2_percentage']; ?>%</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status</td>
                                                        <td>:</td>
                                                        <td>
                                                            <label class="label label-<?php echo parsing_status_class()[$value['step2_status']]; ?>">
                                                                <?php echo parsing_status()[$value['step2_status']]; ?>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        <!-- </div> -->
                    </div>
                <?php } ?>
            </div> 
        </div>
    <?php } ?>
    </div>
</div>
<br>
<script>
    $( document ).ready(function() {
        $('#kec_id').change(function () {
            var id = $(this).val();

            $.ajax({
                dataType: "json",
                url: '<?php echo base_url("adminsamisade/chart/get_all_desa_by_kecamatan"); ?>/' + id,
                success: function (data) {
                    $("#desa_id").empty(); 
                    $("#desa_id").append("<option value=''> --- Pilih --- </option>");
                    $(data).each(function(i) {
                        $("#desa_id").append("<option value=\"" + data[i].id + "-" + data[i].name + "\">" + data[i].name + "</option>");
                    });
                }
            });
        });
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

    var ctx = document.getElementById('chart1').getContext('2d');
    var chart1 = new Chart(ctx, {
        type: 'horizontalBar',
        data: <?php echo $get_chart_activities; ?>,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes:[
                    {
                    ticks:{
                        beginAtZero:true
                    }
                }]
            },
        }
    });
</script>