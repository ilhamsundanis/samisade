<section class="page-title-area" style="background-image: url(<?php echo base_url(); ?>assets/images/page-title-bg1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="page-title text-center">
                    <h1>DETAIL LAPORAN KEGIATAN</h1>
                    <div class="breadcrumb">
                        <ul class="breadcrumb-list">
                            <li><a href="index-2.html">HOME</a></li>
                            <li><a class="active" href="#">DETAIL LAPORAN KEGIATAN</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="news-area-02 pt-40 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="blog blogs mb-40">
                    <div class="blog__content">
                        <h3 class="blog-title mb-15"><a href="#"><?php echo $data['title']; ?></a></h3>
                        <div class="blog__content--meta mb-15">
                            <span><i class="fal fa-user"></i> <?php echo $data['user_name']; ?></span>
                            <span><i class="far fa-calendar-alt"></i> <?php echo $data['created_at']; ?> </span>
                        </div>
                        <img src="<?php echo $data['image1']; ?>" width="100%" alt="">
                        <hr>
                        <div style="text-align: justify;">
                            <?php
                                echo $data['description'];
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="widget mb-40">
			<div class="widget-title-box mb-30">
				<span class="animate-border"></span>
				<h6 class="widget-title">Dokumentasi Realisasi</h6>
			</div>
			<div class="row">
				<?php if ( !empty($data['image2']) ) { ?>
					<div class="col-md-3">
						<a class="insta-post" href="<?php echo $data['image2']; ?>" data-fancybox><img src="<?php echo $data['image2']; ?>" alt="" width="100%"></a>
					</div>
				<?php } ?>
				<?php if ( !empty($data['image3']) ) { ?>
					<div class="col-md-3">
						<a class="insta-post" href="<?php echo $data['image3']; ?>" data-fancybox><img src="<?php echo $data['image3']; ?>" alt="" width="100%"></a>
					</div>
				<?php } ?>
				<?php if ( !empty($data['image4']) ) { ?>
					<div class="col-md-3">
						<a class="insta-post" href="<?php echo $data['image4']; ?>" data-fancybox><img src="<?php echo $data['image4']; ?>" alt="" width="100%"></a>
					</div>
				<?php } ?>
				<?php if ( !empty($data['image5']) ) { ?>
					<div class="col-md-3">
						<a class="insta-post" href="<?php echo $data['image5']; ?>" data-fancybox><img src="<?php echo $data['image5']; ?>" alt="" width="100%"></a>
					</div>				
				</div>
				<?php } ?>
		</div>
    </div>
</section>