<section class="page-title-area" style="background-image: url(<?php echo base_url(); ?>assets/images/page-title-bg1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="page-title text-center">
                    <h1>LAPORAN KEGIATAN</h1>
                    <div class="breadcrumb">
                        <ul class="breadcrumb-list">
                            <li><a href="index-2.html">HOME</a></li>
                            <li><a class="active" href="#">LAPORAN KEGIATAN</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="news-area-02 pt-40 pb-100">
    <div class="container">
        <div class="row">
        <?php
            foreach ($data as $value) {
        ?>
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="blog">
                    <div class="blog__img mb-50">
                        <img src="<?php echo $value['image1']; ?>" alt="">
                    </div>
                    <div class="blog__content">
                        <div class="blog__content--meta mb-15 text-right;">
                            <span><i class="far fa-calendar-alt"></i> <?php echo $value['created_at']; ?></span>
                            <span><i class="far fa-user"></i> <?php echo $value['user_name']; ?></span>
                        </div>
                        <h4 class="mb-15">
                            <a href="<?php echo base_url(); ?>report/detail/<?php echo $value['id']; ?>">
                                <?php echo $value['title']; ?>
                            </a>
                        </h4>
                        <div class="blog__content--list justify-content-between text-right">
                            <a class="more_btn4" href="<?php echo base_url(); ?>report/detail/<?php echo $value['id']; ?>"><i class="far fa-arrow-right"></i> Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
            <?php 
                echo $this->pagination->create_links();
            ?>
    </div>
</section>