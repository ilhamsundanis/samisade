</main>
<!--footer-area start-->
<footer id="top-menu" class="footer-area pos-rel black-bg-soft pt-100">
  <div class="container">
      <div class="row mb-40">
          <div class="col-xl-4 col-lg-6 col-md-12">
              <div class="footer__widget footer__widget__02 mb-30">
                  <h6 class="fot-title mb-30">Tentang Kita</h6>
                  <p class="mb-15" style="text-align: justify;"><?php echo load_company()['description']; ?></p>
              </div>
          </div>
          <div class="col-xl-4 col-lg-6 col-md-6">
              <div class="footer__widget mb-25">
                  <div class="footer__widget--box" style="background-image: url(<?php echo base_url(); ?>assets/images/fot-bg-box1.png);">
                      <div class="logo-area mb-15">
                          <a href="<?php echo base_url(); ?>" class="footer-logo mb-30"><img src="<?php echo load_company()['logo']; ?>" alt="" width="100%"></a>
                      </div>
                      <ul class="box-list">
                          <li><i class="fal fa-envelope"></i> <?php echo load_company()['email']; ?></li>
                          <li><i class="fal fa-phone"></i> <?php echo load_company()['telephone']; ?></li>
                          <li><i class="fal fa-map-marker-alt"></i> <?php echo load_company()['address']; ?></li>
                      </ul>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 col-lg-6 col-md-6">
              <div class="footer__widget mb-30">
                  <h6 class="fot-title mb-30">Social Media</h6>
                  <div class="foter__social mt-10">
                      <a href="#"><i class="fab fa-facebook-f"></i></a>
                      <a href="#"><i class="fab fa-twitter"></i></a>
                      <a href="#"><i class="fab fa-instagram"></i></a>
                      <a href="#"><i class="fab fa-google"></i></a>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="footer-copy-right black-bg-soft2 pt-25 pb-25">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="copy-right-area-02 text-center">
                      <span>Copyright By <a href="#"><?php echo load_company()['name']; ?></a> - 2021</span>
                  </div>
              </div>
          </div>
      </div>
  </div>
</footer>
<!--footer-area end-->

<!-- JS here -->
    <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/one-page-nav-min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.meanmenu.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
    <!-- <script src="<?php // echo base_url(); ?>assets/js/jquery.nice-select.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/js/ajax-form.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.easypiechart.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/tilt.jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>

    <script src="<?php echo base_url(); ?>js/adminsamisade/fancybox/jquery.fancybox.min.js"></script>
</body>


<!-- Mirrored from www.devsnews.com/template/govtpress/govtpress/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 23 Jun 2021 04:43:41 GMT -->
</html>