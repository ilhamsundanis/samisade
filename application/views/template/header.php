<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo load_company()['name']; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>images/logo/logo.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/metisMenu.css">
    <!-- <link rel="stylesheet" href="<?php //echo base_url(); ?>assets/css/nice-select.css"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    
    <link href="<?php echo base_url(); ?>css/adminsamisade/fancybox/jquery.fancybox.min.css" rel="stylesheet" />

    <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-3.5.1.min.js"></script>

    <style>
      .page-active {
        border: 0;
        color: #ffffff;
        font-size: 16px;
        font-family: "Roboto", sans-serif;
        font-weight: 400;
        background: #01a687;
        display: inline-block;
        padding: 20px 22px;
        margin-right: 10px;
        -webkit-transition: all 0.4s ease-out 0s;
        -moz-transition: all 0.4s ease-out 0s;
        -ms-transition: all 0.4s ease-out 0s;
        -o-transition: all 0.4s ease-out 0s;
        transition: all 0.4s ease-out 0s;
      }

      .form-control {
        width: 100% !important;
      }

      .text-white {
        color: white !important;
      }

      .label {
        padding: 5px;
      }

      .label-info {
        background-color: #17a2b8;
        color: white;
      }

      .label-success {
        background-color: #01a687;
        color: white;
      }

      .label-danger {
        background-color: #dc3646;
        color: white;
      }

      .events-wrapper .nav-tabs .nav-link-custom {
        color: black;
        font-size: 18px;
        line-height: 1;
        font-weight: 500;
        background: #f6f6f6;
        padding: 10;
        display: inline-block;
        border-bottom: 2px solid transparent;
        transition: 0.3s;
      }

      .events-wrapper .nav-tabs .nav-link-custom.active {
        background: #01a687!important;
        color: white;
        border-color: #01a687;
      }

      .table-custom {
        font-family: Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      .table-custom td, .table-custom th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      .table-custom tr:nth-child(even){background-color: #f2f2f2;}

      .table-custom tr:hover {background-color: #ddd;}

      .table-custom th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
      }
    </style>
</head>

<body>
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->