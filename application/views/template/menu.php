<!-- header-area start -->
<header class="head-one">
    <div class="main-header-area main-header-04 heding-bg">
        <div class="container">
            <div class="row no-gutters align-items-center">
                <div class="col-xl-2 col-lg-3 col-md-6 col-6">
                    <div class="logo">
                        <a class="logo-img" href="<?php echo base_url(); ?>"><img src="<?php echo load_company()['logo']; ?>" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-7 d-none d-lg-block">
                    <div class="main-menu main-menu-04 d-none d-lg-block text-right">
                        <nav>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url(); ?>home">HOME</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>news">BERITA</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>report">LAPORAN KEGIATAN</a>
                                </li>
                                <li>
                                    <a href="#">REALISASI</a>
                                    <ul class="submenu">
                                        <li><a href="<?php echo base_url(); ?>realization">PEMBANGUNAN</a></li>
                                        <li><a href="<?php echo base_url(); ?>disbursement_date">TANGGAL PENCAIRAN DANA</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">GRAFIK</a>
                                    <ul class="submenu">
                                        <li><a href="<?php echo base_url(); ?>chart/index/total-anggaran">TOTAL ANGGARAN</a></li>
                                        <li><a href="<?php echo base_url(); ?>chart/index/jumlah-dana">JUMLAH DANA TEREALISASI</a></li>
                                        <li><a href="<?php echo base_url(); ?>chart/index/persentase-pembangunan">PERSENTASE PEMBANGUNAN</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>complaint">ADUAN MASYARAKAT</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-6 text-right">
                    <div class="hamburger-menu d-md-block d-lg-none">
                        <a href="javascript:void(0);">
                            <i class="far fa-bars"></i>
                        </a>
                    </div>
                    <div class="main-header-right-one d-none d-lg-block">
                        <div class="donate">
                            <a href="<?php echo base_url(); ?>adminsamisade/auth" class="theme_btn theme_btn3">Login <i class="fal fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-area end -->

<aside class="slide-bar">
    <div class="close-mobile-menu">
        <a href="javascript:void(0);"><i class="fas fa-times"></i></a>
    </div>

    <!-- side-mobile-menu start -->
    <nav class="side-mobile-menu">
        <ul id="mobile-menu-active" class="metismenu">
            <li>
                <a href="<?php echo base_url(); ?>home">HOME</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>news">BERITA</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>report">LAPORAN KEGIATAN</a>
            </li>
            <li class="has-dropdown">
                <a href="#">REALISASI</a>
                <ul class="sub-menu mm-collapse">
                    <li><a href="<?php echo base_url(); ?>realization">PEMBANGUNAN</a></li>
                    <li><a href="<?php echo base_url(); ?>disbursement_date">TANGGAL PENCAIRAN DANA</a></li>
                </ul>
            </li>
            <li class="has-dropdown">
                <a href="#">GRAFIK</a>
                <ul class="sub-menu mm-collapse">
                    <li><a href="<?php echo base_url(); ?>chart/index/total-anggaran">TOTAL ANGGARAN</a></li>
                    <li><a href="<?php echo base_url(); ?>chart/index/jumlah-dana">JUMLAH DANA TEREALISASI</a></li>
                    <li><a href="<?php echo base_url(); ?>chart/index/persentase-pembangunan">PERSENTASE PEMBANGUNAN</a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>complaint">ADUAN MASYARAKAT</a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>adminsamisade/auth">LOGIN</a>
            </li>
        </ul>
    </nav>
    <!-- side-mobile-menu end -->
</aside>
<main>