<!-- Breadcrumb Area -->
<section class="breadcrumb-area text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>404</h2>
                <ul class="list-unstyled list-inline">
                    <li class="list-inline-item"><a href="#">Home</a></li>
                    <li class="list-inline-item"><i class="fa fa-long-arrow-right"></i>404</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- End Breadcrumb Area -->

<!-- 404 Area -->
<div class="error-page text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-box">
                    <h1>404</h1>
                    <h3>Page Not Found</h3>
                    <p>Ooops! The page you are looking for, couldn't be found.</p>
                    <a href="<?php echo base_url(); ?>home"><i class="fa fa-home"></i>Back To Homepage</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End 404 Area -->