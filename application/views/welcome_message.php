<!doctype html>
<html class="no-js" lang="zxx">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>DPMD Kabupaten Bogor</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>images/favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800" rel="stylesheet">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/bootstrap.min.css">

        <!-- Fontawesome Icon -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/font-awesome.min.css">

        <!-- Animate Css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/animate.css">

        <!-- Owl Slider -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/owl.carousel.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/tempusdominus-bootstrap-4.min.css">

        <!-- Magnific PopUp -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/magnific-popup.css">

        <!-- Custom Style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/responsive.css">

    </head>
    <body>

        <!-- Preloader -->
        <div id="preloader">
            <div class="pr-circle">
                <div class="pr-circle1 pr-child"></div>
                <div class="pr-circle2 pr-child"></div>
                <div class="pr-circle3 pr-child"></div>
                <div class="pr-circle4 pr-child"></div>
                <div class="pr-circle5 pr-child"></div>
                <div class="pr-circle6 pr-child"></div>
                <div class="pr-circle7 pr-child"></div>
                <div class="pr-circle8 pr-child"></div>
                <div class="pr-circle9 pr-child"></div>
                <div class="pr-circle10 pr-child"></div>
                <div class="pr-circle11 pr-child"></div>
                <div class="pr-circle12 pr-child"></div>
            </div>
        </div>
        <!-- End Preloader -->

        <!-- Logo Area -->
        <section class="logo-area menu-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-md-3" style="margin-top: -12px;">
                        <div class="logo" style="background-color: #00a687; border-radius: 5px;">
                            <a href="#"><img src="<?php echo $data['company']['logo']; ?>" height="70px;" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="main-menu text-right">
                            <ul class="list-unstyled list-inline">
                                <li class="list-inline-item active"><a>BERANDA</a></li>
                                <li class="list-inline-item"><a>PROFILE <i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown list-unstyled">
                                        <li><a href="06-service-one.html">Sejarah dan Selayang Pandang</a></li>
                                        <li><a href="07-service-two.html">Visi dan Misi</a></li>
                                        <li><a href="08-service-three.html">Tugas Pokok dan Fungsi</a></li>
                                        <li><a href="09-service-details.html">Struktur Organisasi</a></li>
                                        <li><a href="09-service-details.html">Kepala Dinas</a></li>
                                        <li><a href="09-service-details.html">Jabatan Struktural</a></li>
                                        <li><a href="09-service-details.html">Jabatan Fungsional</a></li>
                                    </ul>
                                </li>
                                <li class="list-inline-item"><a>OVOC</a></li>
                                <li class="list-inline-item"><a>CERITA DESA</a></li>
                                <li class="list-inline-item"><a>KOLABORASI</a></li>
                                <li class="list-inline-item"><a>INFORMASI</a></li>
                                <li class="list-inline-item msearch-bar">
                                    <form action="#">
                                        <input type="text" name="search" placeholder="Search Here">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Logo Area -->

        <!-- Mobile Menu -->
        <section class="mobile-menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <!-- <a href="#"><img src="<?php //echo base_url(); ?>images/logo.png" alt=""></a> -->
                                <a href="#" style="font-size: 24px; color:black;"><?php echo $data['company']['name_app']; ?></a>
                                <ul class="list-unstyled">
                                    <li><a>BERANDA</a></li>
                                    <li><a>PROFILE</a>
                                        <ul class="list-unstyled">
                                            <li><a href="06-service-one.html">Sejarah dan Selayang Pandang</a></li>
                                            <li><a href="07-service-two.html">Visi dan Misi</a></li>
                                            <li><a href="08-service-three.html">Tugas Pokok dan Fungsi</a></li>
                                            <li><a href="09-service-details.html">Struktur Organisasi</a></li>
                                            <li><a href="09-service-details.html">Kepala Dinas</a></li>
                                            <li><a href="09-service-details.html">Jabatan Struktural</a></li>
                                            <li><a href="09-service-details.html">Jabatan Fungsional</a></li>
                                        </ul>
                                    </li>
                                    <li><a>OVOC</a></li>
                                    <li><a>CERITA DESA</a></li>
                                    <li><a>KOLABORASI</a></li>
                                    <li><a>INFORMASI</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Mobile Menu -->

        <!-- Slider Area -->
        <style>
            <?php 
                foreach ($data['banner'] as $value) {
            ?>
                .home-slider .slider-wrapper .slider<?php echo $value['id']; ?> {
                    background-image: url("<?php echo $value['image']; ?>");
                    background-size: cover;
                    background-position: center;
                    background-repeat: no-repeat;
                }
            <?php
                }
            ?>

            .home-slider img {
                width: 32px;
            }
        </style>
        <section class="home-slider">
            <div class="slider-wrapper owl-carousel">
            <?php 
                $no = 0;
                $classBanner = "";
                foreach ($data['banner'] as $value) {
                    $no++;
                    if ( $no % 2 == 0 ) {
                        $classBanner = "col-lg-8 offset-lg-4 col-md-12 text-right";
                    } else {
                        $classBanner = "col-lg-8 col-md-12";
                    }

            ?>
                <div class="slider-item slider<?php echo $value['id']?>">
                    <div class="slider-table">
                        <div class="slider-tablecell">
                            <div class="container">
                                <div class="row">
                                    <div class="<?php echo $classBanner; ?>">
                                        <div class="slider-heading wow fadeInDown" data-wow-duration="1.5s" data-wow-delay="0.2s">
                                            <p><?php echo $value['title']; ?></p>
                                        </div>
                                        <div class="slider-para wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.2s" style="margin-top: -40px;">
                                            <h1><?php echo $value['description']; ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
                }
            ?>
            </div>
        </section>
        <!-- End Slider Area -->

        <section class="service-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner-boxes">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="banner-box d-flex">
                                        <div class="box-icon">
                                            <img src="images/service-1.png" alt="">
                                        </div>
                                        <div class="box-content">
                                            <h6>Professional Doctors</h6>
                                            <p>Lorem ipsum dolor sit amet, consectet commodi sit veniam adipisicing.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="banner-box d-flex">
                                        <div class="box-icon">
                                            <img src="images/service-2.png" alt="">
                                        </div>
                                        <div class="box-content">
                                            <h6>Emergency Care</h6>
                                            <p>Lorem ipsum dolor sit amet, consectet commodi sit veniam adipisicing.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="banner-box d-flex">
                                        <div class="box-icon">
                                            <img src="images/service-4.png" alt="">
                                        </div>
                                        <div class="box-content">
                                            <h6>24 Hours Support</h6>
                                            <p>Lorem ipsum dolor sit amet, consectet commodi sit veniam adipisicing.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- welcome Area -->
        <section class="welcome-area">
            <div class="container">
                <div class="row" style="margin-top:50px;">
                    <div class="col-md-6">
                        <div class="welcome-box">
                            <h4><?php echo $data['company']['name']; ?></h4>
                            <img src="#" alt="">
                            <p><?php echo $data['company']['description']?></p>
                            <a href="#">Lihat</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about-image">
                            <img src="<?php echo $data['company']['image']; ?>" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End welcome Area -->

        <!-- Gallery Area -->
        <section class="gallery" style="background-color:#f3f3f3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="gallery-top text-center">
                            <h4>Cerita Desa</h4>
                            <img src="#" alt="">
                            <p>Mengedepankan Cerita Masyarakat Desa.</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <ul class="gallery-filter list-unstyled list-inline text-center">
                            <li class="list-inline-item active" data-filter="*">Lihat Cerita Desa</li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="gallery-items row">
                            <?php
                                foreach ( $data['village_story'] as $value ) {
                            ?>
                                    <div class="col-lg-3 col-md-4 grid-item car">
                                        <div class="gallery-content">
                                            <img src="<?php echo $value['image']; ?>" alt="" class="img-fluid" style="height: 180px !important;">
                                            <div class="fc-icon text-center">
                                                <a href="<?php echo $value['image']; ?>"><img src="<?php echo base_url(); ?>images/focus.png" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Gallery Area -->

        <!-- News & Events -->
        <section class="news" style="padding-bottom:0;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="news-top text-center">
                            <h4>Berita Terkini</h4>
                            <img src="#" alt="">
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint eius inventore magni quod voluptate, molestiae eos odio illo.</p> -->
                        </div>
                    </div>
                    <?php 
                        foreach ( $data['news'] as $value ) {
                    ?>
                        <div class="col-md-4" style="margin-bottom: 40px;">
                            <div class="news-box">
                                <div class="news-image">
                                    <a href="#"><img src="<?php echo $value['image']; ?>" alt="" class="img-fluid" style="height: 200px !important;"></a>
                                </div>
                                <div class="news-heading d-flex">
                                    <div class="news-date text-center">
                                        <p>
                                            <?php echo date_format(date_create($value['created_at']), "d")?><br> 
                                            <?php echo date_format(date_create($value['created_at']), "M")?></p>
                                    </div>
                                    <div class="heading-box">
                                        <h6><a href="#"><?php echo $value['title']; ?>.</a></h6>
                                        <ul class="list-unstyled list-inline">
                                            <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="#"><?php echo $value['name_user']; ?></a></li>
                                            <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="#">(<span>23</span>)</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="news-content">
                                    <p><?php echo substr($value['partial_description'],0, 250); ?> ... <a href="#">Baca Selengkapnya</a></p>
                                </div>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                    <!-- <div class="col-md-4" style="margin-bottom: 40px;">
                        <div class="news-box">
                            <div class="news-image">
                                <a href="#"><img src="<?php echo base_url(); ?>images/news-2.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="news-heading d-flex">
                                <div class="news-date text-center">
                                    <p>11<br> Apr</p>
                                </div>
                                <div class="heading-box">
                                    <h6><a href="#">Sint eius inventore magni quod.</a></h6>
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="#">Jhon Doe</a></li>
                                        <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="#">(<span>23</span>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="news-content">
                                <p>Lorem ipsum dolor sit amet, consectetured adipisicing elit. Cupiditate et distinctioned. Facere aut voluptas optio toi expedita ...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 40px;">
                        <div class="news-box">
                            <div class="news-image">
                                <a href="#"><img src="<?php echo base_url(); ?>images/news-3.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="news-heading d-flex">
                                <div class="news-date text-center">
                                    <p>13<br> Apr</p>
                                </div>
                                <div class="heading-box">
                                    <h6><a href="#">Sint eius inventore magni quod.</a></h6>
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="#">Jhon Doe</a></li>
                                        <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="#">(<span>23</span>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="news-content">
                                <p>Lorem ipsum dolor sit amet, consectetured adipisicing elit. Cupiditate et distinctioned. Facere aut voluptas optio toi expedita ...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 40px;">
                        <div class="news-box">
                            <div class="news-image">
                                <a href="#"><img src="<?php echo base_url(); ?>images/news-1.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="news-heading d-flex">
                                <div class="news-date text-center">
                                    <p>25<br> Mar</p>
                                </div>
                                <div class="heading-box">
                                    <h6><a href="#">Sint eius inventore magni quod.</a></h6>
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="#">Jhon Doe</a></li>
                                        <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="#">(<span>23</span>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="news-content">
                                <p>Lorem ipsum dolor sit amet, consectetured adipisicing elit. Cupiditate et distinctioned. Facere aut voluptas optio toi expedita ...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 40px;">
                        <div class="news-box">
                            <div class="news-image">
                                <a href="#"><img src="<?php echo base_url(); ?>images/news-2.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="news-heading d-flex">
                                <div class="news-date text-center">
                                    <p>11<br> Apr</p>
                                </div>
                                <div class="heading-box">
                                    <h6><a href="#">Sint eius inventore magni quod.</a></h6>
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="#">Jhon Doe</a></li>
                                        <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="#">(<span>23</span>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="news-content">
                                <p>Lorem ipsum dolor sit amet, consectetured adipisicing elit. Cupiditate et distinctioned. Facere aut voluptas optio toi expedita ...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 40px;">
                        <div class="news-box">
                            <div class="news-image">
                                <a href="#"><img src="<?php echo base_url(); ?>images/news-3.jpg" alt="" class="img-fluid"></a>
                            </div>
                            <div class="news-heading d-flex">
                                <div class="news-date text-center">
                                    <p>13<br> Apr</p>
                                </div>
                                <div class="heading-box">
                                    <h6><a href="#">Sint eius inventore magni quod.</a></h6>
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item"><i class="fa fa-user-o"></i><a href="#">Jhon Doe</a></li>
                                        <li class="list-inline-item"><i class="fa fa-commenting-o"></i><a href="#">(<span>23</span>)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="news-content">
                                <p>Lorem ipsum dolor sit amet, consectetured adipisicing elit. Cupiditate et distinctioned. Facere aut voluptas optio toi expedita ...</p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <!-- End News & Events -->

        <!-- Service Three -->
        <section class="service-three" style="background-color:#f3f3f3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="service-top text-center">
                            <h4>LINK TERKAIT</h4>
                            <img src="#" alt="">
                            <br>
                        </div>
                    </div>
                    <?php
                        foreach ( $data['related_link'] as $value ) {
                    ?>
                        <div class="col-lg-4 col-md-6">
                            <a href="<?php echo $value['url']; ?>" target="_blank">
                                <div class="service-box text-center box">
                                    <img src="<?php echo $value['logo']; ?>" alt="" height="60px;" style="margin-bottom:20px;">
                                    <p><?php echo $value['name']; ?></p>
                                </div>
                            </a>
                        </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </section>
        <!-- End Service Three -->

        <!-- Contact Area -->
        <section class="contact">
            <iframe src="<?php echo $data['company']['map']; ?>" height="450" width="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe>
        </section>
        <!-- End Contact Area -->

        <!-- Footer Section -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="findus">
                            <h4><?php echo $data['company']['name']; ?></h4>
                            <p><?php echo $data['company']['description'];?></p>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-map-marker"></i><?php echo $data['company']['address']; ?></li>
                                <li><i class="fa fa-envelope"></i><?php echo $data['company']['email']; ?></li>
                                <li><i class="fa fa-phone"></i><?php echo $data['company']['telephone']; ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="f-menu text-center">
                            <p>Copyright &copy; 2020 by <a href="#" target="_blank"><?php echo $data['company']['name']; ?></a></p>
                            <ul class="social list-unstyled list-inline">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-youtube"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer Section -->

        <!-- =========================================
        JavaScript Files
        ========================================== -->

        <!-- jQuery JS -->
        <script src="<?php echo base_url(); ?>js/assets/vendor/jquery-1.12.4.min.js"></script>

        <!-- Bootstrap -->
        <script src="<?php echo base_url(); ?>js/assets/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>js/assets/bootstrap.min.js"></script>

        <!-- Wow Animation -->
        <script src="<?php echo base_url(); ?>js/assets/wow.min.js"></script>

        <!-- Owl Slider -->
        <script src="<?php echo base_url(); ?>js/assets/owl.carousel.min.js"></script>

        <!-- Date Picker -->
        <script src="<?php echo base_url(); ?>js/assets/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>js/assets/tempusdominus-bootstrap-4.min.js"></script>

        <!-- Isotope -->
        <script src="<?php echo base_url(); ?>js/assets/isotope.pkgd.min.js"></script>

        <!-- Magnific PopUp -->
        <script src="<?php echo base_url(); ?>js/assets/magnific-popup.min.js"></script>

        <!-- Counter Up -->
        <script src="<?php echo base_url(); ?>js/assets/counterup.min.js"></script>
        <script src="<?php echo base_url(); ?>js/assets/waypoints.min.js"></script>

        <!-- Smooth Scroll -->
        <script src="<?php echo base_url(); ?>js/assets/smooth-scroll.js"></script>

        <!-- Syotimer -->
        <script src="<?php echo base_url(); ?>js/assets/jquery.syotimer.min.js"></script>

        <!-- Mean Menu -->
        <script src="<?php echo base_url(); ?>js/assets/jquery.meanmenu.min.js"></script>

        <!-- Form Validation -->
        <script src="<?php echo base_url(); ?>js/assets/form.js"></script>

        <!-- Custom JS -->
        <script src="<?php echo base_url(); ?>js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>js/custom.js"></script>

    </body>
</html>
