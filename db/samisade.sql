-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 10, 2021 at 04:42 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `samisade`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `type` varchar(225) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `name`, `type`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Jalan Desa atau Lingkungan', 'maps_direction', 1, '2021-01-25 13:08:47', NULL),
(2, 'MCK', 'maps', 1, '2021-02-07 14:14:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `desa_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `type` varchar(225) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `desa_id`, `name`, `type`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 88, 'Alat', 'description', 1, '2021-01-25 12:41:01', NULL),
(2, 88, 'Biaya Tenaga Ahli', 'description', 1, '2021-01-25 12:43:26', NULL),
(3, 88, 'Bahan', 'description', 1, '2021-02-08 11:33:19', NULL),
(4, 88, 'Upah', 'description', 1, '2021-02-08 11:33:19', NULL),
(5, 88, 'Biaya Operasional', 'description', 1, '2021-02-08 11:33:33', NULL),
(6, 34, 'Alat', 'description', 1, '2021-01-25 05:41:01', NULL),
(7, 34, 'Biaya Tenaga Ahli', 'description', 1, '2021-01-25 05:43:26', NULL),
(8, 34, 'Bahan', 'description', 1, '2021-02-08 04:33:19', NULL),
(9, 34, 'Upah', 'description', 1, '2021-02-08 04:33:19', NULL),
(10, 34, 'Biaya Operasional', 'description', 1, '2021-02-08 04:33:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `kec_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`id`, `name`, `kec_id`) VALUES
(1, 'CIBADAK', 1),
(2, 'TANJUNGRASA', 1),
(3, 'BABAKAN', 2),
(4, 'CIOMAS', 2),
(5, 'CARIU', 3),
(6, 'BANTARKUNING', 3),
(7, 'SUKAJADI', 3),
(8, 'CIMANDEHILIR', 4),
(9, 'PANCAWATI', 4),
(10, 'MUARAJAYA', 4),
(11, 'CIBURAYUT', 5),
(12, 'LIMUSNUNGGAL', 6),
(13, 'CILEUNGSI KIDUL', 6),
(14, 'WANAHERANG', 7),
(15, 'BOJONG KULUR', 7),
(16, 'TLAJUNG UDIK', 7),
(17, 'PUSPASARI', 8),
(18, 'CITEUREUP', 8),
(19, 'SANJA', 8),
(20, 'CIJAYANTI', 9),
(21, 'SUMURBATU', 9),
(22, 'CIPAMBUAN', 9),
(23, 'JUGALAJAYA', 10),
(24, 'SIPAK', 10),
(25, 'BANYUASIH', 11),
(26, 'BANYUWANGI', 11),
(27, 'MEKARJAYA', 11),
(28, 'NANGGERANG', 12),
(29, 'CIBALUNG', 13),
(30, 'CIPELANG', 13),
(31, 'TANJUNGSARI', 13),
(32, 'PARAKAN', 14),
(33, 'SUKAHARJA', 14),
(34, 'SUKAMAJU', 15),
(35, 'SITU UDIK', 15),
(36, 'CIBATOK I', 15),
(37, 'GIRIMULYA', 15),
(38, 'CIARUTEUN UDIK', 15),
(39, 'BOJONGGEDE', 16),
(40, 'WARINGIN JAYA', 16),
(41, 'RAWAPANJANG', 16),
(42, 'IWUL', 17),
(43, 'PASAREAN', 18),
(44, 'GUNUNG MENYAN', 18),
(45, 'HARKATJAYA', 19),
(46, 'SUKAMULIH', 19),
(47, 'CILEUKSA', 19),
(48, 'CITEKO', 20),
(49, 'KLAPANUNGGAL', 21),
(50, 'BANTARJATI', 21),
(51, 'CIKAHURIPAN', 21),
(52, 'BANJARWARU', 22),
(53, 'BANJARWANGI', 22),
(54, 'CIBEDUG', 22),
(55, 'BOJONGMURNI', 22),
(56, 'BITUNGSARI', 22),
(57, 'CILEUNGSI', 22),
(58, 'SITU DAUN', 23),
(59, 'GUNUNG MALANG', 23),
(60, 'SUKAWANGI', 24),
(61, 'SUKADAMAI', 24),
(62, 'SIRNAJAYA', 24),
(63, 'PABUARAN', 24),
(64, 'CIMANDALA', 25),
(65, 'GUNUNGGEULIS', 25),
(66, 'PARAKANJAYA', 26),
(67, 'SUKAJADI', 27),
(68, 'SUKAMANTRI', 27),
(69, 'SUKARESMI', 27),
(70, 'JAMPANG', 28),
(71, 'PENGASINAN', 28),
(72, 'PEDURENAN', 28),
(73, 'CIDOKOM', 28),
(74, 'CIBADAK', 29),
(75, 'CIAMPEA', 29),
(76, 'BENTENG', 29),
(77, 'BENDUNGAN', 30),
(78, 'MEGAMENDUNG', 31),
(79, 'GADOG', 31),
(80, 'SUKAKARYA', 31),
(81, 'CIBEBER II', 32),
(82, 'KALONG II', 33),
(83, 'SADENG', 33),
(84, 'WANGUN JAYA', 33),
(85, 'PURWASARI', 34),
(86, 'SUKADAMAI', 34),
(87, 'CIHERANG', 34),
(88, 'NEGLASARI', 34);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id`, `name`) VALUES
(1, 'TANJUNGSARI'),
(2, 'TENJO'),
(3, 'CARIU'),
(4, 'CARINGIN'),
(5, 'CIGOMBONG'),
(6, 'CILEUNGSI'),
(7, 'GUNUNG PUTRI'),
(8, 'CITEUREUP'),
(9, 'BABAKAN MADANG'),
(10, 'JASINGA'),
(11, 'CIGUDEG'),
(12, 'TAJUR HALANG'),
(13, 'CIJERUK'),
(14, 'CIOMAS'),
(15, 'CIBUNGBULANG'),
(16, 'BOJONGGEDE'),
(17, 'PARUNG'),
(18, 'PAMIJAHAN'),
(19, 'SUKAJAYA'),
(20, 'CISARUA'),
(21, 'KLAPANUNGGAL'),
(22, 'CIAWI'),
(23, 'TENJOLAYA'),
(24, 'SUKAMAKMUR'),
(25, 'SUKARAJA'),
(26, 'KEMANG'),
(27, 'TAMANSARI'),
(28, 'GUNUNGSINDUR'),
(29, 'CIAMPEA'),
(30, 'JONGGOL'),
(31, 'MEGAMENDUNG'),
(32, 'LEUWILIANG'),
(33, 'LEUWISADENG'),
(34, 'DRAMAGA');

-- --------------------------------------------------------

--
-- Table structure for table `proposal`
--

CREATE TABLE `proposal` (
  `id` int(11) NOT NULL,
  `desa_id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `surat_permohonan` longtext NOT NULL,
  `kata_pengantar` longtext NOT NULL,
  `daftar_isi` longtext NOT NULL,
  `latar_belakang` longtext NOT NULL,
  `dasar_hukum` longtext NOT NULL,
  `maksud_tujuan` longtext NOT NULL,
  `gambaran_umum_desa` longtext NOT NULL,
  `potensi_desa` longtext NOT NULL,
  `program_kegiatan` longtext NOT NULL,
  `kesimpulan` longtext NOT NULL,
  `saran` longtext NOT NULL,
  `file` text NOT NULL,
  `status` varchar(225) NOT NULL COMMENT 'pending,verification,verified,done',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proposal`
--

INSERT INTO `proposal` (`id`, `desa_id`, `title`, `surat_permohonan`, `kata_pengantar`, `daftar_isi`, `latar_belakang`, `dasar_hukum`, `maksud_tujuan`, `gambaran_umum_desa`, `potensi_desa`, `program_kegiatan`, `kesimpulan`, `saran`, `file`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 88, 'PENGAJUAN PERBAIKAN JALAN DESA TELUKPINANG', '<p>jaksdlkj asklkdjkalda</p>\r\n', '<p>as dasda</p>\r\n', '<p>a das d</p>\r\n', '<p>asd asd</p>\r\n', '<p>as dasd</p>\r\n', '<p>a sdasd&nbsp;</p>\r\n', '<p>a sda s</p>\r\n', '<p>a da sd</p>\r\n', '<p>a da d</p>\r\n', '<p>ads ads&nbsp;</p>\r\n', '<p>&nbsp;asdas das</p>\r\n', '', 'pending', 1, '2021-02-06 09:56:35', NULL),
(3, 34, 'Akses Terbuka Bagi Warga Penyandang Disabilitas di Sidamulih dan Bangunsari', '<p>aklsdjaklsjdklakjs kjkaljsdlkajskdjklj</p>\r\n', '<p>ajdlksjalk djalkjsdklajl;d</p>\r\n', '<p>aslkdj akljsd lakjsdl</p>\r\n', '<p>kakj dlakjskld jaklj</p>\r\n', '<p>akjkdl kajskldj aklj</p>\r\n', '<p>ajkdlkjakslkjdalk j</p>\r\n', '<p>akjdl kajsldk jakl</p>\r\n', '<p>akjksd klajksdl kja</p>\r\n', '<p>ajksdkl jkalskkjd&nbsp;</p>\r\n', '<p>ajsdkl akjkldj l</p>\r\n', '<p>alsjdk lajdlkja l</p>\r\n', '', 'verified', 1, '2021-02-09 15:46:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rab`
--

CREATE TABLE `rab` (
  `id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `dimension` varchar(225) NOT NULL,
  `total_cost` bigint(20) NOT NULL,
  `status` varchar(225) NOT NULL DEFAULT 'pending',
  `step1_percentage` float NOT NULL,
  `step1_status` varchar(225) NOT NULL DEFAULT 'pending',
  `step2_percentage` float NOT NULL,
  `step2_status` varchar(225) NOT NULL DEFAULT 'pending',
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rab`
--

INSERT INTO `rab` (`id`, `proposal_id`, `activity_id`, `dimension`, `total_cost`, `status`, `step1_percentage`, `step1_status`, `step2_percentage`, `step2_status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '10 x 20', 533350000, 'verified', 0, 'pending', 0, 'pending', 1, '2021-02-08 11:45:49', NULL),
(2, 3, 2, '10 x 20', 4300000, 'verified', 0, 'pending', 0, 'pending', 1, '2021-02-09 16:31:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rab_item`
--

CREATE TABLE `rab_item` (
  `id` int(11) NOT NULL,
  `rab_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `unit` varchar(225) NOT NULL,
  `volume` float NOT NULL,
  `unit_price` bigint(20) NOT NULL,
  `total_price` bigint(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rab_item`
--

INSERT INTO `rab_item` (`id`, `rab_id`, `category_id`, `name`, `unit`, `volume`, `unit_price`, `total_price`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 'Batu Split 2/3', 'M3', 432, 260000, 112320000, 1, '2021-02-08 11:45:49', NULL),
(2, 1, 3, 'Pasir Cor', 'M3', 222, 280000, 62160000, 1, '2021-02-08 11:45:49', NULL),
(3, 1, 3, 'Sirtu Base Course', 'M3', 180, 165000, 29700000, 1, '2021-02-08 11:45:49', NULL),
(4, 1, 3, ' Semen Pc', 'Sak', 1670, 65000, 108550000, 1, '2021-02-08 11:45:49', NULL),
(5, 1, 3, 'Papan Terentang', 'M3', 8, 920000, 7360000, 1, '2021-02-08 11:45:49', NULL),
(6, 1, 3, 'Papan Cor', 'Bh', 300, 15000, 4500000, 1, '2021-02-08 11:45:49', NULL),
(7, 1, 3, 'Paku', 'Kg', 28, 15000, 420000, 1, '2021-02-08 11:45:49', NULL),
(8, 1, 3, 'Plastik', 'Rol', 15, 265000, 3975000, 1, '2021-02-08 11:45:49', NULL),
(9, 1, 1, 'Mesin Gilas', 'hr', 8, 750000, 6000000, 1, '2021-02-08 11:50:55', NULL),
(10, 1, 1, 'Mobilitas Mesin Gilas', 'ls', 1, 4000000, 4000000, 1, '2021-02-08 11:50:55', NULL),
(11, 1, 1, 'Mesin Molen', 'hr', 75, 400000, 30000000, 1, '2021-02-08 11:50:55', NULL),
(12, 1, 1, 'Gerobak', 'bh', 6, 450000, 2700000, 1, '2021-02-08 11:50:55', NULL),
(13, 1, 1, 'Papan Nama Kegiatan', 'bh', 1, 150000, 150000, 1, '2021-02-08 11:50:55', NULL),
(14, 1, 1, 'Prasasti', 'bh', 1, 100000, 100000, 1, '2021-02-08 11:50:55', NULL),
(15, 1, 1, 'Pengki', 'bh ', 36, 10000, 360000, 1, '2021-02-08 11:50:55', NULL),
(16, 1, 1, 'Ember', 'bh', 50, 15000, 750000, 1, '2021-02-08 11:50:55', NULL),
(17, 1, 1, 'Benang', 'bh', 10, 5000, 50000, 1, '2021-02-08 11:50:55', NULL),
(18, 1, 1, 'Linggis', 'bh', 5, 75000, 375000, 1, '2021-02-08 11:50:55', NULL),
(19, 1, 1, 'Sekop', 'bh', 5, 80000, 400000, 1, '2021-02-08 11:50:55', NULL),
(20, 1, 4, 'Pekerja', 'HOK', 901, 120000, 108120000, 1, '2021-02-08 11:52:37', NULL),
(21, 1, 4, 'Tukang', 'HOK', 165, 150000, 24750000, 1, '2021-02-08 11:52:37', NULL),
(22, 1, 5, 'Operasional TPK', 'Keg', 1, 21110000, 21110000, 1, '2021-02-08 11:56:37', NULL),
(23, 1, 5, 'ATK', 'Keg', 1, 4500000, 4500000, 1, '2021-02-08 11:56:37', NULL),
(24, 1, 5, 'Dokumentasi', 'Keg', 1, 500000, 500000, 1, '2021-02-08 11:56:37', NULL),
(25, 1, 5, 'Biaya Survey dan Transportasi', 'Keg', 1, 500000, 500000, 1, '2021-02-08 11:56:37', NULL),
(26, 2, 6, 'Pengki', 'Bh', 50, 12000, 600000, 1, '2021-02-09 16:31:36', NULL),
(27, 2, 6, 'Pacul', 'Bh', 25, 36000, 900000, 1, '2021-02-09 16:31:36', NULL),
(28, 2, 8, 'Pasir', 'Sak', 50, 56000, 2800000, 1, '2021-02-09 16:31:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rab_item_gallery`
--

CREATE TABLE `rab_item_gallery` (
  `id` int(11) NOT NULL,
  `rab_item_id` int(11) NOT NULL,
  `file` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rab_item_gallery`
--

INSERT INTO `rab_item_gallery` (`id`, `rab_item_id`, `file`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'https://tipsfotografi.net/wp-content/uploads/2013/02/Komposisi-Mata-Untuk-Foto-Portrait-434x550.jpg', 1, '2021-02-08 02:45:38', NULL),
(2, 2, 'https://cdn-2.tstatic.net/jabar/foto/bank/images/kwitansi-rs-di-tangerang-pungut-biaya-pemakaman-jenazah-covid-19.jpg', 1, '2021-02-08 02:47:37', NULL),
(3, 3, 'https://jendela360.com/info/wp-content/uploads/2020/03/Kwitansi-Sewa-Rumah.jpg', 1, '2021-02-08 02:48:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rab_item_realization`
--

CREATE TABLE `rab_item_realization` (
  `id` int(11) NOT NULL,
  `rab_item_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `status` varchar(225) NOT NULL DEFAULT 'pending' COMMENT 'pending,verification,verified',
  `step` int(11) NOT NULL,
  `total_price` bigint(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rab_item_realization`
--

INSERT INTO `rab_item_realization` (`id`, `rab_item_id`, `file_id`, `status`, `step`, `total_price`, `created_by`, `created_at`, `updated_at`) VALUES
(4, 1, 1, 'verification', 1, 112320000, 1, '2021-02-09 10:46:45', NULL),
(5, 2, 2, 'verification', 1, 62160000, 1, '2021-02-09 11:29:53', NULL),
(6, 3, 3, 'verification', 1, 29700000, 1, '2021-02-09 11:30:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rab_location`
--

CREATE TABLE `rab_location` (
  `id` int(11) NOT NULL,
  `rab_id` int(11) NOT NULL,
  `point1` text NOT NULL,
  `point2` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rab_location`
--

INSERT INTO `rab_location` (`id`, `rab_id`, `point1`, `point2`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, '-6.6705181,106.9253353', '-6.680019199999999,106.9305122', 1, '2021-02-08 11:45:49', NULL),
(2, 2, '-6.680705100000001,106.9295587', '', 1, '2021-02-09 16:31:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  `role` int(11) NOT NULL,
  `kec_id` int(11) DEFAULT NULL,
  `desa_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`, `kec_id`, `desa_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Ryan Pramasyana', 'ryanpramasyana@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 1, NULL, NULL, 1, '2021-01-27 08:38:05', NULL),
(2, 'kecamatan', 'kecamatan@gmail.com', '77e2edcc9b40441200e31dc57dbb8829', 2, 34, 87, 1, '2021-01-27 09:55:52', '2021-01-26 22:02:17'),
(3, 'asd', 'asd', '7815696ecbf1c96e6894b779456d330e', 1, NULL, NULL, 1, '2021-02-07 11:01:02', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proposal`
--
ALTER TABLE `proposal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rab`
--
ALTER TABLE `rab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rab_item`
--
ALTER TABLE `rab_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rab_item_gallery`
--
ALTER TABLE `rab_item_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rab_item_realization`
--
ALTER TABLE `rab_item_realization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rab_location`
--
ALTER TABLE `rab_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity`
--
ALTER TABLE `activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `proposal`
--
ALTER TABLE `proposal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rab`
--
ALTER TABLE `rab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rab_item`
--
ALTER TABLE `rab_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `rab_item_gallery`
--
ALTER TABLE `rab_item_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rab_item_realization`
--
ALTER TABLE `rab_item_realization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `rab_location`
--
ALTER TABLE `rab_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
