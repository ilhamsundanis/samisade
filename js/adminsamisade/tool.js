$( document ).ready(function() {
	$('.select2').select2();

	$("#phone").intlTelInput({
      utilsScript: getBaseURL() + "/js/phone/utils.js"
    });

    $('#table-rentist').DataTable();

});